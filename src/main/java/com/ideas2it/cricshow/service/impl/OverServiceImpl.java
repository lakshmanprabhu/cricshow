package com.ideas2it.cricshow.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.cricshow.dao.OverDAO;
import com.ideas2it.cricshow.common.BallResult;
import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.entities.Match;
import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.entities.Over;
import com.ideas2it.cricshow.entities.OverDetail;
import com.ideas2it.cricshow.entities.Team;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.MatchInfo;
import com.ideas2it.cricshow.info.MatchOverInfo;
import com.ideas2it.cricshow.info.PlayerInfo;
import com.ideas2it.cricshow.info.PlayerSelectionInfo;
import com.ideas2it.cricshow.info.TeamInfo;
import com.ideas2it.cricshow.service.MatchService;
import com.ideas2it.cricshow.service.PlayerService;
import com.ideas2it.cricshow.service.OverService;
import com.ideas2it.cricshow.service.OverDetailService;
import com.ideas2it.cricshow.utils.CommonUtil;

/**
 * It links the overService with its dao layer. It implements the overService  
 * interface. It provides the business logics to the application.
 */
@Service
public class OverServiceImpl implements OverService {
    private OverDAO overDao;
    private MatchService matchService;
    private PlayerService playerService;
    private static final Logger log = Logger.getLogger(OverServiceImpl.class);
    @Autowired
    public OverServiceImpl(OverDAO overDao, MatchService matchService, 
            PlayerService playerService) {
        this.overDao = overDao;
        this.matchService = matchService;
        this.playerService = playerService;
    }
    
    @Override
    public MatchOverInfo addOver(String[] batsmenIds, int bowlerId, int matchId)  
            throws CricShowException {
        int overNumber = overDao.retrieveOverNumber(matchId);
        Match match = matchService.fetchMatchByIdForOver(matchId);
        List<Integer> listOfBatsmenIds = 
                CommonUtil.convertStringArrayIdsToIntegerListIds(batsmenIds);
        Player firstBatsman = playerService.fetchPlayerByIdForOverDetail(
                                    listOfBatsmenIds.get(0));
        Player secondBatsman = playerService.fetchPlayerByIdForOverDetail(
                                     listOfBatsmenIds.get(1));
        Player bowler = playerService.fetchPlayerByIdForOverDetail(bowlerId);
        MatchInfo matchInfo = CommonUtil.convertMatchEntityToMatchInfo(match);
        Over over = new Over();
        over.setMatchId(matchId);
        over.setOverNumber(++overNumber);
        int overId = overDao.createOver(over);
        over = overDao.retrieveOverById(overId);
        MatchOverInfo matchOverInfo = new MatchOverInfo();
        matchOverInfo.setMatchInfo(matchInfo);
        matchOverInfo.setBowlerId(bowlerId);
        matchOverInfo.setMatchId(matchId);
        matchOverInfo.setBatsmenIds(batsmenIds);
        matchOverInfo.setFirstBatsmanId(listOfBatsmenIds.get(0));
        matchOverInfo.setSecondBatsmanId(listOfBatsmenIds.get(1));
        matchOverInfo.setOverId(overId);
        matchOverInfo.setOverNumber(over.getOverNumber()); 
        matchOverInfo.setFirstBatsmanName(firstBatsman.getName());
        matchOverInfo.setSecondBatsmanName(secondBatsman.getName());
        matchOverInfo.setBowlerName(bowler.getName());
        return matchOverInfo;
    }
    
    @Override
    public MatchOverInfo addNextOver(int firstBatsmanId, int secondBatsmanId, 
            int bowlerId, int matchId) throws CricShowException {
        String[] batsmenIds = {Integer.toString(firstBatsmanId),
                                        Integer.toString(secondBatsmanId)};    
        MatchOverInfo matchOverInfo = addOver(batsmenIds, 
                                                bowlerId, matchId);
        return matchOverInfo;
    }
            
    @Override
    public MatchOverInfo resumeOver(int strikeBatsmanId, int secondBatsmanId, 
            int bowlerId, int matchId, int overId) throws CricShowException {
        int overNumber = overDao.retrieveOverNumber(matchId);
        Match match = matchService.fetchMatchByIdForOver(matchId);
        Player strikeBatsman = playerService.fetchPlayerByIdForOverDetail(
                                    strikeBatsmanId);
        Player secondBatsman = playerService.fetchPlayerByIdForOverDetail(
                                     secondBatsmanId);
        Player bowler = playerService.fetchPlayerByIdForOverDetail(bowlerId);
        MatchInfo matchInfo = CommonUtil.convertMatchEntityToMatchInfo(match);
        Over over = overDao.retrieveOverById(overId);
        
        String[] batsmenIds = {Integer.toString(strikeBatsmanId),
                                        Integer.toString(secondBatsmanId)};    
        MatchOverInfo matchOverInfo = new MatchOverInfo();
        matchOverInfo.setMatchInfo(matchInfo);
        matchOverInfo.setBowlerId(bowlerId);
        matchOverInfo.setMatchId(matchId);
        matchOverInfo.setBatsmenIds(batsmenIds);
        matchOverInfo.setFirstBatsmanId(strikeBatsmanId);
        matchOverInfo.setSecondBatsmanId(secondBatsmanId);
        matchOverInfo.setOverId(overId);
        matchOverInfo.setOverNumber(over.getOverNumber()); 
        matchOverInfo.setFirstBatsmanName(strikeBatsman.getName());
        matchOverInfo.setSecondBatsmanName(secondBatsman.getName());
        matchOverInfo.setBowlerName(bowler.getName());
        return matchOverInfo;
    }
    
    @Override
    public PlayerSelectionInfo fetchBowlerForOver(int bowlerId, int matchId, 
            int overId, int firstBatsmanId, int secondBatsmanId) throws 
            CricShowException, ParseException {
        Player bowler = playerService.fetchPlayerWithTeamById(bowlerId);
        int fieldingTeamId = bowler.getTeam().getId();
        List<Player> playersInFieldingTeam = playerService.fetchPlayersInTeam(
                                                 fieldingTeamId);        
        List<PlayerInfo> fieldingPlayersInfo = 
                CommonUtil.convertPlayerListToPlayerInfos(playersInFieldingTeam);
        PlayerSelectionInfo playerSelectionInfo = new PlayerSelectionInfo();
        playerSelectionInfo.setFieldingPlayerInfos(fieldingPlayersInfo);
        playerSelectionInfo.setMatchId(matchId);
        playerSelectionInfo.setFirstBatsmanId(firstBatsmanId);
        playerSelectionInfo.setSecondBatsmanId(secondBatsmanId);
        playerSelectionInfo.setOverId(overId);
        playerSelectionInfo.setBowlerId(bowlerId);
        playerSelectionInfo.setPreviousBowlerId(bowlerId);
        return playerSelectionInfo;
    }   
        
    @Override
    public PlayerSelectionInfo fetchBatsmanForOver(int bowlerId, int matchId, 
            int overId, int outBatsmanId, int nonStrikerId) throws 
            CricShowException, ParseException {
        Player outBatsman = playerService.fetchPlayerWithTeamById(outBatsmanId);
        Player nonStrikerBatsman = playerService.fetchPlayerWithTeamById(
                                         nonStrikerId);        
        int battingTeamId = outBatsman.getTeam().getId();
        List<Player> playersInBattingTeam = playerService.fetchPlayersInTeam(
                                                 battingTeamId);        
        List<PlayerInfo> battingingPlayersInfo = 
                CommonUtil.convertPlayerListToPlayerInfos(playersInBattingTeam);
        PlayerSelectionInfo playerSelectionInfo = new PlayerSelectionInfo();
        playerSelectionInfo.setBattingPlayerInfos(battingingPlayersInfo);
        playerSelectionInfo.setMatchId(matchId);
        playerSelectionInfo.setSecondBatsmanId(nonStrikerId);
        playerSelectionInfo.setFirstBatsmanId(outBatsmanId);
        playerSelectionInfo.setBowlerId(bowlerId);
        playerSelectionInfo.setOverId(overId);
        return playerSelectionInfo;
    }
    
    @Override
    public Over fetchOverForOverDetail(int overId) throws CricShowException {
        return overDao.retrieveOverById(overId);
    }
    
    @Override
    public void updateOverDetailForOver(OverDetail overDetail, int overId) 
            throws CricShowException {
        Set<OverDetail> addOverDetail = new HashSet<OverDetail>();
        Over over = overDao.retrieveOverById(overId);
        int total = over.getTotalRuns();
        total += overDetail.getRunScoredInBall();
        overDetail.setOver(over);
        calculateOverStatistics(overDetail.getBallResult(), over);
        addOverDetail = over.getOverDetails();
        addOverDetail.add(overDetail);
        over.setOverDetails(addOverDetail);
        over.setTotalRuns(total);
        overDao.updateOver(over);
        int id = over.getId();
    }
    
    private void calculateOverStatistics(BallResult ballResult, Over over) {
        int run = ballResult.getValue();
        int fourCount = over.getFourCount();
        int sixCount = over.getSixCount();
        int byesCount = over.getByesCount();
        int legByesCount = over.getLegByesCount();
        int noBallCount = over.getNoBallCount();
        int wideBallCount = over.getWideBallCount();
        int wicketCount = over.getWicketCount();
        switch (run) {
            case Constants.FOURS:                
                ++fourCount;
                over.setFourCount(fourCount);
                break;
            case Constants.SIXES:                
                ++sixCount;
                over.setSixCount(sixCount);
                break;
            case Constants.BYES:                                
                ++byesCount;
                over.setByesCount(byesCount);
                break;
            case Constants.LEGBYES:                                
                ++legByesCount;
                over.setLegByesCount(legByesCount);
                break;
            case Constants.NOBALL:                                
                ++noBallCount;
                over.setNoBallCount(noBallCount);
                break;
            case Constants.WIDEBALL:                                
                ++wideBallCount;
                over.setWideBallCount(wideBallCount);
                break;
            case Constants.WICKET:                               
                ++wicketCount;
                over.setWicketCount(wicketCount);
                break;
            case 5:
                ++wicketCount;
                over.setWicketCount(wicketCount);
                break;
            default :
                run = 0;
        }
    }
}
