package com.ideas2it.cricshow.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.dao.PlayerDAO;
import com.ideas2it.cricshow.entities.Contact;
import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.entities.Team;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.PlayerInfo;
import com.ideas2it.cricshow.info.ResponsePagedPlayerInfo;
import com.ideas2it.cricshow.service.PlayerService;
import com.ideas2it.cricshow.utils.AgeUtil;
import com.ideas2it.cricshow.utils.CommonUtil;


/**
 * It a intermediate link between playerdao and controller. 
 * It provides the business logics to the application.
 */
@Service
public class PlayerServiceImpl implements PlayerService {
    private PlayerDAO playerDao;
    private String UPLOAD_DIRECTORY = "/home/ubuntu/Desktop/uploads/";
    
    @Autowired
    public PlayerServiceImpl(PlayerDAO playerDao) {
        this.playerDao = playerDao;
    }

    @Override
    public PlayerInfo addPlayer(PlayerInfo playerInfo, CommonsMultipartFile file)  
            throws CricShowException, ParseException, IOException {
        Player player = CommonUtil.convertPlayerInfoToPlayerEntity(playerInfo);
        Contact contact = player.getContact();
        player.setContact(contact);
        contact.setPlayer(player);
        int playerId = playerDao.savePlayer(player);
        player = saveImage(file, player);
        playerInfo = CommonUtil.convertPlayerEntityToPlayerInfo(player);
        String age = AgeUtil.calculateAge(player.getDob());
        playerInfo.setAge(age);
        return playerInfo;
    }

    @Override
    public Player saveImage(CommonsMultipartFile file, Player player) throws 
            CricShowException, IOException {
        if (!file.getOriginalFilename().isEmpty()) {
            String filename = file.getOriginalFilename();
            byte[] bytes = file.getBytes();  
            BufferedOutputStream stream = new BufferedOutputStream(new 
                    FileOutputStream(new File(UPLOAD_DIRECTORY + player.getId() 
                    + filename)));  
            stream.write(bytes);  
            stream.flush();
            stream.close();
            String pathName = Constants.LABEL_IMAGE_LOCATION + player.getId() 
                                    + file.getOriginalFilename();
            player.setImagePath(pathName); 
        }
        return player;
    }
    
    @Override
    public ResponsePagedPlayerInfo fetchFirstFivePlayers(int page) throws 
            CricShowException, ParseException {
        int noOfPages = (int) Math.ceil(totalCountOfPlayers() * 1.0 
                              / Constants.PAGE_CONTENT_RESTRICTION);
        List<Player> players = playerDao.retrieveAllPlayer(
                                     Constants.PAGE_CONTENT_RESTRICTION,
                                     (page-1) 
                                     * Constants.PAGE_CONTENT_RESTRICTION);
        List<PlayerInfo> playerInfos = CommonUtil.convertPlayerListToPlayerInfos(players);
        ResponsePagedPlayerInfo pagedPlayerInfo = new ResponsePagedPlayerInfo(
                noOfPages, page, playerInfos);
        return pagedPlayerInfo;
    }
    
    @Override
    public JSONArray fetchRemainingPlayers(int page) throws 
            CricShowException {
        List<Player> players = playerDao.retrieveAllPlayer(
                                     Constants.PAGE_CONTENT_RESTRICTION,
                                     (page-1) 
                                     * Constants.PAGE_CONTENT_RESTRICTION);
        JSONArray remainingPlayers = new JSONArray();
        for (Player player : players) {
            JSONObject playerJSON  = new JSONObject();
            playerJSON.put(Constants.LABEL_PLAYER_ID, player.getId());
            playerJSON.put(Constants.LABEL_PLAYER_NAME, player.getName());
            playerJSON.put(Constants.LABEL_PLAYER_ROLE, player.getRole());
            playerJSON.put(Constants.LABEL_PLAYER_STATUS,
                    player.getStatus());
            playerJSON.put(Constants.LABEL_PLAYER_COUNTRY,
                    player.getCountry()); 
            remainingPlayers.put(playerJSON);
        }
        return remainingPlayers;
    }
    
    @Override
    public int totalCountOfPlayers() throws CricShowException { 
        return playerDao.playersTotalCount();
    }

    @Override
    public PlayerInfo fetchPlayerById(int playerId) throws CricShowException, 
            ParseException { 
        Player player = playerDao.retrievePlayer(playerId);
        PlayerInfo playerInfo = CommonUtil.convertPlayerEntityToPlayerInfo(player);
        String age = AgeUtil.calculateAge(player.getDob());
        playerInfo.setAge(age);
        return playerInfo;
    }
    
    @Override
    public Player fetchPlayerByIdForOverDetail(int playerId) throws 
            CricShowException { 
        return playerDao.retrievePlayer(playerId);
    }
       
    @Override
    public PlayerInfo editPlayer(PlayerInfo playerInfo,  
            CommonsMultipartFile file) throws CricShowException, IOException, 
            ParseException {
        Player player = CommonUtil.convertPlayerInfoToPlayerEntity(playerInfo);
        player = saveImage(file, player);
        Contact contact = player.getContact();
        contact.setPlayer(player);
        player.setContact(contact);
        Player playerTeam = playerDao.retrievePlayerWithTeamById(player.getId());
        player.setTeam(playerTeam.getTeam());
        playerDao.updatePlayer(player);
        playerInfo = CommonUtil.convertPlayerEntityToPlayerInfo(player);
        String age = AgeUtil.calculateAge(player.getDob());
        playerInfo.setAge(age);
        return playerInfo;
    }
    
    @Override
    public Player fetchPlayerWithTeamById(int playerId) throws CricShowException {
        return playerDao.retrievePlayerWithTeamById(playerId);
    }
    
    @Override
    public List<Player> fetchPlayersByCountry(String country) throws 
            CricShowException {
        return playerDao.retrievePlayersByCountry(country);
    }
    
    @Override
    public List<Player> fetchPlayersToAddInTeam(List<Integer> playerIds) throws 
            CricShowException {
        return playerDao.retrievePlayersToAddInTeam(playerIds);
    }
   
    @Override
    public List<Player> fetchPlayersInTeam(int teamId) throws 
            CricShowException {
        return playerDao.viewPlayersInTeam(teamId);
    }
   
    @Override
    public void removePlayer(int playerId) throws CricShowException { 
        Player player = playerDao.retrievePlayer(playerId);
        playerDao.deletePlayer(player);
    }
    
    @Override
    public int getBatsmanCountInTeam(int teamId) throws CricShowException { 
        return playerDao.retrieveBatsmanCount(teamId);
    }
    
    @Override
    public int getBowlerCountInTeam(int teamId) throws CricShowException { 
        return playerDao.retrieveBowlerCount(teamId);
    }
    
    @Override
    public int getWicketKeeperCountInTeam(int teamId) throws CricShowException { 
        return playerDao.retrieveWicketKeeperCount(teamId);
    }
    
    @Override
    public int getPlayerCountInTeam(int teamId) throws CricShowException { 
        return playerDao.retrievePlayerCountInTeam(teamId);
    }
} 

