package com.ideas2it.cricshow.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.cricshow.common.BallResult;
import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.dao.OverDetailDAO;
import com.ideas2it.cricshow.entities.Match;
import com.ideas2it.cricshow.entities.Over;
import com.ideas2it.cricshow.entities.OverDetail;
import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.MatchOverInfo;
import com.ideas2it.cricshow.service.PlayerService;
import com.ideas2it.cricshow.service.MatchService;
import com.ideas2it.cricshow.service.OverService;
import com.ideas2it.cricshow.service.OverDetailService;
import com.ideas2it.cricshow.utils.CommonUtil;

/**
 * It links the overDetailService with its dao layer. It implements the  
 * overDetailService interface.It provides the business logics to the application.
 */
@Service
public class OverDetailServiceImpl implements OverDetailService {
    private OverDetailDAO overDetailDao;
    private PlayerService playerService;
    private MatchService matchService;
    private OverService overService;
    
    @Autowired
    public OverDetailServiceImpl(OverDetailDAO overDetailDao, 
            PlayerService playerService, MatchService matchService, 
            OverService overService) {
        this.overDetailDao = overDetailDao;
        this.playerService = playerService;
        this.matchService = matchService;
        this.overService = overService;
    }

    @Override
    public JSONArray addOverDetails(int matchId, int overId, int bowlerId, 
            int batsmanId) throws CricShowException {
        Player batsmanOnStrike = playerService.fetchPlayerByIdForOverDetail(
                                    batsmanId);
        Player bowler = playerService.fetchPlayerByIdForOverDetail(bowlerId);
        Match match = matchService.fetchMatchByIdForOver(matchId);
        int runs = -5 + (int) (Math.random() * ((6 - (-5)) + 1));
        BallResult ballResult = getBallResult(runs);
        int ballNumber = overDetailDao.retrieveBallNumber(matchId, overId);
        int runScored = calculateRuns(ballResult);
        OverDetail overDetail = new OverDetail();
        overDetail.setMatchId(matchId);
        overDetail.setBallNumber(++ballNumber);
        overDetail.setBatsman(batsmanOnStrike);
        overDetail.setBowler(bowler);    
        overDetail.setRunScoredInBall(runScored);
        overDetail.setBallResult(ballResult);   
        int currentBallNumber = overDetail.getBallNumber();
        if (6 >= currentBallNumber) {
            overService.updateOverDetailForOver(overDetail, overId);
        }
        JSONArray playInformation = new JSONArray();
        JSONObject overDetailJSON  = new JSONObject();
        overDetailJSON.put("BowlerName", bowler.getName());
        overDetailJSON.put("BatsmanOnStrikeId", batsmanOnStrike.getId());
        overDetailJSON.put("BallNumber", currentBallNumber);
        overDetailJSON.put("Runs", runScored);
        overDetailJSON.put("BallResult", ballResult);
        playInformation.put(overDetailJSON);
        return playInformation; 
    }
    
    /**
     * It generates ball result such as dot, ones, twos, threes, sixes, fours,
     * wicket, etc using the runs 
     *
     * @param runs - which is generated as a random number ranging from -5 to 6.
     * @return ballResult - which will give the outcome for a ball.
     */
    private BallResult getBallResult(int runs) {
        BallResult ballResult;
        switch (runs) {
            case Constants.DOTBALL:
                ballResult = BallResult.DOTBALL;
                break;
            case Constants.ONES:
                ballResult = BallResult.ONES;
                break;
            case Constants.TWOS:
                ballResult = BallResult.TWOS;
                break;
            case Constants.THREES:
                ballResult = BallResult.THREES;
                break;
            case Constants.FOURS:
                ballResult = BallResult.FOURS;
                break;
            case Constants.SIXES:
                ballResult = BallResult.SIXES;
                break;
            case Constants.BYES:
                ballResult = BallResult.BYES;
                break;
            case Constants.LEGBYES:
                ballResult = BallResult.LEGBYES;
                break;
            case Constants.NOBALL:
                ballResult = BallResult.NOBALL;
                break;
            case Constants.WIDEBALL:
                ballResult = BallResult.WIDEBALL;
                break;
            case Constants.WICKET:
                ballResult = BallResult.WICKET;
                break;
            default :
                ballResult = BallResult.DOTBALL;
         }
         return ballResult; 
    } 

    /**
     * It calculate runs based on the ball result such as one runs for wides, 
     * no ball, ones, byes and leg byes, etc.
     *
     * @param ballResult - which is the outcome of the ball bowled in a over.
     * @returns the calculated runs for a ballresult.
     */    
    private int calculateRuns(BallResult ballResult) {
        int value = ballResult.getValue();
        int run;
        switch (value) {
            case Constants.DOTBALL:
                run = 0;
                break;
            case Constants.ONES:
                run = 1;
                break;
            case Constants.TWOS:
                run = 2;
                break;
            case Constants.THREES:
                run = 3;
                break;
            case Constants.FOURS:
                run = 4;
                break;
            case Constants.SIXES:
                run = 6;
                break;
            case Constants.BYES:
                run = 1;
                break;
            case Constants.LEGBYES:
                run = 1;
                break;
            case Constants.NOBALL:
                run = 1;
                break;
            case Constants.WIDEBALL:
                run = 1;
                break;
            case Constants.WICKET:
                run = 0;
                break;
            default :
                run = 0;
        }
        return run;
    }   
}
