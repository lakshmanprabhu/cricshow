package com.ideas2it.cricshow.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.cricshow.dao.UserDAO;
import com.ideas2it.cricshow.entities.User;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.exception.InValidUserCredentialsException;
import com.ideas2it.cricshow.exception.LoginAttemptExceededException;
import com.ideas2it.cricshow.exception.UserNotFoundException;
import com.ideas2it.cricshow.info.UserInfo;
import com.ideas2it.cricshow.service.UserService;
import com.ideas2it.cricshow.utils.CommonUtil;
import com.ideas2it.cricshow.utils.EncryptionUtil;

/**
 * It a intermediate link between playerdao and controller. 
 * It provides the business logics to the application.
 */
@Service
public class UserServiceImpl implements UserService {
    private UserDAO userDao;
    
    @Autowired
    public UserServiceImpl(UserDAO userDao) {
        this.userDao = userDao;
    }    
    @Override
    public int addUser(UserInfo userInfo) throws CricShowException {
        String encryptedPassword =  EncryptionUtil.getEncryptedPassword(
                                         userInfo.getPassword());
        userInfo.setPassword(encryptedPassword);
        User user = CommonUtil.convertUserInfoToUserEntity(userInfo);
        user.setStatus(Boolean.TRUE);
        user.setLoginAttempt(0);
        return userDao.createUser(user);
    }
    
    @Override
    public UserInfo fetchUserByEmailAddress(String emailAddress) throws  
            CricShowException {    
        User user = userDao.retrieveUserByEmailAddress(emailAddress);
        UserInfo userInfo = CommonUtil.convertUserEntityToUserInfo(user);
        if (null != userInfo) {
            return userInfo;
        } else {
            throw new UserNotFoundException("Sorry no user found");
        }
    }
      
    @Override
    public void updateUser(User user) throws CricShowException {
        userDao.updateUser(user);
    }
    
    @Override
    public boolean isValidUserCredentials(String emailAddress, String password,
            UserInfo userInfo) throws CricShowException { 
        String encryptedPassword = EncryptionUtil.getEncryptedPassword(password);
        int failedAttempts = 0;
        
        User user = CommonUtil.convertUserInfoToUserEntity(userInfo);
        if (user.getPassword().equals(encryptedPassword)
                        && user.getEmailAddress().equals(emailAddress)) {
            return Boolean.TRUE;
        } else {
            failedAttempts = user.getLoginAttempt();
            ++failedAttempts;
            user.setLoginAttempt(failedAttempts);
            userDao.updateUser(user);
            throw new InValidUserCredentialsException(
                    "Sorry username or password incorrect");
        }
    }
    
    @Override
    public boolean isLoginAttemptExceeded(UserInfo userInfo) throws 
            CricShowException { 
        int failedAttempts = 0;
        
        User user = CommonUtil.convertUserInfoToUserEntity(userInfo);
        failedAttempts = user.getLoginAttempt();
        if (3 < failedAttempts) {
            user.setStatus(Boolean.FALSE);
            userDao.updateUser(user);
            throw new LoginAttemptExceededException(
                    "InValid Login attempt exceeded than 3 attempt." +
                    "Now you are restricted ");
        } else {
            return true;
        }
    } 
} 

