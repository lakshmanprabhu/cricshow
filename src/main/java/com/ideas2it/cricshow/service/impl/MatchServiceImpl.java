package com.ideas2it.cricshow.service.impl;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.cricshow.common.PlayingFormat;
import com.ideas2it.cricshow.dao.MatchDAO;
import com.ideas2it.cricshow.entities.Match;
import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.entities.Team;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.MatchInfo;
import com.ideas2it.cricshow.info.PlayerInfo;
import com.ideas2it.cricshow.info.PlayInfo;
import com.ideas2it.cricshow.info.TeamInfo;
import com.ideas2it.cricshow.service.PlayerService;
import com.ideas2it.cricshow.service.MatchService;
import com.ideas2it.cricshow.service.TeamService;
import com.ideas2it.cricshow.utils.CommonUtil;
import com.ideas2it.cricshow.utils.DateUtil;

/**
 * It links the match entity with its dao. 
 * It provides the business logics to the application.
 */
@Service
public class MatchServiceImpl implements MatchService {
    private MatchDAO matchDao;
    private TeamService teamService;
    private PlayerService playerService;
    
    @Autowired
    public MatchServiceImpl(TeamService teamService, PlayerService playerService,
            MatchDAO matchDao) {
        this.matchDao = matchDao;
        this.teamService = teamService;
        this.playerService = playerService;
    }
    
    @Override
    public MatchInfo addMatch(MatchInfo matchInfo, String playingDate) throws 
            ParseException, IOException, CricShowException {
        Date matchDate = DateUtil.fetchPlayingDate(playingDate);
        
        matchInfo.setPlayingDate(matchDate);
        Match match = CommonUtil.convertMatchInfoToMatchEntity(matchInfo);
        int matchId = matchDao.createMatch(match);
        List<Team> teamsForMatch = fetchTeamsForMatch(matchId);
        matchInfo.setTeams(CommonUtil.convertTeamListToTeamInfos(teamsForMatch));
        matchInfo.setId(matchId);
        return matchInfo;
    }
    
    @Override
    public void addTeams(MatchInfo matchInfo, String[] ids, String playingDate) 
            throws ParseException, CricShowException, IOException {
        Match match = CommonUtil.convertMatchInfoToMatchEntity(matchInfo);
        addTeamsToMatch(match, ids, playingDate);
    }
        
    @Override
    public void addTeamsToMatch(Match match, String[] ids, String playingDate) 
            throws ParseException, CricShowException, IOException {  
        Date matchDate = DateUtil.fetchPlayingDate(playingDate);
       
        match.setPlayingDate(matchDate);
        Set<Team> teamsInMatch = new HashSet<Team>();
        Set<Team> teamsToAdd = new HashSet<Team>();
        if (null != ids) {
            List<Integer> teamIds = CommonUtil.convertStringArrayIdsToIntegerListIds(ids);
            for (Integer id : teamIds) {
                Team team = retrieveTeamForMatch(id);
                teamsToAdd.add(team);
            }
            teamsInMatch.addAll(teamsToAdd);  
        } else {
            teamsInMatch = match.getTeams();
        }
        match.setTeams(teamsInMatch);
        matchDao.updateMatch(match);     
    }

    @Override
    public MatchInfo fetchMatchById(int matchId) throws CricShowException {
        Match match = matchDao.retrieveMatchById(matchId);
        MatchInfo matchInfo = CommonUtil.convertMatchEntityToMatchInfo(match);
        List<Team> teamsInMatch = new ArrayList<Team>(match.getTeams());
        matchInfo.setTeams(CommonUtil.convertTeamListToTeamInfos(teamsInMatch));
        return matchInfo;
    }
    
    
    @Override
    public Match fetchMatchByIdForOver(int matchId) throws CricShowException {
        Match match = matchDao.retrieveMatchById(matchId);
        return match;
    }
    
    @Override
    public List<MatchInfo> fetchAllMatches() throws CricShowException { 
        List<Match> retrievedMatches = matchDao.retrieveAllMatches();
        return CommonUtil.convertMatchListToMatchInfos(retrievedMatches);
    } 
    
    @Override
    public List<Team> fetchAllTeams() throws CricShowException { 
        return teamService.fetchAllTeamsForMatch();
    }
    
    @Override
    public void removeMatch(int matchId) throws CricShowException { 
        matchDao.deleteMatch(matchId);
    }

    @Override
    public List<Team> fetchTeamsForMatch(int matchId) throws CricShowException {
        boolean isTeamDateSame = Boolean.FALSE;
       
        List<Team> teamsForMatch = new ArrayList<Team>();
        Match match = matchDao.retrieveMatchById(matchId);
        Date matchDate = match.getPlayingDate();
        List<Team> allTeams = teamService.fetchAllTeamsForMatch();
        for (Team team : allTeams) {
            //if (team.getStatus()) {
                for (Match teamsMatch : team.getMatches()) {
                    if (teamsMatch.getPlayingDate().equals(matchDate)) {
                        isTeamDateSame = Boolean.TRUE;
                    }
                }
                if (!isTeamDateSame) {
                    teamsForMatch.add(team);
                }
                isTeamDateSame = Boolean.FALSE;
            //}
        }
        return teamsForMatch;
    }
    
    @Override
    public void editMatch(MatchInfo matchInfo, String[] ids,
            String playingDate) throws CricShowException, ParseException,
            IOException {
        int matchId = matchInfo.getId();
        
        Match match = matchDao.retrieveMatchById(matchId);
        match.setName(matchInfo.getName());
        match.setVenue(matchInfo.getVenue());
        match.setPlayingFormat(matchInfo.getPlayingFormat()); 
        addTeamsToMatch(match, ids, playingDate);
    }
    
    @Override
    public Team retrieveTeamForMatch(int teamId) throws CricShowException { 
        return teamService.retrieveTeamForMatch(teamId);
    }
    
    @Override
    public MatchInfo fetchMatchForUpdate(int matchId) throws CricShowException {
        Match match = matchDao.retrieveMatchById(matchId);
        List<Team> teamsInMatch = new ArrayList<Team>(match.getTeams());
        MatchInfo matchInfo = CommonUtil.convertMatchEntityToMatchInfo(match);
        matchInfo.setTeams(CommonUtil.convertTeamListToTeamInfos(teamsInMatch));
        List<Team> teamsNotInMatch = fetchTeamsForMatch(matchId);
        matchInfo.setTeamsNotInMatch(CommonUtil.convertTeamListToTeamInfos(
                teamsNotInMatch));
        return matchInfo;
    }
    
    @Override
    public PlayInfo fetchPlayInfoForMatch(int matchId) throws CricShowException, 
            ParseException {
        Match match = matchDao.retrieveMatchById(matchId);
        List<Team> teamsInMatch = new ArrayList<Team>(match.getTeams());
        List<Integer> teamsId = new ArrayList<Integer>();
        for (Team team : teamsInMatch) {
            teamsId.add(team.getId());
        }
        int battingTeamId = teamsId.get(0);
        int fieldingTeamId = teamsId.get(1);

        List<Player> playersInBattingTeam = playerService.fetchPlayersInTeam(
                                                battingTeamId);
        List<Player> playersInFieldingTeam = playerService.fetchPlayersInTeam(
                                                 fieldingTeamId);
        
        List<PlayerInfo> battingPlayersInfo = 
                CommonUtil.convertPlayerListToPlayerInfos(playersInBattingTeam);
        List<PlayerInfo> fieldingPlayersInfo = 
                CommonUtil.convertPlayerListToPlayerInfos(playersInFieldingTeam);
        Team battingTeam = teamsInMatch.get(0);
        Team fieldingTeam = teamsInMatch.get(1);
        TeamInfo battingTeamInfo = CommonUtil.convertTeamEntityToTeamInfo(
                                         battingTeam);
        battingTeamInfo.setPlayersInTeam(battingPlayersInfo);
        TeamInfo fieldingTeamInfo = CommonUtil.convertTeamEntityToTeamInfo(
                                          fieldingTeam);
        fieldingTeamInfo.setPlayersInTeam(fieldingPlayersInfo);
        MatchInfo matchInfo = CommonUtil.convertMatchEntityToMatchInfo(match);
        PlayInfo playInfo = new PlayInfo();
        playInfo.setMatchInfo(matchInfo);
        playInfo.setBattingTeamInfo(battingTeamInfo);
        playInfo.setFieldingTeamInfo(fieldingTeamInfo);
        return playInfo;
    }
}
