package com.ideas2it.cricshow.service.impl;

import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.common.Country;
import com.ideas2it.cricshow.dao.TeamDAO;
import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.entities.Team;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.PlayerInfo;
import com.ideas2it.cricshow.info.TeamInfo;
import com.ideas2it.cricshow.service.PlayerService;
import com.ideas2it.cricshow.service.TeamService;
import com.ideas2it.cricshow.utils.CommonUtil;

/**
 * It links the team entity with its dao. 
 * It provides the business logics to the application.
 */
@Service
public class TeamServiceImpl implements TeamService {
    private TeamDAO teamDao;
    private PlayerService playerService; 
    
    @Autowired
    public TeamServiceImpl(PlayerService playerService, TeamDAO teamDao) {
        this.teamDao = teamDao;
        this.playerService = playerService;
    }
    
    @Override
    public TeamInfo addTeam(TeamInfo teamInfo,  
            String[] ids) throws CricShowException {
        teamInfo.setStatus(Boolean.FALSE);
        Team team = CommonUtil.convertTeamInfoToTeamEntity(teamInfo);
        if (null != ids) {
            List<Player> playersToAdd = fetchPlayersToAddInTeam(ids);
            Set<Player> players = new HashSet<Player>(playersToAdd);
            team.setPlayers(players);
        }
        int teamId = teamDao.createTeam(team);
        teamInfo = CommonUtil.convertTeamEntityToTeamInfo(team);
        teamInfo.setId(teamId);
        return teamInfo;

    }
    
    @Override
    public List<PlayerInfo> fetchPlayersByCountry(String country) throws 
            CricShowException, ParseException {
        List<Player> playersByCountry = playerService.fetchPlayersByCountry(country);
        return CommonUtil.convertPlayerListToPlayerInfos(playersByCountry);
    } 
   
    @Override
    public List<Player> fetchPlayersToAddInTeam(String[] ids) throws 
            CricShowException {
        return playerService.fetchPlayersToAddInTeam(
                CommonUtil.convertStringArrayIdsToIntegerListIds(ids));
    }
    
    @Override
    public List<TeamInfo> fetchAllTeams() throws CricShowException { 
        return CommonUtil.convertTeamListToTeamInfos(teamDao.retrieveAllTeams());
    } 
    
    @Override
    public List<Team> fetchAllTeamsForMatch() throws CricShowException { 
        return teamDao.retrieveAllTeams();  
    }
    
    @Override
    public List<Player> fetchPlayersInTeam(int teamId) throws 
            CricShowException {
        return playerService.fetchPlayersInTeam(teamId);
    }
    
    @Override
    public String fetchCountryByTeamId(int teamId) throws CricShowException { 
        Team team = teamDao.retrieveTeam(teamId);
        String country = team.getCountry().toString();
        return country;
    }
    
    @Override
    public int[] playerRoleCountInTeam(int teamId) throws CricShowException { 
        int[] roleCount = new int[5];
        roleCount[0] = playerService.getBatsmanCountInTeam(teamId);
        roleCount[1] = playerService.getBowlerCountInTeam(teamId);
        roleCount[2] = playerService.getWicketKeeperCountInTeam(teamId);
        roleCount[3] = playerService.getPlayerCountInTeam(teamId);
        Team team = teamDao.retrieveTeam(teamId);
        if (11 == roleCount[3]) {
            if (3 <= roleCount[0] && 3<= roleCount[1] && 1 == roleCount[2]) {
                team.setStatus(Boolean.TRUE);
                roleCount[4] = 1;
            }
        } else {
            team.setStatus(Boolean.FALSE);
            roleCount[4] = 0;
        }
        teamDao.updateTeam(team);
        return roleCount;
    }
    
    @Override
    public void removeTeam(int teamId) throws CricShowException { 
        teamDao.deleteTeam(teamId);
    }

    @Override
    public Team retrieveTeamForMatch(int teamId) throws CricShowException {
        return teamDao.retrieveTeamForMatch(teamId);
    }
     
    @Override
    public void updateTeamInformation(TeamInfo teamInfo, String[] addIds,
            String[] removeIds) throws CricShowException {
        int teamId = teamInfo.getId();
        
        Team team = teamDao.retrieveTeam(teamId);
        Set<Player> playersInTeam = new HashSet<Player>(
                                              fetchPlayersInTeam(teamId));
        if (null != teamInfo.getName()) {
            team.setName(teamInfo.getName());
        }
        if (null != removeIds) {
            List<Integer> playerIds = 
                    CommonUtil.convertStringArrayIdsToIntegerListIds(removeIds);
            for (Integer id : playerIds) {
                for (Player player : playersInTeam) {
                    if (player.getId() == id) {
                        playersInTeam.remove(player);
                        break;
                    }
                }
            }
        }
        if (null != addIds) {
            Set<Player> playersToAdd = new HashSet<Player>(
                                             fetchPlayersToAddInTeam(addIds));
            playersInTeam.addAll(playersToAdd);
        }  
        team.setPlayers(playersInTeam);
        teamDao.updateTeam(team);
    }
    
    @Override
    public TeamInfo fetchTeamInfo(int teamId, String country) throws 
            CricShowException, ParseException {
        int[] rolesCount = playerRoleCountInTeam(teamId);
        List<PlayerInfo> playersInTeam = CommonUtil.convertPlayerListToPlayerInfos(
                                               fetchPlayersInTeam(teamId));
        List<PlayerInfo> playersNotInTeam = fetchPlayersByCountry(country);
        Team team = teamDao.retrieveTeam(teamId);
        TeamInfo teamInfo = new TeamInfo(rolesCount, playersInTeam,
                                  playersNotInTeam);
        teamInfo.setName(team.getName());
        teamInfo.setCountry(Country.valueOf(country));
        teamInfo.setId(teamId);
        return teamInfo;    
    }
}
