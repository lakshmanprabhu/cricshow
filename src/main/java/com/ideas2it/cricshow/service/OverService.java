package com.ideas2it.cricshow.service;

import java.text.ParseException;

import com.ideas2it.cricshow.entities.Over;
import com.ideas2it.cricshow.entities.OverDetail;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.MatchOverInfo;
import com.ideas2it.cricshow.info.PlayerSelectionInfo;

/**
 * It links the over with its dao layer. 
 * It provides the business logics to the application.
 */
public interface OverService {
    
    /**
     * It adds over details such as batsmen , bowler in a over, match in which
     * a play is started.
     *
     * @param batsmenIds - is a ids of batsmen in a match who is going to play. 
     * @param bowlerId - is a id of bowler in a match who is going to play.
     * @param matchId - is a id of match for which the play is going. 
     * @return MatchOverInfo - contains information of the current playing match. 
     * @throws CricShowException which is a custom exception. 
     */
    MatchOverInfo addOver(String[] batsmenIds, int bowlerId, int matchId)  
            throws CricShowException;

    /**
     * It adds next over details such as batsmen , bowler in a over, match in 
     * which a play is to be resumed.
     *
     * @param firstBatsmanId - is a id of strike batsmen in a match.
     * @param secondBatsmanId - is a id of non-strike batsmen in a match.  
     * @param bowlerId - is a id of bowler in a match who has bowled a previous
     * over.
     * @param matchId - is a id of match for which the play is going. 
     * @return MatchOverInfo - contains information of the current playing match. 
     * @throws CricShowException which is a custom exception. 
     */    
    MatchOverInfo addNextOver(int firstBatsmanId, int secondBatsmanId, 
            int bowlerId, int matchId) throws CricShowException;

    /**
     * It fetches bowler list for a over in which a match is played.
     *
     * @param bowlerId - is a id of bowler in a match who is going to play.
     * @param firstBatsmanId - is a id of strike batsmen in a match.
     * @param secondBatsmanId - is a id of non-strike batsmen in a match.  
     * @param matchId - is a id of match for which the play is going. 
     * @param overId - is a id of over for which the over has completed. 
     * @return PlayerSelectionInfo - contains information of the current playing
     * match alongwith bowler list. 
     * @throws CricShowException which is a custom exception. 
     */
    PlayerSelectionInfo fetchBowlerForOver(int bowlerId, int matchId, 
            int overId, int firstBatsmanId, int secondBatsmanId) throws 
            CricShowException, ParseException;

    /**
     * It fetches batsmen list for a over when a wicket has fallen in a match.
     *
     * @param bowlerId - is a id of bowler in a match who is going to play.
     * @param outBatsmanId - is a id of strike batsmen in a match who is out in 
     * a over.
     * @param nonStrikerId - is a id of non-strike batsmen in a match.  
     * @param matchId - is a id of match for which the play is going. 
     * @param overId - is a id of over in which the spell is completed or in 
     * progress. 
     * @return PlayerSelectionInfo - contains information of the current playing
     * match alongwith batsman list. 
     * @throws CricShowException which is a custom exception. 
     */    
    PlayerSelectionInfo fetchBatsmanForOver(int bowlerId, int matchId, 
            int overId, int outBatsmanId, int nonStrikerId) throws 
            CricShowException, ParseException;

    /**
     * It adds over details such as batsmen , bowler in a over, match in which
     * a play is started.
     *
     * @param batsmenIds - is a ids of batsmen in a match who is going to play. 
     * @param bowlerId - is a id of bowler in a match who is going to play.
     * @param matchId - is a id of match for which the play is going. 
     * @return MatchOverInfo - contains information of the current playing match. 
     * @throws CricShowException which is a custom exception. 
     */    
    MatchOverInfo resumeOver(int strikeBatsmanId, int secondBatsmanId, 
            int bowlerId, int matchId, int overId) throws CricShowException;
            
    /**
     * It updates the over after completion of all balls in it.
     *
     * @param overId - is a id of over for which a bowler completed its bowling. 
     * @param overDetail is a object containing information such as runs scored,
     * ball number, etc.
     * @throws CricShowException which is a custom exception. 
     */
    void updateOverDetailForOver(OverDetail overDetail, int overId) throws 
            CricShowException;
    
    /**
     * It retrieves over information for a overdetail using overId.
     *
     * @param overId - is a id of over for which a play is started. 
     * @return information of the current over information. 
     * @throws CricShowException which is a custom exception. 
     */
    Over fetchOverForOverDetail(int overId) throws CricShowException;
}
