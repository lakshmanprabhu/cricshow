package com.ideas2it.cricshow.service;

import java.text.ParseException;
import java.util.List;

import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.entities.Team;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.PlayerInfo;
import com.ideas2it.cricshow.info.TeamInfo;

/**
 * It links the team entity with its dao. 
 * It provides the business logics to the application.
 */
public interface TeamService { 
    
    /**
     * It adds team name, country, status and adds set of players to it .
     *
     * @param name - is a name of team entered .
     * @param country - is a country entered .
     * @param status - is a status entered .
     * @param ids - is a id of list of player to be added in a team.
     * @return the team object of the newly added team.
     * @throws CricShowException which is a custom exception.
     */
    TeamInfo addTeam(TeamInfo teamInfo, String[] ids) throws CricShowException;
    
    /**
     * It fetches all the players having particular country in a database.
     *
     * @param country - is a particular country name used to fetch players.
     * @return list of players with a particular country.
     * @throws CricShowException which is a custom exception.
     */
    List<PlayerInfo> fetchPlayersByCountry(String country) throws 
            CricShowException, ParseException; 
   
    /**
     * It retrieves players with playersId to add in a team .
     *
     * @param ids - contains multiple players id to be retrieved.
     * @return list of players to add in a team.
     * @throws CricShowException which is a custom exception.
     */
    List<Player> fetchPlayersToAddInTeam(String[] ids) throws CricShowException;

    /**
     * It fetches team details using playerId in the database.
     *
     * @param playerId - is a id of a player for which a team is to be fetched.
     * @return team - is a team object containg team information.
     * @throws CricShowException which is a custom exception.
     *
    Team fetchTeamByPlayerId(int playerId) throws CricShowException;
        
    /**
     * It fetches all teams from the database .
     *
     * @return list of teams from the database.
     * @throws CricShowException which is a custom exception.
     */
    List<TeamInfo> fetchAllTeams() throws CricShowException; 
    
    /**
     * It fetches all teams from the database for a match .
     *
     * @return list of teams available for a match.
     * @throws CricShowException which is a custom exception.
     */
    List<Team> fetchAllTeamsForMatch() throws CricShowException;
    
    /**
     * It fetches the list of players in a team using teamId .
     * @param teamId - is a id of a team.
     * @return list of players details such as name, role and id.
     * @throws CricShowException which is a custom exception.
     */
    List<Player> fetchPlayersInTeam(int teamId) throws CricShowException;
       
    /**
     * Retrieve the country using teamId for which a team was created.
     *
     * @param teamId - is a id of a team.
     * @return country of the team.
     * @throws CricShowException which is a custom exception.
     */
    String fetchCountryByTeamId(int teamId) throws CricShowException;
    
    /**
     * It contains player roles count in a team using teamId.
     *
     * @param teamId - used to count no of players with a particular teamId.
     * @return counts for each roles.
     * @throws CricShowException which is a custom exception.
     */
    int[] playerRoleCountInTeam(int teamId) throws CricShowException;
    
    /**
     * Deletes the team details from the database.
     *
     * @param teamId - is a id of player in the database.
     * @throws CricShowException which is a custom exception.
     */
    void removeTeam(int teamId) throws CricShowException;

    /**
     * It retrieves team available for a match to be assigned.
     * 
     * @param teamId - is a id of a team.
     * @return  teams. 
     * @throws CricShowException which is a custom exception.
     */
    Team retrieveTeamForMatch(int teamId) throws CricShowException;
     
    /**
     * It provides the adding and removing of players to a team.
     *
     * @param teamId - is a id of team.
     * @param name - is a name to be updated.
     * @param addIds -  is a ids of players to be added in a team. 
     * @param removeIds -  is a ids of players to be removed from a team.
     * @throws CricShowException which is a custom exception.
     */
    void updateTeamInformation(TeamInfo teamInfo, String[] addIds,
            String[] removeIds) throws CricShowException;

    /**
     * It fetches teamInfo object which contains information such as players   
     * roles count ,list of players in team using teamId and list of players   
     * not in team using country.
     *
     * @param teamId - is a id of team.
     * @param country - is a country representing the team.. 
     * @return teamInfo - is a teamInfo object containing list of players in 
     * and not in team as well as players role count.
     * @throws CricShowException which is a custom exception.
     */    
    TeamInfo fetchTeamInfo(int teamId, String country) throws CricShowException,
            ParseException;
}
