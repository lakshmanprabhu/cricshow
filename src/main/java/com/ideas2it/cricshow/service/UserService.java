package com.ideas2it.cricshow.service;

import com.ideas2it.cricshow.entities.User;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.UserInfo;

/**
 * It a intermediate link between playerdao and controller. 
 * It provides the business logics to the application.
 */
public interface UserService {

    /**
     * It adds credentials of a user and pass it to the UserDAO.
     *
     * @param name - is a name of the user.
     * @param role - is the role given to a user.
     * @param emailAddress - is a email address of a user.
     * @param password - is a password of the user.
     * @param gender - is a gender of user.
     * @param phoneNumber - is a phone number entered by user.
     * @return the id of the newly added user.
     * @throws CricShowException which is a custom exception.
     * @throws ParseException if date is not in correct format.
     */
    int addUser(UserInfo userInfo) throws CricShowException;
    
    /**
     * It fetches the user object using emailAddress from the database.
     *
     * @param emailAddress - is a email address of user.
     * @return user object.
     * @throws CricShowException which is a custom exception.
     */
    UserInfo fetchUserByEmailAddress(String emailAddress) throws 
            CricShowException;
      
    /**
     * It updates user information.
     *
     * @param user - is a user object needed for updating.
     * @throws CricShowException which is a custom exception.
     */
    void updateUser(User user) throws CricShowException;
    
    /**
     * It validates user credentials with entered emailAddress and password 
     * is having authentication or not. It returns true if it is having access
     * otherwise returns false if it does not have access to it.
     *
     * @param emailAddress - is a email address of a user.
     * @param password - is a password of the user.
     * @param user - is a user object for which the authentication is checked.
     * @throws CricShowException which is a custom exception.
     */
    boolean isValidUserCredentials(String emailAddress, String password,
            UserInfo userInfo) throws CricShowException;
    
    /**
     * It counts login attempt from user and returns true if login attempt not  
     * exceeded than 3 otherwise return LoginAttemptExceededException as false.
     *
     * @param user - is a user object for which the authentication is checked.
     * @throws CricShowException which is a custom exception.
     */
    public boolean isLoginAttemptExceeded(UserInfo userInfo) throws 
            CricShowException;
} 

