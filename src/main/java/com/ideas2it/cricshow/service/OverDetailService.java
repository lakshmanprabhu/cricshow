package com.ideas2it.cricshow.service;

import org.json.JSONArray;

import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.MatchOverInfo;

/**
 * It links the overDetail with its dao layer. 
 * It provides the business logics to the application.
 */
public interface OverDetailService {
    
    /**
     * It adds overDetails such as run occured in ball, striker id, bowler id,
     * matchId,  .
     *
     * @param batsmanId - is a id of batsmen in a match who is at strike. 
     * @param bowlerId - is a id of bowler in a match who is going to play.
     * @param matchId - is a id of match for which the play is going. 
     * @return MatchOverInfo - contains information of the current playing match. 
     * @throws CricShowException which is a custom exception. 
     */
    JSONArray addOverDetails(int matchId, int overId, int bowlerId, 
            int batsmanId) throws CricShowException;
    
}
