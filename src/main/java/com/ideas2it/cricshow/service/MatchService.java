package com.ideas2it.cricshow.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import com.ideas2it.cricshow.entities.Match;
import com.ideas2it.cricshow.entities.Team;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.MatchInfo;
import com.ideas2it.cricshow.info.PlayInfo;

/**
 * It links the match entity with its dao. 
 * It provides the business logics to the application.
 */
public interface MatchService {
    
    /**
     * It adds match name, venue, playing format and playing date to it .
     *
     * @param name - is a name of match entered .
     * @param venue - is a venue of match entered .
     * @param playingFormat - is a format of playing entered.
     * @param playingDate - is a date in which a match is to be played.
     * @return matchInfo - contains information of the newly added match 
     * alongwith the teams available to form a match in it.
     * @throws CricShowException which is a custom exception.
     * @throws ParseException if date is not in correct format.
     * @throws IOException if any interuption occured during input/output. 
     */
    MatchInfo addMatch(MatchInfo matchInfo, String playingDate) throws 
            ParseException, IOException, CricShowException;
        
    /**
     * It adds two team in a match in which there is no schedule for a team 
     * is provided to a particular playing date.
     * String[] removeIds,
     * @param matchId - is a id of match in which a team is to be added.
     * @param ids - is a id of two teams to be added.
     * @throws CricShowException which is a custom exception.
     */
    void addTeamsToMatch(Match match, String[] ids, String playingDate) throws 
            ParseException, CricShowException, IOException;
    
    /**
     * It adds two team in a match in which there is no schedule for a team 
     * is provided to a particular playing date.
     * String[] removeIds,
     * @param matchId - is a id of match in which a team is to be added.
     * @param ids - is a id of two teams to be added.
     * @throws CricShowException which is a custom exception.
     */
    void addTeams(MatchInfo matchInfo, String[] ids, String playingDate) 
            throws ParseException, CricShowException, IOException;
    
    /**
     * It fetches all matches from the database .
     *
     * @return list of all match details.
     * @throws CricShowException which is a custom exception.
     */
    List<MatchInfo> fetchAllMatches() throws CricShowException; 
    
    /**
     * It fetches all teams from the database .
     *
     * @return list of all team details.
     * @throws CricShowException which is a custom exception.
     */
    List<Team> fetchAllTeams() throws CricShowException;
    
    /**
     * Deletes the match details from the database.
     *
     * @param matchId - is a id of matchss in the database.
     * @throws CricShowException which is a custom exception.
     */
    void removeMatch(int matchId) throws CricShowException;
    
    /**
     * Fetches the teams having playing date not equals to match date.
     *
     * @param matchId - is a id of a match in the database.
     * @return list of teams having unequal playing date with match date.
     * @throws CricShowException which is a custom exception.
     */
    List<Team> fetchTeamsForMatch(int matchId) throws CricShowException;

    /**
     * Provide options to update informations available in a match.
     * String[] removeIds,
     * @param matchId - is a id of a match. 
     * @param name - is a name of match.
     * @param venue - is a venue of match .
     * @param ids - contains ids of a two teams.
     * @param playingFormat - is a format of playing.
     * @param playingDate - is a date in which a match is to be played.
     * @throws CricShowException which is a custom exception.
     */
    void editMatch(MatchInfo matchInfo, String[] ids,  
            String playingDate) throws CricShowException, ParseException, 
            IOException;
    
    /**
     * It retrieve team details from the database using teamId for a match.
     *
     * @param teamId - is a id present in database.
     * @return team - is a team detail consisting of id, country and status.
     * @throws CricShowException which is a custom exception.
     * @throws ParseException if date is not in correct format.
     * @throws IOException if any interuption occured during input/output.
     */
    Team retrieveTeamForMatch(int teamId) throws CricShowException;
    
    /**
     * It fetches matchInfo object which contains information such as players   
     * roles count ,list of players in team using teamId and list of players   
     * not in team using country.
     *
     * @param matchId - is a id of match to retrieve match and teams for match. 
     * @return matchInfo - is a matchInfo object containing match information  
     * and teams available for a match.
     * @throws CricShowException which is a custom exception.
     */ 
    MatchInfo fetchMatchForUpdate(int matchId) throws CricShowException;
     
     /**
     * It fetches matchInfo object which contains information such as players   
     * roles count ,list of players in team using teamId and list of players   
     * not in team using country.
     *
     * @param matchId - is a id of match to retrieve match and teams for match. 
     * @return matchInfo - is a matchInfo object containing match information  
     * and teams available for a match.
     * @throws CricShowException which is a custom exception.
     */ 
    MatchInfo fetchMatchById(int matchId) throws CricShowException;
    
    /**
     * It retrieve match object for over using matchId.
     *
     * @param matchId - is a id of match to which a match object is fetched.
     * @return match object which contains information of a match such as name,
     * id, venue, playingDate and playingFormat.
     * @throws CricShowException which is a custom exception. 
     */
    Match fetchMatchByIdForOver(int matchId) throws CricShowException;
    
    /**
     * It retrieves playing information of match such as teams in match for which 
     * a play is scheduled alongwith the players in a team using matchId.
     *  
     * @param matchId - is a id for which a play is started.
     * @return playInfo - contains a playing information of a match started.
     */
    PlayInfo fetchPlayInfoForMatch(int matchId) throws CricShowException,
            ParseException; 
}
