package com.ideas2it.cricshow.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.json.JSONArray;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.entities.Team;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.PlayerInfo;
import com.ideas2it.cricshow.info.ResponsePagedPlayerInfo;

/**
 * It a intermediate link between playerdao and controller. 
 * It provides the business logics to the application.
 */
public interface PlayerService {

    /**
     * It adds credentials of a player and pass it to the playerDAO.
     *
     * @param player - is a player object which holds the model data of player.
     * @param file - is a part object which contains the image of user.
     * @return player object of newly added player.
     * @throws CricShowException which is a custom exception.
     * @throws ParseException if date is not in correct format.
     * @throws IOException if any interuption occured during input/output.
     */
    PlayerInfo addPlayer(PlayerInfo playerInfo, CommonsMultipartFile file) throws  
            CricShowException, ParseException, IOException;

    /**
     * It provides the uploading of image for a player .
     *
     * @param player - is a player object which holds the model data of player.
     * @param file - is a part object which contains the image of user.
     * @throws CricShowException which is a custom exception.
     * @throws IOException if any interuption occured during input/output. 
     */
    Player saveImage(CommonsMultipartFile file, Player player) throws 
            CricShowException, IOException;
            
    /**
     * It fetches first five player based on page number and store in a 
     * playerInfo object.
     * @param page - is a page number.
     * @return playerInfo - is a PlayerInfo object which contains current page,
     * no of pages and players list.
     * @throws CricShowException which is a custom exception.
     */
    ResponsePagedPlayerInfo fetchFirstFivePlayers(int page) throws 
            CricShowException, ParseException;
    
    /**
     * It fetches remaining players based on page number and store it in 
     * JSONArray .
     *
     * @param page - is a page number.
     * @return remainingPlayers - is a JSONArray which contains players list.
     * @throws CricShowException which is a custom exception.
     */
    JSONArray fetchRemainingPlayers(int page) throws CricShowException;
    
    /**
     * It gives count of all players in the database.
     *
     * @return  total players count in a database.
     * @throws CricShowException which is a custom exception.
     */
    int totalCountOfPlayers() throws CricShowException;

    /**
     * It fetches the player using id from the database.
     *
     * @param playerId - is a id of player.
     * @return player credentials as a playerInfo object.
     * @throws CricShowException which is a custom exception.
     * @throws ParseException which is a caused when age is parsed with the date
     * of birth.
     */
    PlayerInfo fetchPlayerById(int playerId) throws CricShowException, 
            ParseException;
    
    /**
     * It fetches the player using id from the database.
     *
     * @param playerId - is a id of player.
     * @return player credentials.
     * @throws CricShowException which is a custom exception.
     */
    Player fetchPlayerByIdForOverDetail(int playerId) throws CricShowException;
       
    /**
     * It updates player details in the database.
     *
     * @param player - is a player object which holds the model data of player.
     * @param file - is a part object which contains the image of user.
     * @param player - retrives the player credentials from it.
     * @throws CricShowException which is a custom exception.
     */
    PlayerInfo editPlayer(PlayerInfo playerInfo, CommonsMultipartFile file)   
            throws CricShowException, ParseException, IOException;
    
    /**
     * It fetches team details using playerId in the database.
     *
     * @param playerId - is a id of a player for which a team is to be fetched.
     * @return player - is a player object along with team information.
     * @throws CricShowException which is a custom exception.
     */
    Player fetchPlayerWithTeamById(int playerId) throws CricShowException;
        
    /**
     * It fetches all the players from the database having particular country.
     *
     * @param country - is a particular country name used to fetch players.
     * @return list of players with a particular country.
     * @throws CricShowException which is a custom exception.
     * @throws ParseException if date is not in correct format.
     * @throws IOException if any interuption occured during input/output.
     */
    List<Player> fetchPlayersByCountry(String country) throws CricShowException;
    
    /**
     * It retrieves players with playersId to add in a team .
     *
     * @param playersId - contains multiple players id to be retrieved.
     * @return list of players to add in a team.
     * @throws CricShowException which is a custom exception.
     */
    List<Player> fetchPlayersToAddInTeam(List<Integer> playerIds) throws 
            CricShowException;
   
    /**
     * It fetches the list of players in a team using teamId.
     *
     * @param teamId - is a id of a team.
     * @return list of players details such as name, role and id.
     * @throws CricShowException which is a custom exception.
     */
    List<Player> fetchPlayersInTeam(int teamId) throws CricShowException;
   
    /**
     * Deletes the player details from the database using playerId.
     *
     * @param playerId - is a id of player in the database.
     * @throws CricShowException which is a custom exception.
     */
    void removePlayer(int playerId) throws CricShowException;
    
    /**
     * It counts the number of players having role as batsman.
     *
     * @param teamId - is a id of a team in which players are batsman. 
     * @return  total batsman count in a team.
     * @throws CricShowException which is a custom exception.
     */
    int getBatsmanCountInTeam(int teamId) throws CricShowException;
    
    /**
     * It counts the number of players having role as bowler.
     *
     * @param teamId - is a id of a team in which players are bowler. 
     * @return  total bowler count in a team.
     * @throws CricShowException which is a custom exception.
     */
    int getBowlerCountInTeam(int teamId) throws CricShowException;
    
    /**
     * It counts the number of players having role as wicketKeeper.
     *
     * @param teamId - is a id of a team in which players are wicketKeeper.
     * @return  total wicketKeeper count in a team. 
     * @throws CricShowException which is a custom exception.
     */
    int getWicketKeeperCountInTeam(int teamId) throws CricShowException;
    
    /**
     * It counts all players in a team using teamId.
     *
     * @param teamId - used to count no of players with a particular teamId.
     * @return total count of players.
     * @throws CricShowException which is a custom exception.
     */
    int getPlayerCountInTeam(int teamId) throws CricShowException;
} 

