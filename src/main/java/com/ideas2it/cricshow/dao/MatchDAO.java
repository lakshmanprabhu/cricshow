package com.ideas2it.cricshow.dao;

import java.util.List;

import com.ideas2it.cricshow.entities.Match;
import com.ideas2it.cricshow.exception.CricShowException;

/**
 * It is a link to matches database for manipulating match in it.
 */
public interface MatchDAO {
    
    /**
     * It performs the adding of match in the database.
     *
     * @param match - stores the information such as id, match name, venue, 
     * date of playing and playing format in a match.
     * @return currentId is the id of a match created currently.
     * @throws CricShowException which is a custom exception.
     */
    int createMatch(Match match) throws CricShowException;  
   
    /**
     * It fetches all match details from the database and send it to list.
     *
     * @return allMatches - is a list of all match details .
     * @throws CricShowException which is a custom exception.
     */
    List<Match> retrieveAllMatches() throws CricShowException;
    
    /**
     * It retrieve match details from the database using matchId.
     *
     * @param matchId - is a match id present in database.
     * @return match - is a match detail consisting of id, match name, venue, 
     * date of playing and playing format.
     * @throws CricShowException which is a custom exception.
     */
    Match retrieveMatchById(int matchId) throws CricShowException;    
     
    /**
     * It deletes the match information from the database. 
     *
     * @param matchId - is the id of a match present in the database.
     * @throws CricShowException which is a custom exception.
     */
    void deleteMatch(int matchId) throws CricShowException;
    
    /**
     * It updates information using match in the database.
     *
     * @param match - contains information of a match.
     * @throws CricShowException which is a custom exception.
     */
    void updateMatch(Match match) throws CricShowException;
}
