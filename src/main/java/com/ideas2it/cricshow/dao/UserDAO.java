package com.ideas2it.cricshow.dao;

import com.ideas2it.cricshow.entities.User;
import com.ideas2it.cricshow.exception.CricShowException;

/**
 * It is a link to user table to store user credentials.
 */
public interface UserDAO  {
   
    /**
     * It performs the creation of user information.
     *
     * @param user stores the user information.
     * @return currentId is the id of a user created currently.
     * @throws CricShowException which is a custom exception.
     */
    int createUser(User user) throws CricShowException;

    /**
     * It retrieve the user object using emailAddress.
     *
     * @param emailAddress is a email address of user.
     * @return user - contains all credentials of user.
     * @throws CricShowException which is a custom exception.
     */
    User retrieveUserByEmailAddress(String emailAddress) throws 
            CricShowException;
    
    /**
     * It updates the login attempts made by user in the application.
     *
     * @param user - is a user object.
     * @throws CricShowException which is a custom exception.
     */
    void updateUser(User user) throws CricShowException;
}
