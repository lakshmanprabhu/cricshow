package com.ideas2it.cricshow.dao;

import java.util.List;

import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.exception.CricShowException;

/**
 * It is a link to player table for storing and manipulating its credentials.
 */
public interface PlayerDAO  {
    
    /**
     * It performs the insertion of player along with contact information in
     * the database..
     *
     * @param player stores the player credentials with contact information.
     * @return currentId is the id of a player created currently.
     * @throws CricShowException which is a custom exception.
     */
    int savePlayer(Player player) throws CricShowException;

    /**
     * It shows credentials of player as well as contact information using id.
     *
     * @param playerId is a id of a player.
     * @return player - contains all credentials and contact details of player.
     * @throws CricShowException which is a custom exception.
     */
    Player retrievePlayer(int playerId) throws CricShowException;

    /**
     * It fetches all players credentials from the database and send it to list.
     *
     * @param limit - to limit the number of players to be retrieved.
     * @param offset - to skip the number of players.
     * @return viewall - is a list of all player details .
     * @throws CricShowException which is a custom exception.
     */
    List<Player> retrieveAllPlayer(int limit, int offset) throws CricShowException;

    /**
     * It performs the updation of information such as name, date of birth,
     * country, role, batting type, bowling type, status, address, pincode,
     * phonenumber.
     *
     * @param player is a player object.
     * @throws CricShowException which is a custom exception.
     */
    void updatePlayer(Player player) throws CricShowException;
    
    /**
     * It deletes the player information from the database. 
     *
     * @param player- is a object to be deleted, which contains the information 
     * of player.
     * @throws CricShowException which is a custom exception.
     */
    void deletePlayer(Player player) throws CricShowException;
    
    /**
     * Finds the total number of player records in the database.
     *
     * @return count - the count of player records in the database.
     * @throws CricShowException which is a custom exception.
     */
    int playersTotalCount() throws CricShowException;
    
    /**
     * It retrieves the list of players with same country name .
     *
     * @param country - is a country from the list.
     * @return playersByCountry- is a list of players having same country.
     * @throws CricShowException which is a custom exception.
     */
    List<Player> retrievePlayersByCountry(String country) throws 
            CricShowException;
    
    /**
     * It shows credentials of player as well as team information using id.
     *
     * @param playerId is a id of a player.
     * @return player - contains all credentials and team details of player.
     * @throws CricShowException which is a custom exception.
     */
    Player retrievePlayerWithTeamById(int playerId) throws CricShowException;
              
    /**
     * It retrieves players with playersId to a team .
     *
     * @param playersId - contains multiple players id to be retrieved.
     * @return list of players to add in a team.
     * @throws CricShowException which is a custom exception.
     */
    List<Player> retrievePlayersToAddInTeam(List<Integer> playerIds) throws 
            CricShowException;
    
    /**
     * It shows list of players in a team using team id.
     *
     * @param teamId is a id of team where the players are added to it.
     * @return - list of players in team with information such as id, name
     * and role.
     * @throws CricShowException which is a custom exception.
     */
    List<Player> viewPlayersInTeam(int teamId) throws CricShowException;
    
    /**
     * Finds the total number of batsman in the database.
     *
     * @return batsmanCount - the count of player records having bastman role.
     * @throws CricShowException which is a custom exception.
     */
    int retrieveBatsmanCount(int teamId) throws CricShowException;
    
    /**
     * Finds the total number of bowler in the database.
     *
     * @return bowlerCount - the count of player records having bowler role.
     * @throws CricShowException which is a custom exception.
     */
    int retrieveBowlerCount(int teamId) throws CricShowException;
    
    /**
     * Finds the total number of batsman in the database.
     *
     * @return wicketKeeperCount - the count of player having wicketKeeper role.
     * @throws CricShowException which is a custom exception.
     */
    int retrieveWicketKeeperCount(int teamId) throws CricShowException;
    
    /**
     * It counts all players in a team using teamId.
     *
     * @param teamId - used to count no of players with a particular teamId.
     * @return total count of players in a team.
     * @throws CricShowException which is a custom exception.
     */
    int retrievePlayerCountInTeam(int teamId) throws CricShowException;
}
