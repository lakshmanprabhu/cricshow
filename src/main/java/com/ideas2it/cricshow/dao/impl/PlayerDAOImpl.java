package com.ideas2it.cricshow.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session; 
import org.hibernate.SessionException;   
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository; 

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.dao.PlayerDAO;
import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.entities.Team;
import com.ideas2it.cricshow.exception.CricShowException;

/**
 * It is a link to player table for storing and manipulating its credentials.
 */
@Repository
public class PlayerDAOImpl implements PlayerDAO  {
    private SessionFactory sessionFactory;
    private static final Logger log = Logger.getLogger(PlayerDAO.class);
    
    @Autowired
    public PlayerDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public int savePlayer(Player player) throws CricShowException {
        int currentId;
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        try {   
            currentId = (Integer) session.save(player); 
            transaction.commit();         
        } catch (HibernateException e) {
            log.error(Constants.LABEL_PLAYER_NOT_CREATED + e.getMessage());
            transaction.rollback();
            throw new CricShowException(
                    Constants.LABEL_ERROR_AT_CREATING_PLAYER, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }   
        }
        return currentId;
    }

    @Override
    public Player retrievePlayer(int playerId) throws CricShowException {        
        Player player;
        Session session = sessionFactory.openSession(); 
        try {
            player = (Player) session.get(Player.class, playerId);
        } catch (HibernateException e) {
            log.error(Constants.LABEL_ERROR_AT_DISPLAYING_PLAYER_WITH_ID
                    + playerId + e.getMessage());
            throw new CricShowException(Constants.LABEL_UNABLE_TO_VIEW_PLAYER, 
                    e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return player;
    }

    @Override
    public List<Player> retrieveAllPlayer(int limit, int offset) throws 
            CricShowException { 
        List<Player> viewAll = new ArrayList<Player>();
        Session session = sessionFactory.openSession(); 
        try {
            Criteria crit = session.createCriteria(Player.class);
            crit.setFirstResult(offset);
            crit.setMaxResults(limit);
            viewAll = (List) crit.list();  
        } catch (HibernateException e) {
            log.error(Constants.LABEL_ERROR_AT_DISPLAYING_PLAYER
                    + e.getMessage());
            throw new CricShowException(
                    Constants.LABEL_UNABLE_TO_VIEW_ALL_PLAYER, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }  
        return viewAll;
    }

    @Override
    public void updatePlayer(Player player) throws 
            CricShowException {    
        Session session = sessionFactory.openSession(); 
        Transaction transaction = session.beginTransaction();
        try {
            session.update(player);
            transaction.commit();
        } catch (HibernateException e) {
            log.error(Constants.LABEL_ERROR_AT_UPDATING_PLAYER
                    + e.getMessage());
            transaction.rollback();
            throw new CricShowException(
                    Constants.LABEL_UPDATION_FAILED_TO_PLAYER, e); 
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
    }
    
    @Override
    public void deletePlayer(Player player) throws CricShowException { 
        Session session = sessionFactory.openSession(); 
        Transaction transaction = session.beginTransaction();
        try { 
            session.delete(player);
            transaction.commit();
        } catch (HibernateException e) {
            log.error(Constants.LABEL_ERROR_AT_DELETING_PLAYER + player.getId() 
                    + e.getMessage());
            transaction.rollback();
            throw new CricShowException(
                    Constants.LABEL_FAILED_TO_DELETE_PLAYER , e); 
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
    }
    
    @Override
    public int playersTotalCount() throws CricShowException { 
        int playersCount;
        Session session = sessionFactory.openSession(); 
        try { 
            Query query = session.createQuery("select count(id) from Player");
            playersCount = ((Long)query.getSingleResult()).intValue();    
        } catch (HibernateException e) {
            log.error(Constants.LABEL_ERROR_AT_COUNTING_PLAYERS 
                    + e.getMessage());
            throw new CricShowException(Constants.LABEL_NO_PLAYERS_TO_COUNT, e);   
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return playersCount;
    }
    
    @Override
    public List<Player> retrievePlayersByCountry(String country) throws 
            CricShowException {
        List<Player> playersByCountry = new ArrayList<Player>();
        Session session = sessionFactory.openSession(); 
        String playerCreationQuery = new StringBuilder("FROM Player WHERE ")
                .append("country = :country and team_id is null or team_id = 0")
                .toString();
        try {
            Query query = session.createQuery(playerCreationQuery);
            query.setString("country", country);
            playersByCountry = query.list();
        } catch (HibernateException e) {
            log.error(Constants.LABEL_ERROR_AT_FINDING_PLAYERS_TO_A_COUNTRY
                    + country + e.getMessage());
            throw new CricShowException(
                    Constants.LABEL_NO_PLAYERS_FOUND_FOR_THIS_COUNTRY, e); 
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return playersByCountry;
    }
    
    @Override
    public Player retrievePlayerWithTeamById(int playerId) throws 
            CricShowException {        
        Player player;
        Player playerTeam = new Player();
        Session session = sessionFactory.openSession(); 
        try {
            player = (Player) session.get(Player.class, playerId);
            playerTeam.setTeam(player.getTeam());
        } catch (HibernateException e) {
            log.error(Constants.LABEL_ERROR_AT_DISPLAYING_PLAYER_WITH_ID
                    + playerId + e.getMessage());
            throw new CricShowException(Constants.LABEL_UNABLE_TO_VIEW_PLAYER, 
                    e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        if (null != playerTeam.getTeam()) {
            player.setTeam(playerTeam.getTeam());
        }
        return player;
    }
              
    @Override
    public List<Player> retrievePlayersToAddInTeam(List<Integer> playerIds) throws 
            CricShowException {
        List<Player> playersToAddInTeam = new ArrayList<Player>();
        Session session = sessionFactory.openSession(); 
        try {
            Query query = session.createQuery("FROM Player WHERE id in:playerIds");
            query.setParameterList("playerIds", playerIds);
            playersToAddInTeam = query.list();
        } catch (HibernateException e) {
            log.error(Constants.LABEL_ERROR_AT_FINDING_PLAYERS_TO_ADD_IN_TEAM
                    + e.getMessage());
            throw new CricShowException(
                    Constants.LABEL_NO_PLAYERS_AVAIL_TO_ADD_IN_TEAM, e);  
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return playersToAddInTeam;
    }
    
    @Override
    public List<Player> viewPlayersInTeam(int teamId) throws CricShowException { 
        List<Player> playersToAddInTeam = new ArrayList<Player>();
        Session session = sessionFactory.openSession(); 
        String playersInTeamQuery = new StringBuilder("FROM Player WHERE ")
                .append("team_id = :teamId")
                .toString();
        try {
            Query query = session.createQuery(playersInTeamQuery);
            query.setInteger("teamId", teamId);  
            playersToAddInTeam = query.list();
        } catch (HibernateException e) {
            log.error(Constants.LABEL_ERROR_AT_FINDING_PLAYERS_FOR_A_TEAM_ID 
                    + teamId + e.getMessage());
            throw new CricShowException(
                    Constants.LABEL_NO_PLAYERS_FOUND_FOR_THIS_TEAM, e);  
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return playersToAddInTeam;
    }
    
    @Override
    public int retrieveBatsmanCount(int teamId) throws CricShowException { 
        int batsmanCount;
        Session session = sessionFactory.openSession(); 
        String batsmanCountQuery = new StringBuilder("select count(id)")
                .append(" from Player where team_id = :teamId and role = :role")
                .toString();
        try { 
            Query query = session.createQuery(batsmanCountQuery);
            query.setParameter("teamId", teamId);       
            query.setString("role", Constants.LABEL_BATSMAN);
            batsmanCount = ((Long)query.getSingleResult()).intValue();    
        } catch (HibernateException e) {
            log.error(Constants.LABEL_ERROR_AT_COUNTING_BATSMAN_FOR_A_TEAM_ID 
                    + teamId + e.getMessage());
            throw new CricShowException(
                    Constants.LABEL_NO_BATSMAN_TO_COUNT_IN_THIS_TEAM, e);   
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return batsmanCount;
    }
    
    @Override
    public int retrieveBowlerCount(int teamId) throws CricShowException { 
        int bowlerCount;
        Session session = sessionFactory.openSession(); 
        String bowlerCountQuery = new StringBuilder("select count(id)")
                .append(" from Player where team_id = :teamId and role = :role")
                .toString();
        try { 
            Query query = session.createQuery(bowlerCountQuery);
            query.setParameter("teamId", teamId); 
            query.setString("role", Constants.LABEL_BOWLER);
            bowlerCount = ((Long)query.getSingleResult()).intValue();   
        } catch (HibernateException e) {
            log.error(Constants.LABEL_ERROR_AT_COUNTING_BOWLER_FOR_A_TEAM_ID 
                    + teamId + e.getMessage());
            throw new CricShowException(
                    Constants.LABEL_NO_BOWLER_TO_COUNT_IN_THIS_TEAM, e);   
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return bowlerCount;
    }
    
    @Override
    public int retrieveWicketKeeperCount(int teamId) throws CricShowException { 
        int wicketKeeperCount;
        Session session = sessionFactory.openSession(); 
        String wicketKeeperCountQuery = new StringBuilder("select count(id)")
                .append(" from Player where team_id = :teamId and role = :role")
                .toString();
        try { 
            Query query = session.createQuery(wicketKeeperCountQuery);
            query.setParameter("teamId", teamId); 
            query.setString("role", Constants.LABEL_WICKETKEEPER);
            wicketKeeperCount = ((Long)query.getSingleResult()).intValue();  
        } catch (HibernateException e) {
            log.error(
                    Constants.LABEL_ERROR_AT_COUNTING_WICKETKEEPER_FOR_A_TEAM_ID 
                    + teamId + e.getMessage());
            throw new CricShowException(
                    Constants.LABEL_NO_WICKETKEEPER_TO_COUNT_IN_THIS_TEAM, e);   
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return wicketKeeperCount;
    }
    
    @Override
    public int retrievePlayerCountInTeam(int teamId) throws CricShowException { 
        int playerCount;
        Session session = sessionFactory.openSession(); 
        String totalPlayerCountQuery = new StringBuilder("select count(id)")
                .append("from Player where team_id = :teamId")
                .toString();
        try { 
            Query query = session.createQuery(totalPlayerCountQuery);
            query.setParameter("teamId", teamId);
            playerCount = ((Long)query.getSingleResult()).intValue();   
        } catch (HibernateException e) {
            log.error(Constants.LABEL_ERROR_AT_COUNTING_TOTAL_PLAYERS_FOR_A_TEAM 
                    + teamId + e.getMessage());
            throw new CricShowException(
                    Constants.LABEL_NO_PLAYERS_TO_COUNT_IN_THIS_TEAM, e);   
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return playerCount;
    }
}
