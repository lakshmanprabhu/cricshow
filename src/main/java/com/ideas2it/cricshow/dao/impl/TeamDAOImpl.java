package com.ideas2it.cricshow.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionException;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.dao.TeamDAO;
import com.ideas2it.cricshow.entities.Match;
import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.entities.Team;
import com.ideas2it.cricshow.exception.CricShowException;

/**
 * It is a link to team database and teamDAO object.
 */
@Repository
public class TeamDAOImpl implements TeamDAO {
    private SessionFactory sessionFactory; 
    private static final Logger log = Logger.getLogger(TeamDAO.class);
    
    @Autowired
    public TeamDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public int createTeam(Team team) throws CricShowException { 
        int id;
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        try {
            id = (Integer) session.save(team); 
            transaction.commit();      
        } catch (HibernateException e) {
            log.error("Team cannot be created " + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Error at creating team", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }   
        return id;
   }  
   
    @Override
    public List<Team> retrieveAllTeams() throws CricShowException {
        List<Team> allTeams = new ArrayList<Team>();
        Session session = sessionFactory.openSession();
        try {
            allTeams = session.createQuery("FROM Team ").list();  
        } catch (HibernateException e) {
            log.error("Error at viewing all teams " + e.getMessage());
            throw new CricShowException("Unable to view all teams :", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return allTeams;
    }
    
    @Override
    public Team retrieveTeam(int teamId) throws CricShowException {  
        Team team;
        Session session = sessionFactory.openSession();  
        try {
            team = (Team) session.get(Team.class, teamId);
        } catch (HibernateException e) {
            log.error("Error at retrieving team with id " + teamId 
                    + e.getMessage());
            throw new CricShowException("Unable to view team ", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return team;
    }
    
    @Override
    public Team retrievePlayersFromTeam(int teamId) throws CricShowException {  
        Team team;
        Set<Player> playersInTeam = new HashSet<Player>();
        Session session = sessionFactory.openSession();  
        try {
            team = (Team) session.get(Team.class, teamId);
            playersInTeam.addAll(team.getPlayers());
        } catch (HibernateException e) {
            log.error("Error at retrieving players in a team with id " + teamId 
                    + e.getMessage());
            throw new CricShowException("Unable to view players in a team ", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        team.setPlayers(playersInTeam);
        return team; 
    }
    
    @Override
    public Team retrieveTeamForMatch(int teamId) throws CricShowException {  
        Team team;
        Set<Match> matchesInTeam = new HashSet<Match>();
        Session session = sessionFactory.openSession();  
        try {
            team = (Team) session.get(Team.class, teamId);
            matchesInTeam.addAll(team.getMatches());
        } catch (HibernateException e) {
            log.error("Error at retrieving team with id" + teamId + "to a match"  
                    + e.getMessage());
            throw new CricShowException("Unable to view team :", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        team.setMatches(matchesInTeam);
        return team;
    }

    @Override
    public void deleteTeam(int teamId) throws CricShowException { 
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        try {
            Team team = (Team) session.get(Team.class, teamId);
            session.delete(team);
            transaction.commit();
        } catch (HibernateException e) {
            log.error("Deleting failed to team Id" + teamId + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Failed to delete team ", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
    }
    
    @Override
    public void updateTeam(Team team) throws CricShowException {
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        try {
            session.update(team);
            transaction.commit();
        } catch (HibernateException e) {
            log.error("Updating team failed" + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Failed to update team ", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
    }
}
