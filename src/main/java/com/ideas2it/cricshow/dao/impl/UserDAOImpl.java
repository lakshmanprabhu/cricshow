package com.ideas2it.cricshow.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.dao.UserDAO;
import com.ideas2it.cricshow.entities.User;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.exception.UserAlreadyExistException;

/**
 * It is a link to user table to store user credentials.
 */
@Repository
public class UserDAOImpl implements UserDAO {
    private SessionFactory sessionFactory;
    private static final Logger log = Logger.getLogger(UserDAO.class);
    
    @Autowired
    public UserDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public int createUser(User user) throws CricShowException {
        int currentId;
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        try {   
            currentId = (Integer) session.save(user); 
            transaction.commit();         
        } catch (HibernateException e) {
            log.error("User cannot be created " + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Error at creating user", e);
        } catch (Exception e) {
            log.error("User cannot be created " + e.getMessage());
            transaction.rollback();
            throw new UserAlreadyExistException("user already existed");
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return currentId;
    }

    @Override
    public User retrieveUserByEmailAddress(String emailAddress) throws 
            CricShowException {        
        User user = null;
        String userByEmailAddressQuery = 
                "FROM User where email_address=:emailAddress";
        Session session = sessionFactory.openSession(); 
        try {
            Query query = session.createQuery(userByEmailAddressQuery);
            query.setString("emailAddress", emailAddress);
            List userList = query.list();
            if (!userList.isEmpty() && null != userList) {
                user = (User) userList.get(0);
            }
        } catch (HibernateException e) {
            log.error("Error at retrieving user for" + emailAddress 
                    + e.getMessage());
            throw new CricShowException("Unable to retrieve user :", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return user;
    }
    
    @Override
    public void updateUser(User user) throws CricShowException {    
        Session session = sessionFactory.openSession(); 
        Transaction transaction = session.beginTransaction();
        try {
            session.update(user);
            transaction.commit();
        } catch (HibernateException e) {
            log.error("Updating failed to user" + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Failed to update user ", e); 
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
    }
}
