package com.ideas2it.cricshow.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionException;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.dao.OverDAO;
import com.ideas2it.cricshow.entities.Over;
import com.ideas2it.cricshow.exception.CricShowException;

/**
 * It is a link to over database for manipulating over in it.
 */
@Repository
public class OverDAOImpl implements OverDAO {
    private SessionFactory sessionFactory;
    private static final Logger log = Logger.getLogger(OverDAO.class);

    @Autowired
    public OverDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }    

    @Override
    public int retrieveOverNumber(int matchId) throws CricShowException { 
        int overNumber = 0;
        String previousOverCountQuery = new StringBuilder("select max(overNumber)")
                .append("from Over where match_id = :matchId")
                .toString();
        Session session = sessionFactory.openSession();   
        try {
            Query query = session.createQuery(previousOverCountQuery);
            query.setParameter("matchId", matchId);
            if (null != query.getSingleResult()) {
                overNumber = ((Integer)query.getSingleResult()).intValue();
                overNumber = overNumber;
            }
        } catch (HibernateException e) {
            log.error("Max of over number is not available " + e.getMessage());
            throw new CricShowException("Error at retrieving over number ", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return overNumber;
    }  
    
    @Override
    public int createOver(Over over) throws CricShowException { 
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        int overId;
        try {
            overId = (Integer) session.save(over); 
            transaction.commit();         
        } catch (HibernateException e) {
            log.error("Over cannot be created " + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Error at Over match", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return overId;
    }
    
    @Override
    public Over retrieveOverById(int overId) throws CricShowException {  
        Over over = null; 
        Session session = sessionFactory.openSession(); 
        try {
            over = (Over) session.get(Over.class, overId);
        } catch (HibernateException e) {
            log.error("Error at retrieving match for Id" + overId 
                    + e.getMessage());
            throw new CricShowException("Unable to retrieve over :", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return over;        
    } 
    
    @Override
    public void deleteOver(int overId) throws CricShowException { 
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        try {
            Over over = (Over) session.get(Over.class, overId);
            session.delete(over);
            transaction.commit();
        } catch (HibernateException e) {
            log.error("Deleting failed to a over with Id" + overId 
                    + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Failed to delete over ", e); 
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
    }
    
    @Override
    public void updateOver(Over over) throws CricShowException {
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        try {
            session.update(over);
            transaction.commit();
        } catch (HibernateException e) {
            log.error("Updating failed to a over" + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Failed to update over ", e); 
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
    }          
}
