package com.ideas2it.cricshow.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionException;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.dao.MatchDAO;
import com.ideas2it.cricshow.entities.Match;
import com.ideas2it.cricshow.exception.CricShowException;

/**
 * It is a link to matches database for manipulating match in it.
 */
@Repository
public class MatchDAOImpl implements MatchDAO {
    private SessionFactory sessionFactory;
    private static final Logger log = Logger.getLogger(MatchDAO.class);

    @Autowired
    public MatchDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }    

    @Override
    public int createMatch(Match match) throws CricShowException { 
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        int id;
        try {
            id = (Integer) session.save(match); 
            transaction.commit();         
        } catch (HibernateException e) {
            log.error("Match cannot be created " + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Error at creating match", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return id;
   }  
   
    @Override
    public List<Match> retrieveAllMatches() throws CricShowException {
        List<Match> allMatches = new ArrayList<Match>();
        Session session = sessionFactory.openSession();  
        try { 
            allMatches = session.createQuery("FROM Match ").list();  
        } catch (HibernateException e) {
            log.error("Error at viewing all matches" + e.getMessage());
            throw new CricShowException("Unable to view all matches :", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return allMatches;
    }
    
    @Override
    public Match retrieveMatchById(int matchId) throws CricShowException {  
        Match match = new Match(); 
        Session session = sessionFactory.openSession(); 
        try {
            match = (Match) session.get(Match.class, matchId);
        } catch (HibernateException e) {
            log.error("Error at retrieving match for Id" + matchId 
                    + e.getMessage());
            throw new CricShowException("Unable to retrieve match :", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return match;
        
    }

    @Override
    public void deleteMatch(int matchId) throws CricShowException { 
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        try {
            Match match = (Match) session.get(Match.class, matchId);
            session.delete(match);
            transaction.commit();
        } catch (HibernateException e) {
            log.error("Deleting failed to a match with Id" + matchId 
                    + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Failed to delete match ", e); 
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
    }
    
    @Override
    public void updateMatch(Match match) throws CricShowException {
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        try {
            session.update(match);
            transaction.commit();
        } catch (HibernateException e) {
            log.error("Updating failed to a match" + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Failed to update match ", e); 
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
    }
}
