package com.ideas2it.cricshow.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionException;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.dao.OverDetailDAO;
import com.ideas2it.cricshow.entities.Over;
import com.ideas2it.cricshow.entities.OverDetail;
import com.ideas2it.cricshow.exception.CricShowException;

/**
 * It is a link to over database for manipulating over in it.
 */
@Repository
public class OverDetailDAOImpl implements OverDetailDAO {
    private SessionFactory sessionFactory;
    private static final Logger log = Logger.getLogger(OverDetailDAO.class);

    @Autowired
    public OverDetailDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }    

    @Override
    public int retrieveBallNumber(int matchId, int overId) throws CricShowException { 
        int ballNumber = 0;
        String previousBallCountQuery = new StringBuilder("select max(ballNumber)")
                .append("from OverDetail where match_id = :matchId ")
                .append("and over_id = :overId")
                .toString();
        Session session = sessionFactory.openSession();   
        try {
            Query query = session.createQuery(previousBallCountQuery);
            query.setParameter("matchId", matchId);
            query.setParameter("overId", overId);            
            if (null != query.getSingleResult()) {
                ballNumber = ((Integer)query.getSingleResult()).intValue();
            }
        } catch (HibernateException e) {
            log.error("Max of over number is not available " + e.getMessage());
            throw new CricShowException("Error at retrieving over number ", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return ballNumber;
    }  
    
    @Override
    public int createOverDetail(OverDetail overDetail) throws CricShowException { 
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        int overDetailId;
        try {
            overDetailId = (Integer) session.save(overDetail); 
            transaction.commit();         
        } catch (HibernateException e) {
            log.error("OverDetail cannot be created " + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Error at creating OverDetail in a play",
                    e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return overDetailId;
    }
    
    @Override
    public OverDetail retrieveOverDetailById(int overDetailId) throws 
            CricShowException {  
        OverDetail overDetail = null; 
        Session session = sessionFactory.openSession(); 
        try {
            overDetail = (OverDetail) session.get(OverDetail.class, 
                               overDetailId);
        } catch (HibernateException e) {
            log.error("Error at retrieving overDetail for Id " + overDetailId 
                    + e.getMessage());
            throw new CricShowException("Unable to retrieve overDetail :", e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
        return overDetail;        
    } 
    
    @Override
    public void deleteOver(int overId) throws CricShowException { 
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        try {
            Over over = (Over) session.get(Over.class, overId);
            session.delete(over);
            transaction.commit();
        } catch (HibernateException e) {
            log.error("Deleting failed to a over with Id " + overId 
                    + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Failed to delete over ", e); 
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
    }
    
    @Override
    public void updateOver(Over over) throws CricShowException {
        Session session = sessionFactory.openSession();  
        Transaction transaction = session.beginTransaction();
        try {
            session.update(over);
            transaction.commit();
        } catch (HibernateException e) {
            log.error("Updating failed to a over" + e.getMessage());
            transaction.rollback();
            throw new CricShowException("Failed to update over ", e); 
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                log.error(Constants.LABEL_ERROR_IN_CLOSING_SESSION 
                        + e.getMessage());
            }
        }
    }          
}
