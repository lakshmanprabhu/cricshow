package com.ideas2it.cricshow.dao;

import java.util.List;

import com.ideas2it.cricshow.entities.Team;
import com.ideas2it.cricshow.exception.CricShowException;

/**
 * It is a link to team database for manipulating team in it.
 */
public interface TeamDAO {
    
    /**
     * It adds information such as team id, name, country and status in a team.
     *
     * @param team - it contains information of a team.
     * @return currentId is the id of a team created currently.
     * @throws CricShowException which is a custom exception.
     */
    int createTeam(Team team) throws CricShowException;  
   
    /**
     * It fetches all team details from the database and send it as a list.
     *
     * @return allTeams - is a list of all teams details .
     * @throws CricShowException which is a custom exception.
     */
    List<Team> retrieveAllTeams() throws CricShowException;
    
    /**
     * It retrieve team details from the database using teamId.
     *
     * @param teamId - is a id present in database.
     * @return team - is a team detail consisting of id, country and status.
     * @throws CricShowException which is a custom exception.
     */
    Team retrieveTeam(int teamId) throws CricShowException;
    
    /**
     * It retrieve players from a team in the database using teamId.
     *
     * @param teamId - is a id present in database.
     * @return team - is a team detail consisting of id, country, status and set
     * of players in a team .
     * @throws CricShowException which is a custom exception.
     */
    Team retrievePlayersFromTeam(int teamId) throws CricShowException;
    
    /**
     * It retrieve team details for match from the database using teamId.
     *
     * @param teamId - is a id of a team used to retrieve team details.
     * @return team - is a team detail consisting of id, country, status and 
     * matches in team.
     * @throws CricShowException which is a custom exception.
     */
    Team retrieveTeamForMatch(int teamId) throws CricShowException;

    /**
     * It deletes the team information from the database using teamId. 
     *
     * @param teamId - is the id of a team present in the database.
     * @throws CricShowException which is a custom exception.
     */
    void deleteTeam(int teamId) throws CricShowException;
    
    /**
     * It updates team information such as name, addplayers and remove players
     * in the database.
     *
     * @param team - contains information of a team.
     * @throws CricShowException which is a custom exception.
     */
    void updateTeam(Team team) throws CricShowException;
}
