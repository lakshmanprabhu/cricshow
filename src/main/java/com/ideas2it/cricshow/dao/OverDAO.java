package com.ideas2it.cricshow.dao;

import com.ideas2it.cricshow.entities.Over;
import com.ideas2it.cricshow.exception.CricShowException;

/**
 * It is a link to matches database for manipulating match in it.
 */
public interface OverDAO {
    
    /**
     * It retrieves the current over number from the database. If over number 
     * gives null then there is no over number available for that match and is
     * is set as zeroth over otherwise adds plus one with previous over number.
     *
     * @param matchId - it is used to check in a match whether the over is there
     * or not. 
     * @return overNumber - is the previous over number present in database.
     * @throws CricShowException which is a custom exception.
     */
    int retrieveOverNumber(int matchId) throws CricShowException; 

    /**
     * It creates a over for a match to be played with information such as match,
     * over number and returns the over id to the service.
     * 
     * @param over - is a over object containing the information of a over.
     * @return overId - is a id of over object created.
     */    
    int createOver(Over over) throws CricShowException;
    
    /**
     * It retrieve over details from the database using overId.
     *
     * @param overId - is a over id present in database.
     * @return over - is a over detail consisting of id, match, over number,
     * six count, four count, wicket count, extras and total runs in a over. 
     * @throws CricShowException which is a custom exception.
     */
    Over retrieveOverById(int overId) throws CricShowException;    
     
    /**
     * It deletes the over information from the database. 
     *
     * @param overId - is the id of a over present in the database.
     * @throws CricShowException which is a custom exception.
     */
    void deleteOver(int overId) throws CricShowException;    
    
    /**
     * It updates information of over in the database.
     *
     * @param over - contains information of a over.
     * @throws CricShowException which is a custom exception.
     */
    void updateOver(Over over) throws CricShowException; 
}
