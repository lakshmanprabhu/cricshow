package com.ideas2it.cricshow.controller;

import java.text.ParseException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;    
import org.springframework.web.bind.annotation.ModelAttribute;      
import org.springframework.web.bind.annotation.RequestMapping;    
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam;   
import org.springframework.ui.Model;

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.TeamInfo;
import com.ideas2it.cricshow.info.PlayerInfo;
import com.ideas2it.cricshow.service.TeamService;

/**
 * It provides the controller functions to a team.
 *
 * @author    Lakshmanprabhu Naidu
 *
 * @date    20/08/2019
 */
@Controller
public class TeamController {
    private TeamService teamService;
    private static final Logger log = Logger.getLogger(TeamController.class);
    
    @Autowired
    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }
    
    /**
     * It provides the team creation form which includes team name and country.
     * 
     * @param model - which is used to add team object to it.
     * @return the addTeam view to the user with team model.
     */
    @RequestMapping(value = Constants.LABEL_CREATE_TEAM,
            method = RequestMethod.GET)
    private String showTeamForm(Model model) {
        model.addAttribute(Constants.LABEL_TEAM_INFO, new TeamInfo());  
        return Constants.LABEL_ADD_TEAM_PAGE;
    }
    
    /**
     * It a creation menu to which the user creates a team with its 
     * credentials .
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output. 
     */
    @RequestMapping(value = Constants.LABEL_SAVE_TEAM,
            method = RequestMethod.POST)
    private String createTeam(@ModelAttribute(Constants.LABEL_TEAM_INFO) 
            TeamInfo teamInfo, Model model) {
        try {  
            String country = teamInfo.getCountry().toString();
            List<PlayerInfo> playersByCountry = teamService.fetchPlayersByCountry(
                                                  country);
            model.addAttribute(Constants.LABEL_TEAM_INFO, teamInfo);
            model.addAttribute(Constants.LABEL_PLAYERS_BY_COUNTRY,
                    playersByCountry);
        } catch (CricShowException | ParseException e) {
            log.error(Constants.LABEL_ERROR_AT_CREATING_TEAM + e.getMessage());
        }   
        return Constants.LABEL_ADD_PLAYERS_TO_TEAM_PAGE; 
    }

    /**
     * It adds players available to form a team.
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output.  
     */
    @RequestMapping(value = Constants.LABEL_ADD_PLAYERS_TO_TEAM,
            method = RequestMethod.POST)
    private String addPlayers(@ModelAttribute(Constants.LABEL_TEAM_INFO) 
            TeamInfo teamInfo, @RequestParam(name = Constants.LABEL_ADD_PLAYERS,
            required = false) String[] ids, Model model) {
        try {
            teamInfo = teamService.addTeam(teamInfo, ids);
            model.addAttribute(Constants.LABEL_ID, teamInfo.getId());
            model.addAttribute(Constants.LABEL_COUNTRY, teamInfo.getCountry());
        } catch (CricShowException e) {
            log.error(Constants.LABEL_ERROR_AT_ADDING_PLAYER_TO_TEAM 
                    + e.getMessage());
        }
        return Constants.LABEL_REDIRECT_VIEW_TEAM;
    }
    
    /**
     * It displays the team with players in it.
     *
     * @param teamId - is a used to display player credentials.
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output.  
     */
    @RequestMapping(value = Constants.LABEL_VIEW_TEAM,
            method = RequestMethod.GET)
    private String viewTeam(Model model, @RequestParam(Constants.LABEL_ID) 
            int teamId, @RequestParam(Constants.LABEL_COUNTRY) 
            String country) {
        try {
            TeamInfo teamInfo = teamService.fetchTeamInfo(teamId, country);
            model.addAttribute(Constants.LABEL_TEAM_INFO, teamInfo);
        } catch (CricShowException | ParseException e) {
            log.error(Constants.LABEL_ERROR_AT_DISPLAYING_TEAM_WITH_ID + teamId 
                    + e.getMessage());
        } 
        return Constants.LABEL_VIEW_TEAM_PAGE;
    }
    
    /**
     * It provides user to delete a team.
     * It asks user to enter team id and delete it from database.
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output.  
     */
    @RequestMapping(value = Constants.LABEL_DELETE_TEAM,
            method = RequestMethod.GET)
    private String deleteTeam(Model model, @RequestParam(Constants.LABEL_ID) 
            int teamId) {
        try {
            teamService.removeTeam(teamId);
        } catch (CricShowException e) {
            log.error(Constants.LABEL_ERROR_AT_DELETING_TEAM + e.getMessage());
        }
        return Constants.LABEL_REDIRECT_VIEW_TEAMS;
    }
    
    /**
     * It displays credentials of all teams present in the database alongwith
     * the players in it. 
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output. 
     */
    @RequestMapping(value = Constants.LABEL_VIEW_TEAMS,
            method = RequestMethod.GET)
    private String viewAllTeam(Model model) {
        try {
            List<TeamInfo> retrievedTeams = teamService.fetchAllTeams();
            model.addAttribute("viewTeams", retrievedTeams);
        } catch (CricShowException e) {
            log.error("Error at viewing all teams" + e.getMessage());
        }    
        return Constants.LABEL_VIEW_TEAMS_PAGE;
    }  
    
    /**
     * It provides information of a team such as name, country, players in team
     * and players not in team. 
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output. 
     */
    @RequestMapping(value = Constants.LABEL_UPDATE_TEAM,
            method = RequestMethod.GET)
    private String updateTeam(Model model, @RequestParam(Constants.LABEL_ID) 
            int teamId, @RequestParam(Constants.LABEL_COUNTRY) 
            String country) { 
        try {
            TeamInfo teamInfo = teamService.fetchTeamInfo(teamId, country);
            model.addAttribute(Constants.LABEL_TEAM_INFO, teamInfo);
        } catch (CricShowException | ParseException e) {
            log.error("Error at viewing team details " + e.getMessage()); 
        }
        return Constants.LABEL_UPDATE_TEAM_PAGE;
    }
    
    /**
     * Saves the edited information of a team information such as name, add
     * players or remove players in a team. 
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output. 
     */
    @RequestMapping(value = Constants.LABEL_EDIT_TEAM, 
            method = RequestMethod.POST)
    private String saveUpdatedInformation(@ModelAttribute(Constants.LABEL_TEAM_INFO)  
            TeamInfo teamInfo, @RequestParam(name = Constants.LABEL_ADD_PLAYERS, 
            required = false) String[] addIds, @RequestParam(name = 
            Constants.LABEL_REMOVE_PLAYERS, required = false) 
            String[] removeIds) { 
        try {
            teamService.updateTeamInformation(teamInfo, addIds, removeIds);
        } catch (CricShowException e) {
            log.error("Error at saving updated information " + e.getMessage());
        }
        return "redirect:/viewTeams";
    }
}
