package com.ideas2it.cricshow.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.json.JSONArray;   
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;    
import org.springframework.web.bind.annotation.ModelAttribute;      
import org.springframework.web.bind.annotation.RequestMapping;    
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam;   
import org.springframework.ui.Model;

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.MatchOverInfo;
import com.ideas2it.cricshow.service.OverDetailService;

/**
 * It provides the controller functions to a OverDetail.
 *
 * @author    Lakshmanprabhu Naidu
 *
 * @date    26/08/2019
 */
@Controller
public class OverDetailController {
    private OverDetailService overDetailService;
    private static final Logger log = Logger.getLogger(OverDetailController.class);

    @Autowired
    public OverDetailController(OverDetailService overDetailService) {
        this.overDetailService = overDetailService;
    }

    /**
     * It is a creation menu to which the user creates a over to a match.
     *
     * @param batsmenIds - is a ids of batsmen in a match who is going to play. 
     * @param bowlerId - is a id of bowler in a match who is going to play.
     * @param matchId - is a id of match for which the play is going. 
     * @param model - is used to display the created over in a view.
     * @throws CricShowException - is a custom exception which gives appropriate
     * context message to the user.  
     */
    @RequestMapping(value = "/startOver",
            method = RequestMethod.GET)
    private void createOverDetail(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        try {  
            int matchId = Integer.parseInt(request.getParameter("matchId"));
            System.out.println("match " + matchId);
            int overId = Integer.parseInt(request.getParameter("overId"));
            System.out.println("over " + overId);
            int bowlerId = Integer.parseInt(request.getParameter("bowlerId"));
            System.out.println("bowler " + bowlerId);
            int batsmanId = Integer.parseInt(request.getParameter("batsmanId"));
            System.out.println("batsman " + batsmanId);
            JSONArray playDetails = overDetailService.addOverDetails(matchId, 
                                          overId, bowlerId, batsmanId);
            response.setContentType(Constants.LABEL_APPLICATION_TYPE_JSON);
            response.getWriter().write(playDetails.toString());
        } catch (CricShowException e) {
            log.error("Error at creating over " + e.getMessage());
        }   
    }
}
