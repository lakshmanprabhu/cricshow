package com.ideas2it.cricshow.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;    
import org.springframework.web.bind.annotation.ModelAttribute;      
import org.springframework.web.bind.annotation.RequestMapping;    
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam;   
import org.springframework.ui.Model;

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.MatchInfo;
import com.ideas2it.cricshow.info.PlayInfo;
import com.ideas2it.cricshow.service.MatchService;

/**
 * It provides the controller functions to a match.
 *
 * @author    Lakshmanprabhu Naidu
 *
 * @date    20/08/2019
 */
@Controller
public class MatchController {
    private MatchService matchService;
    private static final Logger log = Logger.getLogger(MatchController.class);

    @Autowired
    public MatchController(MatchService matchService) {
        this.matchService = matchService;
    }
    
    /**
     * It provides the match creation form which includes match name ,venue,
     * playingDate and playingFormat. 
     *
     * @param model - which is used to add match object to it.
     * @return the addMatch view to the user with match model.
     */
    @RequestMapping(value = Constants.LABEL_CREATE_MATCH,
            method = RequestMethod.GET)
    private String showMatchForm(Model model) {
        model.addAttribute("matchInfo", new MatchInfo());  
        return Constants.LABEL_ADD_MATCH_PAGE;
    }
    

    /**
     * It is a creation menu to which the user creates a match with its 
     * credentials .
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output.  
     */
    @RequestMapping(value = Constants.LABEL_SAVE_MATCH,
            method = RequestMethod.POST)
    private String createMatch(@ModelAttribute("matchInfo") 
            MatchInfo matchInfo, @RequestParam("dateOfPlaying") String 
            playingDate, Model model) {
        try {  
            matchInfo = matchService.addMatch(matchInfo, playingDate);
            model.addAttribute("matchInfo", matchInfo);
            model.addAttribute("dateOfPlaying", playingDate);
        } catch (CricShowException | ParseException| IOException e) {
            log.error("Error at creating match " + e.getMessage());
        }   
        return "addTeamsToMatch";
    }
    
    /**
     * It adds two team for a match from a list of teams available. 
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output.  
     */
    @RequestMapping(value = "/addTeamsToMatch",
            method = RequestMethod.POST)
    private String addTeams(@ModelAttribute("matchInfo") 
            MatchInfo matchInfo,
            @RequestParam(name = "addteam", required = false)
            String[] ids, @RequestParam("dateOfPlaying") String playingDate,
            Model model) {
        try {
            matchService.addTeams(matchInfo, ids, playingDate);
            model.addAttribute("id", matchInfo.getId());
        } catch (ParseException | CricShowException | IOException e) {
            log.error("Error at adding teams in a match " + e.getMessage());
        }
        return "redirect:/viewMatch";
    }
     
    /**
     * It displays the match information from the match.
     *
     * @param matchId - is a used to display match credentials.
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output.  
     */
   @RequestMapping(value = "/viewMatch",
            method = RequestMethod.GET)
   private String viewMatch(Model model, @RequestParam(Constants.LABEL_ID) 
            int matchId) {
        try {
            MatchInfo matchInfo = matchService.fetchMatchById(matchId);
            model.addAttribute("matchInfo", matchInfo);
        } catch (CricShowException e) {
            log.error("Error at viewing a match with id " + matchId 
                    + e.getMessage());
        }  
        return "viewMatch";
    }
    
    /**
     * It provides user to delete a match.
     * It asks user to enter match id and delete it from database.
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output.  
     */
    @RequestMapping(value = "/deleteMatch",
            method = RequestMethod.GET)
    private String deleteMatch(@RequestParam(Constants.LABEL_ID) 
            int matchId) {
        try {
            matchService.removeMatch(matchId);
        } catch (CricShowException e) {
            log.error("Error at deleting match" + e.getMessage());
        }
        return "redirect:/viewMatches";
    }
   
    /**
     * It displays information of all matches present in the database. 
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output. 
     */
    @RequestMapping(value = "/viewMatches",
            method = RequestMethod.GET)
    private String viewAllMatches(Model model) {
        try {
            List<MatchInfo> viewMatches = matchService.fetchAllMatches();
            model.addAttribute("viewmatch", viewMatches);
        } catch (CricShowException e) {
            log.error("Error at viewing all matches" + e.getMessage());
        } 
        return "viewMatches";  
    }
    
    /**
     * It provides information of a match such as name, venue, playingformat,
     * playingformat and teams in a match. 
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output. 
     */
    @RequestMapping(value = "/updateMatch",
            method = RequestMethod.GET)
    private String updateMatch(Model model, @RequestParam(Constants.LABEL_ID) 
            int matchId) { 
        try {
            MatchInfo matchInfo = matchService.fetchMatchForUpdate(matchId);
            model.addAttribute("matchInfo", matchInfo);                        
        } catch (CricShowException e) {
            log.error("Error at viewing match information" + e.getMessage());
        }
        return "updateMatch";
    }
    
    /**
     * Saves the edited information of a match information such as name, add
     * teams, venue, playingdate, playingdate in a team. 
     * @RequestParam(name = "removeTeams",
            required = false) String[] removeIds,
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output.  
     */
    @RequestMapping(value = "/editMatch", 
            method = RequestMethod.POST)
    private String saveUpdatedInformation(@ModelAttribute("matchInfo") 
            MatchInfo matchInfo , @RequestParam(name = "addteam",  
            required = false) String[] ids, @RequestParam("dateOfPlaying")
            String playingDate, Model model) { 
        try {
            matchService.editMatch(matchInfo, ids, playingDate);
        } catch (CricShowException | ParseException | IOException e) {
            log.error("Error at updating match information" + e.getMessage());
        }
        return "redirect:/viewMatches";
    }
    
    /**
     * It displays the match which is to be played between two teams by 
     * displaying players in each team and selecting batsman and bowler role
     * in respective teams. 
     *
     * @param model - is a model to display the players available in each teams. 
     * @param matchId - is a matchId for which the play has to be started.
     * @return the starting of match view.
     */
    @RequestMapping(value = "/startMatch",
            method = RequestMethod.GET)
    private String startMatch(Model model, @RequestParam(Constants.LABEL_ID) 
            int matchId) { 
        try {
            PlayInfo playInfo = matchService.fetchPlayInfoForMatch(matchId);
            model.addAttribute("playInfo", playInfo);                        
        } catch (CricShowException | ParseException e) {
            log.error("Error at viewing match information" + e.getMessage());
        }
        return "startMatch";
    }
}
