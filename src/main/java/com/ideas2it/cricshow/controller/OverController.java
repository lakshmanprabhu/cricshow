package com.ideas2it.cricshow.controller;

import java.text.ParseException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;    
import org.springframework.web.bind.annotation.ModelAttribute;      
import org.springframework.web.bind.annotation.RequestMapping;    
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam;   
import org.springframework.ui.Model;

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.MatchOverInfo;
import com.ideas2it.cricshow.info.PlayerSelectionInfo;
import com.ideas2it.cricshow.service.OverService;

/**
 * It provides the controller functions to a Over.
 *
 * @author    Lakshmanprabhu Naidu
 *
 * @date    26/08/2019
 */
@Controller
public class OverController {
    private OverService overService;
    private static final Logger log = Logger.getLogger(OverController.class);

    @Autowired
    public OverController(OverService overService) {
        this.overService = overService;
    }

    /**
     * It is a creation menu to which the user creates a over to a match.
     *
     * @param batsmenIds - is a ids of a batsmen in a match who is going to play. 
     * @param bowlerId - is a id of bowler in a match who is going to play.
     * @param matchId - is a id of match for which the play is going. 
     * @param model - is used to display the created over in a view.
     * @return a view containing the information of a over.
     */
    @RequestMapping(value = "/createOver",
            method = RequestMethod.POST)
    private String createOver(@RequestParam("addBatsmen") String[] batsmenIds, 
            @RequestParam("addBowler") int bowlerId, @RequestParam("matchId") 
            int matchId, Model model) {
        try {  
            MatchOverInfo matchOverInfo = overService.addOver(batsmenIds, 
                                                bowlerId, matchId);
            model.addAttribute("matchOverInfo", matchOverInfo);
        } catch (CricShowException e) {
            log.error("Error at creating over " + e.getMessage());
        }   
        return "playMatch";
    }
    
    /**
     * It creates a next over if the over has came to end in a match.
     *
     * @param firstBatsmanId - is a id of firstBatsman who is playing.
     * @param secondBatsmanId - is a id of secondBatsman who is playing.  
     * @param bowlerId - is a id of bowler who is playing.
     * @param matchId - is a id of match for which the play is going. 
     * @param model - is used to display the created next over in a view. 
     * @return a view containing the next over details.
     */
    @RequestMapping(value = "/nextOver",
            method = RequestMethod.POST)
    private String createNextOver(@RequestParam("firstBatsmanId") 
            int firstBatsmanId, @RequestParam("secondBatsmanId") 
            int secondBatsmanId, @RequestParam("addBowler") int bowlerId, 
            @RequestParam("matchId") int matchId, Model model) {
        try {            
            MatchOverInfo matchOverInfo = overService.addNextOver(firstBatsmanId, 
                                                secondBatsmanId, bowlerId, 
                                                matchId);
            model.addAttribute("matchOverInfo", matchOverInfo);
        } catch (CricShowException e) {
            log.error("Error at creating over " + e.getMessage());
        }   
        return "playMatch";
    }
    
    /**
     * It creates a next over if the over has came to end in a match.
     *
     * @param firstBatsmanId - is a id of firstBatsman who is playing.
     * @param secondBatsmanId - is a id of secondBatsman who is playing.  
     * @param bowlerId - is a id of bowler who is playing.
     * @param matchId - is a id of match for which the play is going. 
     * @param model - is used to display the created next over in a view. 
     * @retrun a view containg the over detail which is to be resumed. 
     */
    @RequestMapping(value = "/resumeOver",
            method = RequestMethod.POST)
    private String createNextOver(@RequestParam("addBatsman") 
            int strikeBatsmanId, @RequestParam("secondBatsmanId") 
            int secondBatsmanId, @RequestParam("bowlerId") int bowlerId, 
            @RequestParam("matchId") int matchId, @RequestParam("overId") 
            int overId, Model model) {
        try {            
            MatchOverInfo matchOverInfo = overService.resumeOver(strikeBatsmanId, 
                                                secondBatsmanId, bowlerId, 
                                                matchId, overId);
            model.addAttribute("matchOverInfo", matchOverInfo);
        } catch (CricShowException e) {
            log.error("Error at creating over " + e.getMessage());
        }   
        return "playMatch";
    }
    
    /**
     * It displays the list of bowlers available when an over is completed.
     *
     * @param model - is a model used to display the bowlers available in a
     * fielding team. 
     * @param bowlerId - is a id of bowler who has finished his spell.
     * @param matchId - is a matchId for which the play has started.
     * @param overId - is a overId for which the over has finished.
     * @param firstBatsmanId - is a id of strike batsman who is going to retain
     * strike.
     * @param secondBatsmanId - is a id of non-strike batsman who is at tail end.
     * @return the view containing the list of bowlers available for a over.
     */
    @RequestMapping(value = "/selectBowler",
            method = RequestMethod.POST)
    private String selectBowler(Model model, @RequestParam("bowlerId") 
            int bowlerId, @RequestParam("matchId") int matchId, 
            @RequestParam("overId") int overId, @RequestParam("firstBatsmanId") 
            int firstBatsmanId, @RequestParam("secondBatsmanId") 
            int secondBatsmanId) { 
        try {
            PlayerSelectionInfo playerSelectionInfo = 
                    overService.fetchBowlerForOver(bowlerId, matchId, overId, 
                            firstBatsmanId, secondBatsmanId);
            model.addAttribute("playerSelectionInfo", playerSelectionInfo);                        
        } catch (CricShowException | ParseException e) {
            log.error("Error at viewing playerSelection information" 
                    + e.getMessage());
        }
        return "fieldingTeam";
    }
    
    /**
     * It displays the list of batsmen available for a batting team when a 
     * wicket has fallen.
     *
     * @param model - is a model used to display the list of batsmen available 
     * in a batting team. 
     * @param bowlerId - is a id of bowler who is bowling a spell.
     * @param matchId - is a matchId for which the play has started.
     * @param overId - is a overId for which the over is currrently in progress.
     * @param outBatsmanId - is a id of batsman who got out in an over.
     * @param nonStrikerId - is a id of non-strike batsman who is at tail end.
     * @return the view containing a list of batsmen available for a batting team.
     */
    @RequestMapping(value = "/selectBatsman",
            method = RequestMethod.POST)
    private String selectBatsman(Model model, @RequestParam("bowlerId") 
            int bowlerId, @RequestParam("matchId") int matchId, 
            @RequestParam("overId") int overId, @RequestParam("outBatsmanId") 
            int outBatsmanId, @RequestParam("nonStrikerId") 
            int nonStrikerId) { 
        try {
            PlayerSelectionInfo playerSelectionInfo = 
                    overService.fetchBatsmanForOver(bowlerId, matchId, overId, 
                            outBatsmanId, nonStrikerId);
            model.addAttribute("playerSelectionInfo", playerSelectionInfo);                        
        } catch (CricShowException | ParseException e) {
            log.error("Error at viewing playerSelection information" 
                    + e.getMessage());
        }
        return "battingTeam";
    }
}
