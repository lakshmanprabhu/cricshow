package com.ideas2it.cricshow.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;      
import org.springframework.web.bind.annotation.RequestMapping;    
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam;   
import org.springframework.ui.Model; 

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.exception.InValidUserCredentialsException;
import com.ideas2it.cricshow.exception.LoginAttemptExceededException;
import com.ideas2it.cricshow.exception.UserAlreadyExistException;
import com.ideas2it.cricshow.exception.UserNotFoundException;
import com.ideas2it.cricshow.info.UserInfo;
import com.ideas2it.cricshow.service.UserService;

/**
 * It provides the controller functions to the users.
 *
 * @author    Lakshmanprabhu Naidu
 *
 * @date    24/08/2019
 */
@Controller
public class UserController {
    private UserService userService;
    private static final Logger log = Logger.getLogger(UserController.class);
    
    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }
    
    /**
     * It provides the user creation form which includes user object as 
     * model and render to the signUp view.
     * 
     * @param model - which is used to add user object to it.
     * @return the signUp view to the user with user model.
     */
    @RequestMapping(value = "/signUp",
            method = RequestMethod.GET)
    private String showPlayerForm(Model model) {
        model.addAttribute("userInfo", new UserInfo());  
        return "signUp";
    }
    
    /**
     * It a creation menu to which a user is created with its credentials .
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws IOException if any interuption occured during input/output.
     * @throws ServletException if request failed with server.  
     */
    @RequestMapping(value = "/register",
            method = RequestMethod.POST)
    private String createUser(@ModelAttribute("userInfo") UserInfo userInfo) {
        try {
            int userId = userService.addUser(userInfo);
            return "index";
        } catch (UserAlreadyExistException e) {
           log.error("Error at creating user for application" + e.getMessage());
           return "redirect:/signUp";
        } catch (CricShowException e) {
           log.error("Error at creating user for application" + e.getMessage());
           return "redirect:/signUp";
        }
    }

    /**
     * It check whether the user is present in database or not.
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output.
     */
    @RequestMapping(value = "/signIn",
            method = RequestMethod.POST)
    private String userLogin(HttpServletRequest request) throws IOException, 
            ServletException {
        String redirectPage = "";
        try {
            String emailAddress = request.getParameter("emailaddress");
            String password = request.getParameter("password");
            log.info("Log occured");
            UserInfo userInfo = userService.fetchUserByEmailAddress(emailAddress);
            if (checkLoginAttempt(userInfo) 
                    && checkUserCredentials(emailAddress, password, userInfo)) {
                HttpSession session = request.getSession(Boolean.FALSE);
                session.setAttribute("username", userInfo.getName());
                redirectPage = "home";
            }
        }  catch (LoginAttemptExceededException e) {
           log.error("Error at logging user emailId " 
                   + request.getParameter("emailaddress") + e.getMessage());
           request.setAttribute(Constants.LABEL_ERROR_MESSAGE, e.getMessage());
           redirectPage = "index";
        } catch (UserNotFoundException e) {
           log.error("Sorry no user found " 
                   + request.getParameter("emailaddress") + e.getMessage());
           request.setAttribute(Constants.LABEL_ERROR_MESSAGE, e.getMessage());
           redirectPage = "index";
        }  catch (InValidUserCredentialsException e) {
           log.error("Sorry username or password incorrect " 
                   + request.getParameter("emailaddress") + e.getMessage());
           request.setAttribute(Constants.LABEL_ERROR_MESSAGE, e.getMessage());
           redirectPage = "index";
        }  catch (CricShowException e) {
           log.error("Error during redirecting error page. ");
           request.setAttribute(Constants.LABEL_ERROR_MESSAGE, e.getMessage());
           redirectPage = "index";
        } finally {
            return redirectPage;
        } 
    }
    
    /**
     * It provides the user creation form which includes user object as 
     * model and render to the signUp view.
     * 
     * @param model - which is used to add user object to it.
     * @return the signUp view to the user with user model.
     */
    @RequestMapping(value = "/logOut",
            method = RequestMethod.GET)
    private String userLogOut(HttpServletRequest request) throws IOException,
            ServletException {
        HttpSession session = request.getSession();
        if (null != session) {
            session.invalidate();
        }
        return "index";
    }
    
    /**
     * It validates whether the email and password entered is present in 
     * database or not.
     *
     * @param emailAddress - is a email address.
     * @param password - is a password.
     * @param user - is a user object. 
     * @return true if present and false if not present.
     * @throws CricShowException which is a custom exception.
     */
    private boolean checkUserCredentials(String emailAddress, String password,
            UserInfo userInfo) throws CricShowException {
        return userService.isValidUserCredentials(emailAddress, password, 
                userInfo);
    }

    /**
     * It checks whether the login attempt exceeds than 3 attempt or not. If 
     * exceeded then returns LoginAttemptExceededException as false otherwise 
     * returns true. 
     *
     * @param user - is a user object. 
     * @return true if present and false if not present.
     * @throws CricShowException which is a custom exception.
     */
    private boolean checkLoginAttempt(UserInfo userInfo) throws 
            CricShowException {
        return userService.isLoginAttemptExceeded(userInfo);
    }
}
