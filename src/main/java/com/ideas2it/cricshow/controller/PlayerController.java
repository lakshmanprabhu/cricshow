package com.ideas2it.cricshow.controller;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.json.JSONArray;   
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Controller;    
import org.springframework.web.bind.annotation.ModelAttribute;      
import org.springframework.web.bind.annotation.RequestMapping;    
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam;  
import org.springframework.web.multipart.commons.CommonsMultipartFile; 
import org.springframework.ui.Model; 

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.exception.CricShowException;
import com.ideas2it.cricshow.info.PlayerInfo;
import com.ideas2it.cricshow.info.ResponsePagedPlayerInfo;
import com.ideas2it.cricshow.service.PlayerService;

/**
 * It provides the controller functions to a player.
 *
 * @author    Lakshmanprabhu Naidu
 *
 * @date    19/08/2019
 */
@Controller
public class PlayerController {
    private PlayerService playerService;
    private static final Logger log = Logger.getLogger(PlayerController.class);
    
    @Autowired
    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }
    
    /**
     * It provides the player creation form which includes player object as 
     * model and render to the addPlayer view.
     * 
     * @param model - which is used to add player object to it.
     * @return the addPlayer view to the user with player model.
     */
    @RequestMapping(value = Constants.LABEL_CREATE_PLAYER,
            method = RequestMethod.GET)
    private String showPlayerForm(Model model) {
        model.addAttribute(Constants.LABEL_PLAYER_INFO, new PlayerInfo());  
        return Constants.LABEL_ADD_PLAYER;
    }
    
    /**
     * It provides creation of player to which the user creates a player with it 
     * credentials such as name, country, battingType, bowlingType, dateofBirth,
     * status, image, address, phoneNumber and pincode.
     *
     * @param file - which is used to upload image of a player.
     * @param player - is a player object which holds the model data of player.
     * @param model - which is used to add player object to it. 
     * @return the viewPlayer view to display the saved credentials of player.
     */
    @RequestMapping(value = Constants.LABEL_SAVE_PLAYER,
            method = RequestMethod.POST)
    private String createPlayer(@RequestParam CommonsMultipartFile file,
            @ModelAttribute(Constants.LABEL_PLAYER_INFO) PlayerInfo playerInfo, 
            Model model) {
        try {  
            playerInfo = playerService.addPlayer(playerInfo, file);
            model.addAttribute(Constants.LABEL_PLAYER_INFO, playerInfo);
        } catch (ParseException e) {
            log.error(Constants.LABEL_ERROR_DUE_TO_PARSING_DATE 
                    + e.getMessage());
        } catch (CricShowException | IOException e) {
            log.error(Constants.LABEL_ERROR_AT_CREATING_PLAYER 
                     + e.getMessage());
        }
        return Constants.LABEL_VIEW_PLAYER_PAGE;
    }
    
    /**
     * It displays details of a player including contact information also.
     *
     * @param model - which is used to add player object to it. 
     * @param playerId - it is a id used to fetch the data of the player. 
     */
    @RequestMapping(value = Constants.LABEL_VIEW_PLAYER, 
            method = RequestMethod.GET)
    private String viewPlayer(Model model, @RequestParam(Constants.LABEL_ID) 
            int playerId) {
        try {
            PlayerInfo playerInfo = playerService.fetchPlayerById(playerId);
            model.addAttribute(Constants.LABEL_PLAYER_INFO, playerInfo);
        } catch (CricShowException | ParseException e) {
            log.error(Constants.LABEL_ERROR_AT_DISPLAYING_PLAYER_WITH_ID  
                     + e.getMessage()); 
        }  
        return Constants.LABEL_VIEW_PLAYER_PAGE;
    }
    
    /**
     * It provides information of a player to be edited.
     * Displays credentials of player and asks user to update its credentials. 
     *
     * @param model - which is used to add player object to it.
     * @param playerId - it is a id used to fetch the data of the player. 
     */
    @RequestMapping(value = Constants.LABEL_UPDATE_PLAYER,
            method = RequestMethod.GET)
    private String updatePlayer(Model model, @RequestParam(Constants.LABEL_ID) 
            int playerId) {  
        try {
            PlayerInfo playerInfo = playerService.fetchPlayerById(playerId);
            model.addAttribute(Constants.LABEL_PLAYER_INFO, playerInfo);
        } catch (CricShowException | ParseException e) {
            log.error(Constants.LABEL_ERROR_AT_DISPLAYING_PLAYER
                    + e.getMessage()); 
        }
        return Constants.LABEL_UPDATE_PLAYER_PAGE;
    }
    
    /**
     * Saves the edited information of a player with contact . 
     *
     * @param file - which is used to upload image of a player.
     * @param player - is a player object which holds the model data of player.
     * @param model - which is used to add player object to it.  
     */
    @RequestMapping(value = Constants.LABEL_EDIT_PLAYER,
            method = RequestMethod.POST)
    private String saveUpdatedInformation(@RequestParam CommonsMultipartFile file,
            @ModelAttribute(Constants.LABEL_PLAYER_INFO) PlayerInfo playerInfo, 
            Model model) {
        try {
            playerInfo = playerService.editPlayer(playerInfo, file);
            model.addAttribute(Constants.LABEL_PLAYER_INFO, playerInfo);    
        } catch (CricShowException | ParseException | IOException e) {
            log.error(Constants.LABEL_ERROR_AT_SAVING_UPDATED_PLAYER 
                    + e.getMessage());
        }  
        return Constants.LABEL_REDIRECT_VIEW_PLAYERS;
    }
    
    /**
     * It displays credentials of first five players only. 
     *
     * @param model - is used to add playerInfo object and last page to it.   
     */   
    @RequestMapping(value = Constants.LABEL_VIEW_PLAYERS,
            method = RequestMethod.GET)      
    private String viewFirstFivePlayers(Model model) {
        try {
            int page = 1;
            ResponsePagedPlayerInfo pagedPlayerInfo = playerService
                    .fetchFirstFivePlayers(page); 
            int lastPage = pagedPlayerInfo.getNoOfPages();
            model.addAttribute(Constants.LABEL_PAGED_PLAYER_INFO, 
                    pagedPlayerInfo);   
            model.addAttribute(Constants.LABEL_LAST_PAGE, lastPage);
        } catch (Exception e) {
            log.error(Constants.LABEL_ERROR_AT_RETRIEVING_FIRST_FIVE_PLAYERS
                    + e.getMessage());
        }    
        return Constants.LABEL_VIEW_PLAYERS_PAGE; 
    }   

    /**
     * It retrieves credentials of remaining players using ajax from database. 
     *
     * @param request - is a HttpServletRequest request. 
     * @param response - is a HttpServletResponse response.
     * @throws ServletException if request failed with server.
     * @throws IOException if any interuption occured during input/output. 
     */   
    @RequestMapping(value = Constants.LABEL_VIEW_REMAINING_PLAYERS, 
            method = RequestMethod.GET)
    private void viewRemainingPlayers(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        try {
            int page = Integer.parseInt(request.getParameter(
                             Constants.LABEL_PAGE));
            JSONArray array = playerService.fetchRemainingPlayers(page);
            response.setContentType(Constants.LABEL_APPLICATION_TYPE_JSON);
            response.getWriter().write(array.toString());
        } catch (CricShowException e) {
            log.error(Constants.LABEL_ERROR_AT_VIEWING_REMAINING_PLAYERS 
                    + e.getMessage()); 
        }      
    }   

    /**
     * It provides user to delete player credentials.
     * 
     * @param model - is used to add player object.
     * @param playerId - it is a id used to delete the data of the player.
     */
    @RequestMapping(value = Constants.LABEL_DELETE_PLAYER,
            method = RequestMethod.GET)
    private String deletePlayer(Model model, @RequestParam(Constants.LABEL_ID) 
            int playerId) {
        try {
            playerService.removePlayer(playerId);
        } catch (CricShowException e) {
            log.error(Constants.LABEL_ERROR_AT_DELETING_PLAYER + e.getMessage());
        }
        return Constants.LABEL_REDIRECT_VIEW_PLAYERS;
    }
}
