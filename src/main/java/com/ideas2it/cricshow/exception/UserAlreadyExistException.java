package com.ideas2it.cricshow.exception;

/**
 * UserAlreadyExistException act as a runtime exception which is used to handle 
 * when user is already present in the database.
 */
public class UserAlreadyExistException extends RuntimeException {

    /**
     * It handles the exception and gives a meaningful message to user.
     * 
     * @param message - used to display context for a exception occured.
     */
    public UserAlreadyExistException(String message) {
        super(message);
    }

} 



