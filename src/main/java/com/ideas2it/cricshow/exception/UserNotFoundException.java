package com.ideas2it.cricshow.exception;

/**
 * UserNotFoundException act as a custom exception which is used to handle 
 * when user is not found.
 */
public class UserNotFoundException extends RuntimeException {

    /**
     * It handles the exception and gives a meaningful message to user.
     * 
     * @param message - used to display context for a exception occured.
     */
    public UserNotFoundException(String message) {
        super(message);
    }

} 

