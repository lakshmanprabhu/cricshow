package com.ideas2it.cricshow.exception;

/**
 * InValidUserCredentialsException act as a runtime exception which is used to  
 * handle when user credentials is incorrect.
 */
public class InValidUserCredentialsException extends RuntimeException {

    /**
     * It handles the exception and gives a meaningful context message to user.
     * 
     * @param message - used to display context for a exception occured.
     */
    public InValidUserCredentialsException(String message) {
        super(message);
    }

} 

