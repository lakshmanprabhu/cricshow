package com.ideas2it.cricshow.exception;

/**
 * TeamNotFoundException act as a runtime exception which is used to handle when
 * team is not found.
 */
public class TeamNotFoundException extends RuntimeException {

    /**
     * It handles the exception and gives a meaningful message to user.
     * 
     * @param message - used to display context for a exception occured.
     */
    public TeamNotFoundException(String message) {
        super(message);
    }

} 
