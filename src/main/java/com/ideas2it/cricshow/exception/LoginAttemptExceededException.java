package com.ideas2it.cricshow.exception;

/**
 * LoginAttemptExceededException act as a runtime exception which is used to  
 * handle when user attempts incorrect login more than 3 attempts.
 */
public class LoginAttemptExceededException extends RuntimeException {

    /**
     * It handles the exception and gives a meaningful context message to user.
     * 
     * @param message - used to display context for a exception occured.
     */
    public LoginAttemptExceededException(String message) {
        super(message);
    }

} 

