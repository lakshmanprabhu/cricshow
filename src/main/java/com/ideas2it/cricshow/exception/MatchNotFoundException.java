package com.ideas2it.cricshow.exception;

/**
 * MatchNotFoundException act as a runtime exception which is used to handle when
 * match is not found.
 */
public class MatchNotFoundException extends RuntimeException {

    /**
     * It handles the exception and gives a meaningful message to user.
     * 
     * @param message - used to display context for a exception occured.
     */
    public MatchNotFoundException(String message) {
        super(message);
    }

} 
