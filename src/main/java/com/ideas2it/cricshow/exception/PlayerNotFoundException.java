package com.ideas2it.cricshow.exception;

/**
 * PlayerNotFoundException act as a runtime exception which is used to handle 
 * when player is not found.
 */
public class PlayerNotFoundException extends RuntimeException {

    /**
     * It handles the exception and gives a meaningful message to user.
     * 
     * @param message - used to display context for a exception occured.
     */
    public PlayerNotFoundException(String message) {
        super(message);
    }

} 

