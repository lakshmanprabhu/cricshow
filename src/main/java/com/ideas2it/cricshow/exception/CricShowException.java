package com.ideas2it.cricshow.exception;

/**
 * CricShowException act as a custom exception to handles all exception which
 * occured in the overall CricShow project.
 */
public class CricShowException extends Exception {

    /**
     * It handles the exception and gives a meaningful context message to user.
     * 
     * @param message - used to display context for a exception occured.
     * @param cause - the root where the exception occured.
     */
    public CricShowException(String message, Throwable cause) {
        super(message, cause);
    }

} 

