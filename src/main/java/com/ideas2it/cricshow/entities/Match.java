package com.ideas2it.cricshow.entities;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.ideas2it.cricshow.common.PlayingFormat;
import com.ideas2it.cricshow.entities.Team;

/**
 * It stores the match credentials such as id, playingDate, name, venue and
 * playingFormat in a match.
 */
@Entity
@Table(name = "matches")
public class Match {
    
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "playing_date")
    private Date playingDate;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "venue")
    private String venue;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "playing_format")
    private PlayingFormat playingFormat;
    
    @ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "match_team", joinColumns = { 
			@JoinColumn(name = "match_id") }, 
			inverseJoinColumns = { @JoinColumn(name = "team_id") })
    private Set<Team> teams;

    /**
     * toString method is overrided to describe match information such as 
     * name, id, venue, playingformat and matchdate.
     *
     * @return result - containing the credentials of a match.
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(" Name : " + getName());
        result.append(", Id : " + getId());
        result.append(", Venue : " + getVenue());
        result.append(", PlayingFormat : " + getPlayingFormat());
        result.append(", PlayingDate : " + getPlayingDate());
        result.append("\n");
        return result.toString();  
    }
    
    /**
     * Equals method is overrided and returns true if two objects are equal else 
     * false when objects are unequal.
     *
     * @param object is a Object instance.
     * @return true or false.
     */
    public boolean equals(Object object) {
        if (object == this) {
            return Boolean.TRUE;
        }
        if (null == object || getClass() != object.getClass()) { 
            return Boolean.FALSE;
        }
        Match match = (Match) object;
        return (id == match.getId() && name.equals(match.getName()));
    }
    
    /**
     * HashCode method is overrided and returns unique hashcode.
     *
     * @return result - is a unique hashcode generated using id.
     */
    public int hashCode() {
        int result = 3;  
        result = (int) (id/ 11); 
        return result;
    }
    
    // Getters and Setters methods for a match.
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
        
    public Date getPlayingDate() {
        return playingDate;
    }
  
    public void setPlayingDate(Date playingDate) {
        this.playingDate = playingDate;
    }
    
    public String getName() {
        return name;
    }
  
    public void setName(String name) {
        this.name = name;
    }
    
    public String getVenue() {
        return venue;
    }
  
    public void setVenue(String venue) {
        this.venue = venue;
    }

    public PlayingFormat getPlayingFormat() {
        return playingFormat;
    }
  
    public void setPlayingFormat(PlayingFormat playingFormat) {
        this.playingFormat = playingFormat;
    }
    
    public Set<Team> getTeams() {
        return teams;
    }
    
    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }
}
