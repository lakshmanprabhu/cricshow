package com.ideas2it.cricshow.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * It stores the user credentials such as id, loginAttempt, status, name, gender,
 * role, emailAddress, phoneNumber and password.
 */
@Entity
@Table(name = "user")
public class User {
    
    @Column(name = "status")
    private boolean status;
    
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "login_attempt")
    private int loginAttempt;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "gender")
    private String gender;
    
    @Column(name = "role")
    private String role;
    
    @Column(name = "email_address", unique = true)
    private String emailAddress;
    
    @Column(name = "phone_number")
    private String phoneNumber;
    
    @Column(name = "password")
    private String password;
    
    /**
     * toString method is overrided to describe user information such as 
     * name, id, gender, login attempt, password, email address, phone number
     * and status.
     *
     * @return result - containing the credentials of a user.
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
	    result.append(" User Name : " + getName());
        result.append(", User Id : " + getId());
        result.append(", Gender : " + getGender()); 
        result.append(", Role : " + getRole());
        result.append(", Email address : " + getEmailAddress());
        result.append(", Phone Number : " + getPhoneNumber());
        result.append(", Password : " + getPassword());
        result.append("\n");
        return result.toString();  
    }
    
    /**
     * Equals method is overrided and returns true or false .
     *
     * @param object is a Object instance.
     * @return true or false.
     */
    public boolean equals(Object object) {
        if (object == this) {
            return Boolean.TRUE;
        }
        if (null == object || getClass() != object.getClass()) { 
            return Boolean.FALSE;
        }
        User user = (User) object;
        return (password == user.getPassword() 
                && emailAddress.equals(user.getEmailAddress()));
    }
    
    /**
     * HashCode method is overrided and returns integer value.
     *
     * @return integer value.
     */
    public int hashCode() {
        int result = 3;  
        result = (int) (id/ 11); 
        return result;
    }
    
    // Getters and Setters for user credentials.
    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getLoginAttempt() {
        return loginAttempt;
    }

    public void setLoginAttempt(int loginAttempt) {
        this.loginAttempt = loginAttempt;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }
 
    public void setGender(String gender) {
        this.gender = gender;
    }
 
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {   
        this.password = password;  
    }
}
