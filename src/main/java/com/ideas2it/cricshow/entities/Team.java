package com.ideas2it.cricshow.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.ideas2it.cricshow.common.Country;
import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.entities.Match;

/**
 * It stores the team credentials such as id, name, country and status.
 */
@Entity
@Table(name = "team")
public class Team {
    
    @Column(name = "status")
    private boolean status;
    
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "name")
    private String name;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "country")
    private Country country;
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinColumn(name = "team_id")
    private Set<Player> players;
    
    @ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "match_team", joinColumns = { 
			@JoinColumn(name = "team_id") }, 
			inverseJoinColumns = { @JoinColumn(name = "match_id") })
    private Set<Match> matches;
    
    /**
     * toString method is overrided to describe team information such as 
     * name, id, country and status.
     *
     * @return result - containing all credentials of a team.
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(", Id : " + getId());
        result.append(", Name : " + getName());
        result.append(", Country : " + getCountry());
        result.append("\n");
        return result.toString();  
    }
    
    /**
     * Equals method is overrided and returns true if two objects are equal else 
     * false when objects are unequal.
     *
     * @param object is a Object instance.
     * @return true or false.
     */
    public boolean equals(Object object) {
        if (object == this) {
            return Boolean.TRUE;
        }
        if (null == object || getClass() != object.getClass()) { 
            return Boolean.FALSE;
        }
        Team team = (Team) object;
        return (id == team.getId() && name.equals(team.getName()));
    }
    
    /**
     * HashCode method is overrided and returns unique hashcode.
     *
     * @return result - is a unique hashcode generated using id.
     */
    public int hashCode() {
        int result = 3;  
        result = (int) (id/ 11); 
        return result;
    }
    
    // Getters and Setters methods for a team.    
    public boolean getStatus() {
        return status;
    }
    
    public void setStatus(boolean status) {
        this.status = status;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
  
    public void setName(String name) {
        this.name = name;
    }
    
    public Country getCountry() {
        return country;
    }
  
    public void setCountry(Country country) {
        this.country = country;
    }

    public Set<Player> getPlayers() {
        return players;
    }
    
    public void setPlayers(Set<Player> players) {
        this.players = players;
    }
    
    public Set<Match> getMatches() {
        return matches;
    }
    
    public void setMatches(Set<Match> matches) {
        this.matches = matches;
    }
}
