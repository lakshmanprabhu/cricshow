package com.ideas2it.cricshow.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ideas2it.cricshow.common.BallResult;
import com.ideas2it.cricshow.entities.Match;
import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.entities.Over;

/**
 * It stores the over detailed information such as ball number, runs scored in
 * a ball, batsman playing the ball , bowler bowling the ball, over in which a 
 * ball is being bowled, ball result and match object.
 */
@Entity
@Table(name = "over_detail")
public class OverDetail {
    
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "ball_number")
    private int ballNumber;
    
    @Column(name = "run_scored_in_ball")
    private int runScoredInBall;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "ball_result")
    private BallResult ballResult;
    
    @ManyToOne
    @JoinColumn(name = "batsman_id")
    private Player batsman;
    
    @ManyToOne
    @JoinColumn(name = "bowler_id")
    private Player bowler;
    
    @Column(name = "match_id")
    private int matchId;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name = "over_id")
    private Over over;
    
    /**
     * toString method is overrided to describe over detailed credentials such  
     * as Id, Ball Number, Runs Scored In Ball, Ball result, Bowler, Batsman,
     * Match and Over.
     *
     * @return result - containing all information of a overDetail.
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Over\n");
	    result.append(" Id : " + getId());
        result.append(", Ball Number : " + getBallNumber());
        result.append(", Runs Scored In Ball : " + getRunScoredInBall()); 
        result.append(", Ball result  : " + getBallResult());
        result.append("\n");
        return result.toString();  
    }
    
    /**
     * Equals method is overrided and returns true if two objects are equal else 
     * false when objects are unequal.
     *
     * @param object is a Object instance.
     * @return true or false.
     */
    public boolean equals(Object object) {
        if (object == this) {
            return Boolean.TRUE;
        }
        if (null == object || getClass() != object.getClass()) { 
            return Boolean.FALSE;
        }
        OverDetail overDetail = (OverDetail) object;
        return (id == over.getId());
    }
    
    /**
     * HashCode method is overrided and returns unique hashcode.
     *
     * @return result - is a unique hashcode generated using id.
     */
    public int hashCode() {
        int result = 3;  
        result = (int) (id/ 11); 
        return result;
    }
    
    // Getters and Setters for overDetail credentials.
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getBallNumber() {
        return this.ballNumber;
    }

    public void setBallNumber(int ballNumber) {
        this.ballNumber = ballNumber;
    }
    
    public int getRunScoredInBall() {
        return this.runScoredInBall;
    }

    public void setRunScoredInBall(int runScoredInBall) {
        this.runScoredInBall = runScoredInBall;
    }

    public BallResult getBallResult() {
        return this.ballResult;
    }
 
    public void setBallResult(BallResult ballResult) {   
        this.ballResult = ballResult;  
    }

    public Player getBatsman() {
        return this.batsman;
    }
 
    public void setBatsman(Player batsman) {
        this.batsman = batsman;
    }
 
    public Player getBowler() {
        return this.bowler;
    }

    public void setBowler(Player bowler) {
        this.bowler = bowler;
    }
    
    public int getMatchId() {
        return this.matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }
    
    public Over getOver() {
        return this.over;
    }

    public void setOver(Over over) {
        this.over = over;
    }
}

