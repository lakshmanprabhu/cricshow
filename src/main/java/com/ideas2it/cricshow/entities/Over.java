package com.ideas2it.cricshow.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ideas2it.cricshow.entities.Match;
import com.ideas2it.cricshow.entities.OverDetail;

/**
 * It stores the over information such as total runs, wickets count, fours count,
 * sixes count, extra count and match object.
 */
@Entity
@Table(name = "over")
public class Over {
    
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "over_number")
    private int overNumber;
        
    @Column(name = "total_runs")
    private int totalRuns;
    
    @Column(name = "wicket_count")
    private int wicketCount;
    
    @Column(name = "four_count")
    private int fourCount;
    
    @Column(name = "six_count")
    private int sixCount;
    
    @Column(name = "no_ball_count")
    private int noBallCount;
    
    @Column(name = "wide_ball_count")
    private int wideBallCount;
    
    @Column(name = "byes_count")
    private int byesCount;
    
    @Column(name = "leg_byes_count")
    private int legByesCount;
        
    @Column(name = "match_id")
    private int matchId;
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name = "over_id")
    private Set<OverDetail> overDetails;
    
    
    /**
     * toString method is overrided to describe over credentials such as 
     * Total Runs, Id, Wicket Count, Four Count, Six Count, No Ball Count, 
     * Wide Ball Count, Byes Count and Leg Byes Count.
     *
     * @return result - containing all information of a over.
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Over\n");
	    result.append(" Id : " + getId());
	    result.append(", Over Number : " + getOverNumber());
        result.append(", Total Runs : " + getTotalRuns());
        result.append(", Wicket Count : " + getWicketCount()); 
        result.append(", Four Count : " + getFourCount());
        result.append(", Six Count : " + getSixCount());
        result.append(", No Ball Count : " + getNoBallCount());
        result.append(", Wide Ball Count : " + getWideBallCount());
        result.append(", Byes Count : " + getByesCount());
        result.append(", Leg Byes Count : " + getLegByesCount());
        result.append("\n");
        return result.toString();  
    }
    
    /**
     * Equals method is overrided and returns true if two objects are equal else 
     * false when objects are unequal.
     *
     * @param object is a Object instance.
     * @return true or false.
     */
    public boolean equals(Object object) {
        if (object == this) {
            return Boolean.TRUE;
        }
        if (null == object || getClass() != object.getClass()) { 
            return Boolean.FALSE;
        }
        Over over = (Over) object;
        return (id == over.getId());
    }
    
    /**
     * HashCode method is overrided and returns unique hashcode.
     *
     * @return result - is a unique hashcode generated using id.
     */
    public int hashCode() {
        int result = 3;  
        result = (int) (id/ 11); 
        return result;
    }
    
    // Getters and Setters for over credentials.
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOverNumber() {
        return this.overNumber;
    }

    public void setOverNumber(int overNumber) {
        this.overNumber = overNumber;
    }
        
    public int getTotalRuns() {
        return this.totalRuns;
    }

    public void setTotalRuns(int totalRuns) {
        this.totalRuns = totalRuns;
    }
    
    public int getWicketCount() {
        return this.wicketCount;
    }

    public void setWicketCount(int wicketCount) {
        this.wicketCount = wicketCount;
    }

    public int getFourCount() {
        return this.fourCount;
    }
 
    public void setFourCount(int fourCount) {
        this.fourCount = fourCount;
    }
 
    public int getSixCount() {
        return this.sixCount;
    }

    public void setSixCount(int sixCount) {
        this.sixCount = sixCount;
    }

    public int getNoBallCount() {
        return this.noBallCount;
    }

    public void setNoBallCount(int noBallCount) {
        this.noBallCount = noBallCount;
    }

    public int getWideBallCount() {
        return this.wideBallCount;
    }
     
    public void setWideBallCount(int wideBallCount) {
        this.wideBallCount = wideBallCount;
    }
    
    public int getByesCount() {
        return this.byesCount;
    }
 
    public void setByesCount(int byesCount) {   
        this.byesCount = byesCount;  
    }
        
    public int getLegByesCount() {
        return this.legByesCount;
    }
 
    public void setLegByesCount(int legByesCount) {   
        this.legByesCount = legByesCount;  
    }

    public int getMatchId() {
        return this.matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }
    
    public Set<OverDetail> getOverDetails() {
        return this.overDetails;
    }

    public void setOverDetails(Set<OverDetail> overDetails) {
        this.overDetails = overDetails;
    }
}

