package com.ideas2it.cricshow.entities;

import java.text.ParseException;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ideas2it.cricshow.common.Country;
import com.ideas2it.cricshow.entities.Contact;
import com.ideas2it.cricshow.entities.Team;

/**
 * It stores the player credentials such as id, name, status, dob, role, age,
 * bowlingType, battingType, imagePath, country as well as contact and team 
 * information.
 */
@Entity
@Table(name = "player")
public class Player {
    
    @Column(name = "status")
    private boolean status;
    
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "date_of_birth")
    private String dob;
    
    @Column(name = "role")
    private String role;
    
    @Column(name = "bowling_type")
    private String bowlingType;
    
    @Column(name = "batting_type")
    private String battingType;
    
    @Transient
    private String age;
    
    @Column(name = "image_path")
    private String imagePath;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "country")
    private Country country;
    
    @OneToOne(mappedBy = "player", cascade = CascadeType.ALL)
    private Contact contact;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "team_id")
    private Team team;

    /**
     * toString method is overrided to describe player credentials such as 
     * name, id, date of birth, role, batting-type, bowling-type, country,
     * age and status.
     *
     * @return result - containing the credentials of a player.
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Player Information\n");
	    result.append(" Name : " + getName());
        result.append(", ID : " + getId());
        result.append(", D.O.B : " + getDob()); 
        result.append(", role : " + getRole());
        result.append(", Batting-Type : " + getBattingType());
        result.append(", Bowling-Type : " + getBowlingType());
        result.append(", Country : " + getCountry());
        result.append(", Age : " + getAge());
        result.append(", Status : " + getStatus());
        result.append("\n");
        return result.toString();  
    }
    
    /**
     * Equals method is overrided and returns true if two objects are equal else 
     * false when objects are unequal.
     *
     * @param object is a Object instance.
     * @return true or false.
     */
    public boolean equals(Object object) {
        if (object == this) {
            return Boolean.TRUE;
        }
        if (null == object || getClass() != object.getClass()) { 
            return Boolean.FALSE;
        }
        Player player = (Player) object;
        return (id == player.getId() && name.equals(player.getName()));
    }
    
    /**
     * HashCode method is overrided and returns unique hashcode.
     *
     * @return result - is a unique hashcode generated using id.
     */
    public int hashCode() {
        final int prime = 31;
	    int result = 1;
	    result = prime * result + id;
	    result = prime * result + ((name == null) ? 0 : name.hashCode());
	    return result;
    }
    
    // Getters and Setters for player credentials.
    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) { 
        this.status = status;
    }
    
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return this.dob;
    }
 
    public void setDob(String dob) {
        this.dob = dob;
    }
 
    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBowlingType() {
        return this.bowlingType;
    }

    public void setBowlingType(String bowlingType) {
        this.bowlingType = bowlingType;
    }

    public String getBattingType() {
        return this.battingType;
    }
     
    public void setBattingType(String battingType) {
        this.battingType = battingType;
    }
    
    public String getAge() {
        return this.age;
    }
 
    public void setAge(String age) throws ParseException {   
        this.age = age;  
    }
        
    public String getImagePath() {
        return this.imagePath;
    }
 
    public void setImagePath(String imagePath) {   
        this.imagePath = imagePath;  
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Contact getContact() {
        return this.contact;
    }
    
    public void setContact(Contact contact) {
        this.contact = contact;
    }
    
    public Team getTeam() {
        return this.team;
    }
    
    public void setTeam(Team team) {
        this.team = team;
    }
}

