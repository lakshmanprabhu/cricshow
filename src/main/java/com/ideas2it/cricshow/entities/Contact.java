package com.ideas2it.cricshow.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ideas2it.cricshow.entities.Player;

/**
 * It stores the contact information of a player such as id, phonenumber, 
 * address and pincode.
 */
@Entity
@Table(name = "contact")
public class Contact {
    
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "pincode")
    private int pincode;
    
    @Column(name = "phone_number")
    private String phoneNumber;
    
    @Column(name = "address")
    private String address;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "player_id")
    private Player player;
    
    /**
     * toString method is overrided to describe contact information such as 
     * phonenumber, address and pincode.
     *
     * @return result - containing all credentials of a contact.
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
	    result.append("PhoneNumber : " + getPhoneNumber());
        result.append(",Address : " + getAddress());
        result.append(",Pincode : " + getPincode()); 
        result.append("\n");
        return result.toString();  
    }
    
    /**
     * Equals method is overrided and returns true if two objects are equal else 
     * false when objects are unequal.
     *
     * @param object is a Object instance.
     * @return true or false.
     */
    public boolean equals(Object object) {
        if (object == this) {
            return Boolean.TRUE;
        }
        if (null == object || getClass() != object.getClass()) { 
            return Boolean.FALSE;
        }
        Contact contact = (Contact) object;
        return (id == contact.getId() 
                && phoneNumber.equals(contact.getPhoneNumber()));
    }
    
    /**
     * HashCode method is overrided and returns unique hashcode.
     *
     * @return result - is a unique hashcode generated using id.
     */
    public int hashCode() {
        int result = 3;  
        result = (int) (id/ 11); 
        return result;
    }
     
    // Getters and Setters of a player contact information.
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getPincode() {
        return pincode;
    }
  
    public void setPincode(int pincode) {
        this.pincode = pincode;
    }
     
    public String getPhoneNumber() {
        return phoneNumber;
    }
     
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
 
    public String getAddress() {
        return address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public Player getPlayer() {
        return player;
    }
    
    public void setPlayer(Player player) {
        this.player = player;
    }
}
