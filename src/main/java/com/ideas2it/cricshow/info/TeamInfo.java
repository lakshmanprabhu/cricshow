package com.ideas2it.cricshow.info;

import java.util.List;

import com.ideas2it.cricshow.common.Country;
import com.ideas2it.cricshow.info.PlayerInfo;

/**
 * It stores the fields of team object as a info object which is used to 
 * communicate between view and controller in a application.
 */
public class TeamInfo {
    private boolean status;
    private int id;
    private int[] rolesCount;
    private String name;
    private Country country;
    private List<PlayerInfo> playersInTeam;
    private List<PlayerInfo> playersNotInTeam;
    
    //NO argument constructor
    public TeamInfo() {}
    
    // Parameterized constructor of a TeamInfo object.
    public TeamInfo(int[] rolesCount, List<PlayerInfo> playersInTeam, 
            List<PlayerInfo> playersNotInTeam) {
        this.rolesCount = rolesCount;
        this.playersInTeam = playersInTeam;
        this.playersNotInTeam = playersNotInTeam;
    }

    // Getters and Setters for TeamInfo object.
    public boolean getStatus() {
        return status;
    }
    
    public void setStatus(boolean status) {
        this.status = status;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
        
    public int[] getRolesCount() {
        return rolesCount;
    }

    public void setRolesCount(int[] rolesCount) {
        this.rolesCount = rolesCount;
    }
    
    public String getName() {
        return name;
    }
  
    public void setName(String name) {
        this.name = name;
    }
    
    public Country getCountry() {
        return country;
    }
  
    public void setCountry(Country country) {
        this.country = country;
    }

    public List<PlayerInfo> getPlayersInTeam() {
        return playersInTeam;
    }

    public void setPlayersInTeam(List<PlayerInfo> playersInTeam) {
        this.playersInTeam = playersInTeam;
    }

    public List<PlayerInfo> getPlayersNotInTeam() {
        return playersNotInTeam;
    }
    
    public void setPlayersNotInTeam(List<PlayerInfo> playersNotInTeam) {
        this.playersNotInTeam = playersNotInTeam;
    }  
}

