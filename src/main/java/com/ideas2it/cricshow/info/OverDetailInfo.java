package com.ideas2it.cricshow.info;

import com.ideas2it.cricshow.common.BallResult;
import com.ideas2it.cricshow.info.MatchInfo;
import com.ideas2it.cricshow.info.OverInfo;
import com.ideas2it.cricshow.info.PlayerInfo;

/**
 * It stores the fields of overdetail object as a info object which is used to 
 * communicate between view and controller in a application.
 */
public class OverDetailInfo {
    
    private int id;
    private int ballNumber;
    private int runScoredInBall;
    private BallResult ballResult;
    private PlayerInfo batsmanInfo;
    private PlayerInfo bowlerInfo;
    private MatchInfo matchInfo;
    private OverInfo overInfo;

    // Getters and Setters for OverDetailInfo credentials.
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getBallNumber() {
        return this.ballNumber;
    }

    public void setBallNumber(int ballNumber) {
        this.ballNumber = ballNumber;
    }
    
    public int getRunScoredInBall() {
        return this.runScoredInBall;
    }

    public void setRunScoredInBall(int runScoredInBall) {
        this.runScoredInBall = runScoredInBall;
    }

    public BallResult getBallResult() {
        return this.ballResult;
    }
 
    public void setBallResult(BallResult ballResult) {   
        this.ballResult = ballResult;  
    }

    public PlayerInfo getBatsmanInfo() {
        return this.batsmanInfo;
    }
 
    public void setBatsmanInfo(PlayerInfo batsmanInfo) {
        this.batsmanInfo = batsmanInfo;
    }
 
    public PlayerInfo getBowlerInfo() {
        return this.bowlerInfo;
    }

    public void setBowlerInfo(PlayerInfo bowlerInfo) {
        this.bowlerInfo = bowlerInfo;
    }
    
    public MatchInfo getMatchInfo() {
        return this.matchInfo;
    }

    public void setMatchInfo(MatchInfo matchInfo) {
        this.matchInfo = matchInfo;
    }
    
    public OverInfo getOverInfo() {
        return this.overInfo;
    }

    public void setOverInfo(OverInfo overInfo) {
        this.overInfo = overInfo;
    }
}

