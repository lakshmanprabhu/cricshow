package com.ideas2it.cricshow.info;

import com.ideas2it.cricshow.entities.Over;
import com.ideas2it.cricshow.info.MatchInfo;

/**
 * It stores the field such as bowlerId, batsmenIds and matchId and is used to 
 * communicate between view and controller in a application.
 */
public class MatchOverInfo {
    private int bowlerId;
    private int matchId;
    private int overId;
    private int overNumber;
    private int ballNumber;
    private int firstBatsmanId;
    private int secondBatsmanId;
    private String[] batsmenIds;
    private String bowlerName;
    private String firstBatsmanName;
    private String secondBatsmanName;
    private MatchInfo matchInfo;
    

    // Getters and Setters for MatchOverInfo credentials.
    public int getBowlerId() {
        return this.bowlerId;
    }

    public void setBowlerId(int bowlerId) {
        this.bowlerId = bowlerId;
    }

    public int getMatchId() {
        return this.matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getOverNumber() {
        return this.overNumber;
    }

    public void setOverNumber(int overNumber) {
        this.overNumber = overNumber;
    }
    
    public int getBallNumber() {
        return this.ballNumber;
    }

    public void setBallNumber(int ballNumber) {
        this.ballNumber = ballNumber;
    }
    
    public int getFirstBatsmanId() {
        return this.firstBatsmanId;
    }

    public void setFirstBatsmanId(int firstBatsmanId) {
        this.firstBatsmanId = firstBatsmanId;
    }         
               
    public int getSecondBatsmanId() {
        return this.secondBatsmanId;
    }
    
    public void setSecondBatsmanId(int secondBatsmanId) {
        this.secondBatsmanId = secondBatsmanId;
    }  
                      
    public String[] getBatsmenIds() {
        return this.batsmenIds;
    }

    public void setBatsmenIds(String[] batsmenIds) {
        this.batsmenIds = batsmenIds;
    }
                        
    public String getBowlerName() {
        return this.bowlerName;
    }

    public void setBowlerName(String bowlerName) {
        this.bowlerName = bowlerName;
    }
                        
    public String getFirstBatsmanName() {
        return this.firstBatsmanName;
    }

    public void setFirstBatsmanName(String firstBatsmanName) {
        this.firstBatsmanName = firstBatsmanName;
    }
                        
    public String getSecondBatsmanName() {
        return this.secondBatsmanName;
    }

    public void setSecondBatsmanName(String secondBatsmanName) {
        this.secondBatsmanName = secondBatsmanName;
    }
                            
    public int getOverId() {
        return this.overId;
    }

    public void setOverId(int overId) {
        this.overId = overId;
    }
                           
    public MatchInfo getMatchInfo() {
        return this.matchInfo;
    }

    public void setMatchInfo(MatchInfo matchInfo) {
        this.matchInfo = matchInfo;
    }    
}
