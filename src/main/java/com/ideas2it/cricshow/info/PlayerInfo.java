package com.ideas2it.cricshow.info;

import java.text.ParseException;

import com.ideas2it.cricshow.common.Country;

/**
 * It stores the fields of player object as a info object which is used to 
 * communicate between view and controller in a application.
 */
public class PlayerInfo {
    private boolean status;
    private int id;
    private int contactId;
    private int pincode;
    private String name;
    private String dob;
    private String role;
    private String bowlingType;
    private String battingType;
    private String age;
    private String imagePath;
    private String phoneNumber;
    private String address;
    private Country country;

    //Getters and Setters for PlayerInfo credentials.
    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) { 
        this.status = status;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getContactId() {
        return contactId;
    }
    
    public void setContactId(int contactId) {
        this.contactId = contactId;
    }
    
    public int getPincode() {
        return pincode;
    }
  
    public void setPincode(int pincode) {
        this.pincode = pincode;
    }
         
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }
 
    public void setDob(String dob) {
        this.dob = dob;
    }
 
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBowlingType() {
        return bowlingType;
    }

    public void setBowlingType(String bowlingType) {
        this.bowlingType = bowlingType;
    }

    public String getBattingType() {
        return battingType;
    }
     
    public void setBattingType(String battingType) {
        this.battingType = battingType;
    }
    
    public String getAge() {
        return age;
    }
 
    public void setAge(String age) throws ParseException {   
        this.age = age;  
    }
        
    public String getImagePath() {
        return imagePath;
    }
 
    public void setImagePath(String imagePath) {   
        this.imagePath = imagePath;  
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
     
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
 
    public String getAddress() {
        return address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
}

