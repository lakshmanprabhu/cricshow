package com.ideas2it.cricshow.info;

import java.util.Date;
import java.util.List;

import com.ideas2it.cricshow.common.PlayingFormat;
import com.ideas2it.cricshow.info.TeamInfo;

/**
 * It stores the fields of match object as a info object which is used to 
 * communicate between view and controller in a application.
 */
public class MatchInfo {
    private int id;
    private Date playingDate;
    private String name;
    private String venue;
    private PlayingFormat playingFormat;
    private List<TeamInfo> teams;
    private List<TeamInfo> teamsNotInMatch;

    //Getters and Setters for MatchInfo object.
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
        
    public Date getPlayingDate() {
        return playingDate;
    }
  
    public void setPlayingDate(Date playingDate) {
        this.playingDate = playingDate;
    }
    
    public String getName() {
        return name;
    }
  
    public void setName(String name) {
        this.name = name;
    }
    
    public String getVenue() {
        return venue;
    }
  
    public void setVenue(String venue) {
        this.venue = venue;
    }

    public PlayingFormat getPlayingFormat() {
        return playingFormat;
    }
  
    public void setPlayingFormat(PlayingFormat playingFormat) {
        this.playingFormat = playingFormat;
    }
    
    public List<TeamInfo> getTeams() {
        return teams;
    }

    public void setTeams(List<TeamInfo> teams) {
        this.teams = teams;
    }
    
    public List<TeamInfo> getTeamsNotInMatch() {
        return teamsNotInMatch;
    }

    public void setTeamsNotInMatch(List<TeamInfo> teamsNotInMatch) {
        this.teamsNotInMatch = teamsNotInMatch;
    }
}

