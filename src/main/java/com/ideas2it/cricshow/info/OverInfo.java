package com.ideas2it.cricshow.info;

import com.ideas2it.cricshow.info.MatchInfo;

/**
 * It stores the field of over object as a info object which is used to 
 * communicate between view and controller in a application.
 */
public class OverInfo {
    private int id;
    private int overNumber;
    private int totalRuns;
    private int wicketCount;
    private int fourCount;
    private int sixCount;
    private int noBallCount;
    private int wideBallCount;
    private int byesCount;
    private int legByesCount;
    private MatchInfo matchInfo;

    // Getters and Setters for OverInfo credentials.
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOverNumber() {
        return this.overNumber;
    }

    public void setOverNumber(int overNumber) {
        this.overNumber = overNumber;
    }
        
    public int getTotalRuns() {
        return this.totalRuns;
    }

    public void setTotalRuns(int totalRuns) {
        this.totalRuns = totalRuns;
    }
    
    public int getWicketCount() {
        return this.wicketCount;
    }

    public void setWicketCount(int wicketCount) {
        this.wicketCount = wicketCount;
    }

    public int getFourCount() {
        return this.fourCount;
    }
 
    public void setFourCount(int fourCount) {
        this.fourCount = fourCount;
    }
 
    public int getSixCount() {
        return this.sixCount;
    }

    public void setSixCount(int sixCount) {
        this.sixCount = sixCount;
    }

    public int getNoBallCount() {
        return this.noBallCount;
    }

    public void setNoBallCount(int noBallCount) {
        this.noBallCount = noBallCount;
    }

    public int getWideBallCount() {
        return this.wideBallCount;
    }
     
    public void setWideBallCount(int wideBallCount) {
        this.wideBallCount = wideBallCount;
    }
    
    public int getByesCount() {
        return this.byesCount;
    }
 
    public void setByesCount(int byesCount) {   
        this.byesCount = byesCount;  
    }
        
    public int getLegByesCount() {
        return this.legByesCount;
    }
 
    public void setLegByesCount(int legByesCount) {   
        this.legByesCount = legByesCount;  
    }

    public MatchInfo getMatch() {
        return this.matchInfo;
    }

    public void setMatch(MatchInfo matchInfo) {
        this.matchInfo = matchInfo;
    }
}
