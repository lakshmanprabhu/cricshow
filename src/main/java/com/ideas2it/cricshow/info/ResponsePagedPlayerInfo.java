package com.ideas2it.cricshow.info;

import java.util.List;

import com.ideas2it.cricshow.info.PlayerInfo;

/**
 * It stores the noOfPages, currentPage and playerInfo object which is used for
 * pagination.
 */
public class ResponsePagedPlayerInfo {
    private int noOfPages;
    private int currentPage;
    private List<PlayerInfo> playerInfos;
    
    // Parameterized constructor of ResponsePagedPlayerInfo object.
    public ResponsePagedPlayerInfo(int noOfPages, int currentPage, 
            List<PlayerInfo> playerInfos) {
        this.noOfPages = noOfPages;
        this.currentPage = currentPage;
        this.playerInfos = playerInfos;
    }
    
    //Getters and Setters for ResponsePagedPlayerInfo object.
    public int getNoOfPages() {
        return noOfPages;
    }

    public void setNoOfPages(int noOfPages) {
        this.noOfPages = noOfPages;
    }
    
    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public List<PlayerInfo> getPlayerInfos() {
        return playerInfos;
    }
    
    public void setPlayerInfos(List<PlayerInfo> playerInfos) {
        this.playerInfos = playerInfos;
    }
}
