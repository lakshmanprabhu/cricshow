package com.ideas2it.cricshow.info;

import com.ideas2it.cricshow.info.MatchInfo;
import com.ideas2it.cricshow.info.TeamInfo;

/**
 * It stores the fields of match object, battingTeam and fieldingTeam as a info  
 * object which is used to communicate between view and controller in a 
 * application.
 */
public class PlayInfo {
    private MatchInfo matchInfo;
    private TeamInfo battingTeamInfo;
    private TeamInfo fieldingTeamInfo;

    //Getters and Setters for PlayInfo object.
    public MatchInfo getMatchInfo() {
        return matchInfo;
    }
    
    public void setMatchInfo(MatchInfo matchInfo) {
        this.matchInfo = matchInfo;
    }
    
    public TeamInfo getBattingTeamInfo() {
        return battingTeamInfo;
    }

    public void setBattingTeamInfo(TeamInfo battingTeamInfo) {
        this.battingTeamInfo = battingTeamInfo;
    }
    
    public TeamInfo getFieldingTeamInfo() {
        return fieldingTeamInfo;
    }

    public void setFieldingTeamInfo(TeamInfo fieldingTeamInfo) {
        this.fieldingTeamInfo = fieldingTeamInfo;
    }
}

