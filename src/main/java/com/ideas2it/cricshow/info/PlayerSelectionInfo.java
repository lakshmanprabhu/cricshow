package com.ideas2it.cricshow.info;

import java.util.List;

import com.ideas2it.cricshow.info.PlayerInfo;

/**
 * It stores the list of players in batting and fielding team as info object. 
 * Also it stores bowler, batsman, match, over, firstBatsman and secondBatsman 
 * id respectively.
 */
public class PlayerSelectionInfo {
    private int bowlerId;
    private int matchId;
    private int overId;
    private int firstBatsmanId;
    private int secondBatsmanId;
    private int previousBowlerId;
    private List<PlayerInfo> battingPlayerInfos;
    private List<PlayerInfo> fieldingPlayerInfos;   
    
    //Getters and Setters for PlayerSelectionInfo credentials.    
    public int getBowlerId() {
        return this.bowlerId;
    }

    public void setBowlerId(int bowlerId) {
        this.bowlerId = bowlerId;
    }

    public int getMatchId() {
        return this.matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }
                            
    public int getOverId() {
        return this.overId;
    }

    public void setOverId(int overId) {
        this.overId = overId;
    }
        
    public int getFirstBatsmanId() {
        return this.firstBatsmanId;
    }

    public void setFirstBatsmanId(int firstBatsmanId) {
        this.firstBatsmanId = firstBatsmanId;
    }         
               
    public int getSecondBatsmanId() {
        return this.secondBatsmanId;
    }
    
    public void setSecondBatsmanId(int secondBatsmanId) {
        this.secondBatsmanId = secondBatsmanId;
    }                        

    public int getPreviousBowlerId() {
        return this.previousBowlerId;
    }

    public void setPreviousBowlerId(int previousBowlerId) {
        this.previousBowlerId = previousBowlerId;
    }

    public List<PlayerInfo> getBattingPlayerInfos() {
        return battingPlayerInfos;
    }

    public void setBattingPlayerInfos(List<PlayerInfo> battingPlayerInfos) {
        this.battingPlayerInfos = battingPlayerInfos;
    }
    
    public List<PlayerInfo> getFieldingPlayerInfos() {
        return fieldingPlayerInfos;
    }

    public void setFieldingPlayerInfos(List<PlayerInfo> fieldingPlayerInfos) {
        this.fieldingPlayerInfos = fieldingPlayerInfos;
    }
}

