package com.ideas2it.cricshow.info;

/**
 * It stores the fields of user object as a info object which is used to 
 * communicate between view and controller in a application.
 */
public class UserInfo {
    private boolean status;
    private int id;
    private int loginAttempt;
    private String name;
    private String gender;
    private String role;
    private String emailAddress;
    private String phoneNumber;
    private String password;

    // Getters and Setters for UserInfo credentials.
    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getLoginAttempt() {
        return loginAttempt;
    }

    public void setLoginAttempt(int loginAttempt) {
        this.loginAttempt = loginAttempt;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }
 
    public void setGender(String gender) {
        this.gender = gender;
    }
 
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {   
        this.password = password;  
    }
}
