package com.ideas2it.cricshow.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import com.ideas2it.cricshow.exception.CricShowException;

/**
 * It is an encryption utility used to encrypt passwords.
 *
 */
public class EncryptionUtil {

    /**
     * It encrypts the password using messageDigest5 algorithm which takes any
     * length of input and returns as output a fixed length of digest value .
     *
     * @param passwordToHash - used to generate the hash of password. 
     * @return generatedPassword - is a encrypted password formed using md5.
     * @throws CricShowException which is a custom exception in which proper
     * context is sent to log it.  
     */
    public static String getEncryptedPassword(String passwordToHash) throws
            CricShowException {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(passwordToHash.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16)
                        .substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new CricShowException("Error at encrypting password", e);
        } 
        return generatedPassword;
    }
}



