package com.ideas2it.cricshow.utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.cricshow.entities.Contact;
import com.ideas2it.cricshow.entities.Match;
import com.ideas2it.cricshow.entities.Player;
import com.ideas2it.cricshow.entities.Team;
import com.ideas2it.cricshow.entities.User;
import com.ideas2it.cricshow.info.MatchInfo;
import com.ideas2it.cricshow.info.PlayerInfo;
import com.ideas2it.cricshow.info.TeamInfo;
import com.ideas2it.cricshow.info.UserInfo;


/**
 * It is a common util in which conversion of entity object to info object,
 * other conversion such as array to list and vice versa are included here.
 */
public class CommonUtil {

    /**
     * It converts list of string arrays of ids into a list of Integer ids.
     *
     * @param ids - is a list of string arrays of ids.
     * @return listOfIds - is a list of Integer ids.
     */
    public static List<Integer> convertStringArrayIdsToIntegerListIds(String[] ids) {
        List<Integer> listOfIds = new ArrayList<Integer>();
        for (String id : ids) {
            listOfIds.add(Integer.parseInt(id));
        }
        return listOfIds;
    }
    
    /**
     * It converts list of players into a list of playerInfos object.
     *
     * @param players - is a list of player object.
     * @return playerInfos - is a list of playerInfo object.
     * @throws ParseException during the calculation of age in a player.
     */
    public static List<PlayerInfo> convertPlayerListToPlayerInfos(List<Player> 
            players) throws ParseException {
        List<PlayerInfo> playerInfos = new ArrayList<PlayerInfo>();
        for (Player player : players) {
            playerInfos.add(convertPlayerEntityToPlayerInfo(player));
        }
        return playerInfos;
    }

    /**
     * It converts list of matches into a list of matchInfos object.
     *
     * @param matches - is a list of match object.
     * @return matchInfos - is a list of matchInfo object.
     */
    public static List<MatchInfo> convertMatchListToMatchInfos(List<Match> 
            matches) {
        List<MatchInfo> matchInfos = new ArrayList<MatchInfo>();
        for (Match match : matches) {
            matchInfos.add(convertMatchEntityToMatchInfo(match));
        }
        return matchInfos;
    }

    /**
     * It converts list of teams into a list of teamInfos object.
     *
     * @param teams - is a list of team object.
     * @return teamInfos - is a list of teamInfo object.
     */
    public static List<TeamInfo> convertTeamListToTeamInfos(List<Team> teams) {
        List<TeamInfo> teamsInfos = new ArrayList<TeamInfo>();
        for (Team team : teams) {
            teamsInfos.add(convertTeamEntityToTeamInfo(team));
        }
        return teamsInfos;
    }

    /**
     * It converts the team entity into teamInfo Object which was used by view
     * layer in the application.
     *
     * @param team - is a team entity object.
     * @return teamInfo - is a team info object.
     */
    public static TeamInfo convertTeamEntityToTeamInfo(Team team) {
        TeamInfo teamInfo = new TeamInfo();
        
        teamInfo.setStatus(team.getStatus());
        teamInfo.setId(team.getId());
        if (null != team.getName()) {
            teamInfo.setName(team.getName());
        }
        if (null != team.getCountry()) {
            teamInfo.setCountry(team.getCountry());
        }
        return teamInfo;
    }
    
    /**
     * It converts the teamInfo object into team entity which is used by service
     * layer in the application.
     *
     * @param teamInfo - is a team info object.
     * @return team - is a team entity object.
     */
    public static Team convertTeamInfoToTeamEntity(TeamInfo teamInfo) {
        Team team = new Team();
        
        team.setStatus(teamInfo.getStatus());
        team.setId(teamInfo.getId());
        if (null != teamInfo.getName()) {
            team.setName(teamInfo.getName());
        }
        if (null != teamInfo.getCountry()) {
            team.setCountry(teamInfo.getCountry());
        }
        return team;
    }
    
    /**
     * It converts the player object into playerInfo Object which was used by
     * view layer in the application.
     *
     * @param player - is a player entity object.
     * @return playerInfo - is a player info object.
     * @throws ParseException during age conversion from date of birth.
     */
    public static PlayerInfo convertPlayerEntityToPlayerInfo(Player player) 
            throws ParseException {
        PlayerInfo playerInfo = new PlayerInfo();
        Contact contact = player.getContact();
        
        playerInfo.setStatus(player.getStatus());
        playerInfo.setId(player.getId());
        playerInfo.setContactId(contact.getId());
        playerInfo.setPincode(contact.getPincode());
        if (null != player.getName()) {
            playerInfo.setName(player.getName());
        }
        if (null != player.getDob()) {
            playerInfo.setDob(player.getDob());
        }
        if (null != player.getRole()) {
            playerInfo.setRole(player.getRole());
        }
        if (null != player.getBattingType()) {
            playerInfo.setBattingType(player.getBattingType());
        }
        if (null != player.getBowlingType()) {
            playerInfo.setBowlingType(player.getBowlingType());
        }
        if (null != player.getAge()) {
            playerInfo.setAge(player.getAge());
        }
        if (null != player.getImagePath()) {
            playerInfo.setImagePath(player.getImagePath());
        }
        if (null != player.getCountry()) {
            playerInfo.setCountry(player.getCountry());
        }
        if (null != contact.getPhoneNumber()) {
            playerInfo.setPhoneNumber(contact.getPhoneNumber());
        }
        if (null != contact.getAddress()) {
            playerInfo.setAddress(contact.getAddress());
        }        
        return playerInfo;  
    }
    
    /**
     * It converts the playerInfo object into player object which is used by 
     * service layer in the application.
     *
     * @param playerInfo - is a player info object.
     * @return player - is a player entity object.
     * @throws ParseException during age conversion from date of birth.
     */
    public static Player convertPlayerInfoToPlayerEntity(PlayerInfo playerInfo) 
            throws ParseException {
        Player player = new Player();
        Contact contact =  new Contact();
        
        player.setStatus(playerInfo.getStatus());
        player.setId(playerInfo.getId());
        contact.setId(playerInfo.getContactId());
        contact.setPincode(playerInfo.getPincode());
        if (null != playerInfo.getName()) {
            player.setName(playerInfo.getName());
        }
        if (null != playerInfo.getDob()) {
            player.setDob(playerInfo.getDob());
        }
        if (null != playerInfo.getRole()) {
            player.setRole(playerInfo.getRole());
        }
        if (null != playerInfo.getBattingType()) {
            player.setBattingType(playerInfo.getBattingType());
        }
        if (null != playerInfo.getBowlingType()) {
            player.setBowlingType(playerInfo.getBowlingType());
        }
        if (null != playerInfo.getAge()) {
            player.setAge(playerInfo.getAge());
        }
        if (null != playerInfo.getImagePath()) {
            player.setImagePath(playerInfo.getImagePath());
        }
        if (null != playerInfo.getCountry()) {
            player.setCountry(playerInfo.getCountry());
        }
        if (null != playerInfo.getPhoneNumber()) {
            contact.setPhoneNumber(playerInfo.getPhoneNumber());
        }
        if (null != playerInfo.getAddress()) {
            contact.setAddress(playerInfo.getAddress());
        }  
              
        player.setContact(contact);
        return player;
    }
    
    /**
     * It converts the match entity into matchInfo Object which was used by view
     * layer in the application.
     *
     * @param match - is a match entity object.
     * @return matchInfo - is a match info object.
     */
    public static MatchInfo convertMatchEntityToMatchInfo(Match match) {
        MatchInfo matchInfo = new MatchInfo();
        
        matchInfo.setId(match.getId());
        if (null != match.getName()) {
            matchInfo.setName(match.getName());
        }
        if (null != match.getVenue()) {
            matchInfo.setVenue(match.getVenue());
        }
        if (null != match.getPlayingDate()) {
            matchInfo.setPlayingDate(match.getPlayingDate());
        }
        if (null != match.getPlayingFormat()) {
            matchInfo.setPlayingFormat(match.getPlayingFormat());
        }
        return matchInfo;  
    }
    
    /**
     * It converts the matchInfo object into match entity which is used by 
     * service layer in the application.
     *
     * @param matchInfo - is a match info object.
     * @return match - is a match entity object.
     */
    public static Match convertMatchInfoToMatchEntity(MatchInfo matchInfo) {
        Match match = new Match();

        match.setId(matchInfo.getId());
        if (null != matchInfo.getName()) {
            match.setName(matchInfo.getName());
        }
        if (null != matchInfo.getVenue()) {
            match.setVenue(matchInfo.getVenue());
        }
        if (null != matchInfo.getPlayingDate()) {
            match.setPlayingDate(matchInfo.getPlayingDate());
        }
        if (null != matchInfo.getPlayingFormat()) {
            match.setPlayingFormat(matchInfo.getPlayingFormat());
        }
        return match;
    }
    
    /**
     * It converts the user entity into userInfo Object which was used by view
     * layer in the application.
     *
     * @param user - is a user entity object.
     * @return userInfo - is a user info object.
     */
    public static UserInfo convertUserEntityToUserInfo(User user) {
        UserInfo userInfo = new UserInfo();
        
        userInfo.setStatus(user.getStatus());
        userInfo.setId(user.getId());
        userInfo.setLoginAttempt(user.getLoginAttempt());
        if (null != user.getName()) {
            userInfo.setName(user.getName());
        }
        if (null != user.getGender()) {
            userInfo.setGender(user.getGender());
        }
        if (null != user.getRole()) {
            userInfo.setRole(user.getRole());
        }
        if (null != user.getEmailAddress()) {
            userInfo.setEmailAddress(user.getEmailAddress());
        }
        if (null != user.getPassword()) {
            userInfo.setPassword(user.getPassword());
        }
        if (null != user.getPhoneNumber()) {
            userInfo.setPhoneNumber(user.getPhoneNumber());
        }        
        return userInfo;  
    }
    
    /**
     * It converts the userInfo object into user entity which is used by service
     * layer in the application.
     *
     * @param userInfo - is a user info object.
     * @return user - is a user entity object.
     */
    public static User convertUserInfoToUserEntity(UserInfo userInfo) {
        User user = new User();
        
        user.setStatus(userInfo.getStatus());
        user.setId(userInfo.getId());
        user.setLoginAttempt(userInfo.getLoginAttempt());
        if (null != userInfo.getName()) {
            user.setName(userInfo.getName());
        }
        if (null != userInfo.getGender()) {
            user.setGender(userInfo.getGender());
        }
        if (null != userInfo.getRole()) {
            user.setRole(userInfo.getRole());
        }
        if (null != userInfo.getEmailAddress()) {
            user.setEmailAddress(userInfo.getEmailAddress());
        }
        if (null != userInfo.getPassword()) {
            user.setPassword(userInfo.getPassword());
        }
        if (null != userInfo.getPhoneNumber()) {
            user.setPhoneNumber(userInfo.getPhoneNumber());
        }        
        return user;  
    }   
}
