package com.ideas2it.cricshow.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.ideas2it.cricshow.common.Constants;
import com.ideas2it.cricshow.exception.CricShowException;

/**
 * It is a date utility in which conversion of string to date and vice versa is
 * done.
 */
public class DateUtil {

    /**
     * Takes date as string and returns as a date type to user. The valid date
     * format is yyyy-MM-dd.
     *
     * @param dateAsString - is a input as string from user.
     * @return date if it is in valid format. 
     * @throws CricShowException if input is not in correct format which 
     * occured during parsing operation.
     */
    public static Date fetchPlayingDate(String dateAsString) throws
            CricShowException {
        Date date = new Date();
        try {
            date = new SimpleDateFormat(Constants.LABEL_DATE_FORMAT).parse(
                         dateAsString);  
        } catch (ParseException e) {
            throw new CricShowException(Constants.LABEL_INVALID_DATE, e);
        }
        return date;     
    }
    
    /**
     * Takes date as date type and returns as a string date to user. The valid 
     * date format is yyyy-MM-dd.
     *
     * @param date - is a input date as date type from user.
     * @return dateToString if it is in valid format. 
     */
    public static String convertDateToString(Date date) {
       String dateToString = new String();
       DateFormat dateFormat = new SimpleDateFormat(Constants.LABEL_DATE_FORMAT);
       dateToString = dateFormat.format(date);
       return dateToString;
   }
}
