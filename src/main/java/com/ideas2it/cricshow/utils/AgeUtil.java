package com.ideas2it.cricshow.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;

/**
 * It provides utility function for age calculation in which user inputs the 
 * date of birth.
 *
 */
public class AgeUtil {

    /**
     * Calculates age from the Date of Birth input.Initially it checks the 
     * format of date of birth to be yyyy-MM-dd, if incorrect format is entered  
     * then sends an message to it.
     *
     * @param dob - is a date of birth.
     * @return currentAge - is a calculated age which tells .
     */
    public static String calculateAge(String dob) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date dobFormat = dateFormat.parse(dob);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dobFormat);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int date = calendar.get(Calendar.DATE);
        LocalDate dateOfBirth = LocalDate.of(year, month, date);
        LocalDate currentDate = LocalDate.now();
        Period difference = Period.between(dateOfBirth, currentDate);
        String currentAge = String.valueOf(difference.getYears()  
            + " years " + difference.getMonths() + " months "  
            + difference.getDays() + " days ");
        return currentAge;   
    }
  }

