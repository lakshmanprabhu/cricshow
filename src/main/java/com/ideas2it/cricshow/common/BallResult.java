package com.ideas2it.cricshow.common;
/**
 * It contains the possibility that can occured after a ball is being bowled 
 * which gives runs in that ball.
 * for e.g wide ball, six, four, byes, legbyes, dotballs, etc.
 *
 */
public enum BallResult {
    DOTBALL(0),
    ONES(1),
    TWOS(2),
    THREES(3),
    FOURS(4),
    SIXES(6),
    BYES(-1),
    LEGBYES(-2),
    NOBALL(-3),
    WIDEBALL(-4),
    WICKET(-5);
    
    private int value;
        private BallResult(int value) {
            this.value = value;
        }
        public int getValue() {
            return this.value;
        }
}

