package com.ideas2it.cricshow.common;

/**
 * It consist of list of playing format available in a match to be scheduled.
 * The playing format available for a match are one day, test and t20.
 */
public enum PlayingFormat { 
    ONEDAY,
    T20,
    TEST;
}

