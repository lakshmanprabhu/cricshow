package com.ideas2it.cricshow.common;

/**
 * It contains the common constants used in the project.
 */
public class Constants {

    // Label constants for views(Jsp PAGES).
    public static final String LABEL_ERROR_PAGE = "error.jsp";
    public static final String LABEL_VIEW_PLAYER_PAGE = "viewPlayer";
    public static final String LABEL_UPDATE_PLAYER_PAGE = "updatePlayer";
    public static final String LABEL_VIEW_PLAYERS_PAGE = "viewPlayers";
    public static final String LABEL_ADD_PLAYER_PAGE = "addPlayer";
    public static final String LABEL_ADD_TEAM_PAGE = "addTeam";
    public static final String LABEL_ADD_PLAYERS_TO_TEAM_PAGE = "addPlayerToTeam";
    public static final String LABEL_VIEW_TEAM_PAGE = "viewTeam";
    public static final String LABEL_VIEW_TEAMS_PAGE = "viewTeams";
    public static final String LABEL_UPDATE_TEAM_PAGE = "updateTeam";
    public static final String LABEL_ADD_MATCH_PAGE = "addMatch";
    
    // Label constants for manipulation of data.
    public static final String LABEL_ID = "id";
    public static final String LABEL_NAME = "name";
    public static final String LABEL_STATUS ="status";
    public static final String LABEL_COUNTRY = "country";
    public static final String LABEL_ROLE = "role";
    public static final String LABEL_BATTING_TYPE = "battingType";
    public static final String LABEL_BOWLING_TYPE = "bowlingType";
    public static final String LABEL_DATE_OF_BIRTH = "dateOfBirth";
    public static final String LABEL_AGE = "age";
    public static final String LABEL_ADDRESS = "address";
    public static final String LABEL_PHONE_NUMBER = "phoneNumber";
    public static final String LABEL_PINCODE = "pincode";
    public static final String LABEL_IMAGE = "image";
    public static final String LABEL_IMAGE_LOCATION = 
            "http://localhost:8080/uploads/";
    public static final String LABEL_DATE_FORMAT = "yyyy-MM-dd";
    public static final String LABEL_INVALID_DATE = "\nInvalid date";
    public static final String LABEL_WICKETKEEPER = "WicketKeeper";
    public static final String LABEL_BATSMAN = "Batsman";
    public static final String LABEL_BOWLER = "Bowler";
    public static final String LABEL_ALLROUNDER = "Allrounder";
    public static final String LABEL_PLAYER = "player";
    public static final String LABEL_CONTACT = "contact";
    public static final String LABEL_TEAM = "team";
    public static final String LABEL_MATCH = "match";
    public static final String LABEL_USER = "user";
    public static final String LABEL_OVER = "over";
    public static final String LABEL_OVER_DETAIL = "overDetail";
    public static final String LABEL_PLAYER_INFO = "playerInfo";
    public static final String LABEL_CONTACT_INFO = "contactInfo";
    public static final String LABEL_TEAM_INFO = "teamInfo";
    public static final String LABEL_MATCH_INFO = "matchInfo";
    public static final String LABEL_USER_INFO = "userInfo";
    public static final String LABEL_OVER_INFO = "overInfo";
    public static final String LABEL_OVER_DETAIL_INFO = "overDetailInfo";
    public static final String LABEL_PAGED_PLAYER_INFO = "pagedPlayerInfo";
    public static final String LABEL_PAGE = "page";
    public static final String LABEL_PLAYERS = "players";
    public static final String LABEL_NUMBER_OF_PAGES = "noOfPages";
    public static final String LABEL_CURRENT_PAGE = "currentPage";
    public static final String LABEL_LAST_PAGE = "last";
    public static final String LABEL_APPLICATION_TYPE_JSON = "application/json";
    public static final String LABEL_SAVE_PLAYER = "/savePlayer";
    public static final String LABEL_EDIT_PLAYER = "/editPlayer";
    public static final String LABEL_UPDATE_PLAYER = "/updatePlayer";
    public static final String LABEL_DELETE_PLAYER = "/deletePlayer";
    public static final String LABEL_VIEW_REMAINING_PLAYERS = "/viewRemainingPlayers";
    public static final String LABEL_VIEW_PLAYERS = "/viewPlayers";
    public static final String LABEL_VIEW_PLAYER = "/viewPlayer";
    public static final String LABEL_REDIRECT_VIEW_PLAYERS = "redirect:/viewPlayers";
    public static final String LABEL_CREATE_PLAYER = "/createPlayer";
    public static final String LABEL_ADD_PLAYER = "addPlayer";
    public static final String LABEL_PLAYERS_IN_TEAM = "playersInTeam";
    public static final String LABEL_BATSMAN_COUNT = "batsmanCount";
    public static final String LABEL_BOWLER_COUNT = "bowlerCount";
    public static final String LABEL_WICKETKEEPER_COUNT = "wicketKeeperCount";
    public static final String LABEL_TOTAL_COUNT = "totalCount";
    public static final String LABEL_PLAYERS_BY_COUNTRY = "playersByCountry";
    public static final String LABEL_PLAYERS_NOT_IN_TEAM = "playersNotInTeam";
    public static final String LABEL_REMOVE_PLAYERS = "removePlayers";
    public static final String LABEL_ADD_PLAYERS = "addPlayers";    
    public static final String LABEL_CREATE_TEAM = "/createTeam";
    public static final String LABEL_SAVE_TEAM = "/saveTeam";
    public static final String LABEL_ADD_PLAYERS_TO_TEAM = "/addPlayerToTeam";
    public static final String LABEL_REDIRECT_VIEW_TEAM = "redirect:/viewTeam";
    public static final String LABEL_REDIRECT_VIEW_TEAMS = "redirect:/viewTeams";
    public static final String LABEL_VIEW_TEAM = "/viewTeam";
    public static final String LABEL_VIEW_TEAMS = "/viewTeams";
    public static final String LABEL_DELETE_TEAM = "/deleteTeam";
    public static final String LABEL_UPDATE_TEAM = "/updateTeam";
    public static final String LABEL_EDIT_TEAM = "/editTeam";
    public static final String LABEL_CREATE_MATCH = "/createMatch";
    public static final String LABEL_SAVE_MATCH = "/saveMatch";
    public static final int PAGE_CONTENT_RESTRICTION = 5;
    
    //Label constants for errors.
    public static final String LABEL_ERROR_MESSAGE = "err ";
    public static final String LABEL_ERROR_DUE_TO_PARSING_DATE = 
            "Error at creating player due to parsing date ";
    public static final String LABEL_ERROR_AT_CREATING_PLAYER = 
            "Error at creating player ";
    public static final String LABEL_ERROR_AT_CREATING_TEAM = 
            "Error at creating team ";
    public static final String LABEL_ERROR_AT_ADDING_PLAYER_TO_TEAM = 
            "Error at adding players to team ";
    public static final String LABEL_ERROR_AT_SAVING_IMAGE = 
            "Error at saving image ";
    public static final String LABEL_EXCEPTION = "Exception occured ";
    public static final String LABEL_ERROR_AT_DISPLAYING_PLAYER_WITH_ID = 
            "Error at viewing player details for id ";
    public static final String LABEL_ERROR_AT_DISPLAYING_TEAM_WITH_ID = 
            "Error at viewing team details for id ";
    public static final String LABEL_ERROR_AT_DISPLAYING_PLAYER = 
            "Error at viewing player details ";
    public static final String LABEL_ERROR_AT_SAVING_UPDATED_PLAYER =
            "Error at saving updated information ";
    public static final String LABEL_ERROR_AT_RETRIEVING_FIRST_FIVE_PLAYERS =
            "Error at viewing first five record ";
    public static final String LABEL_ERROR_AT_VIEWING_REMAINING_PLAYERS =
            "Error at viewing remaining players ";
    public static final String LABEL_ERROR_AT_DELETING_PLAYER = 
            "Error at deleting player ";
    public static final String LABEL_ERROR_AT_DELETING_TEAM = 
            "Error at deleting team ";
    public static final String LABEL_ERROR_AT_UPDATING_PLAYER = 
            "Error at updating player ";
    public static final String LABEL_ERROR_AT_COUNTING_PLAYERS = 
            "Error at counting total players "; 
    public static final String LABEL_ERROR_AT_FINDING_PLAYERS_TO_A_COUNTRY = 
            "Error at finding players for a country ";
    public static final String LABEL_ERROR_AT_FINDING_PLAYERS_TO_ADD_IN_TEAM = 
            "Players cannot be found to add in a team ";
    public static final String LABEL_ERROR_AT_FINDING_PLAYERS_FOR_A_TEAM_ID = 
            "Players cannot found for teamId ";
    public static final String LABEL_ERROR_AT_COUNTING_BATSMAN_FOR_A_TEAM_ID = 
            "Error at counting batsman role for teamId ";
    public static final String LABEL_ERROR_AT_COUNTING_BOWLER_FOR_A_TEAM_ID = 
            "Error at counting bowler role for teamId ";                
    public static final  
            String LABEL_ERROR_AT_COUNTING_WICKETKEEPER_FOR_A_TEAM_ID = 
            "Error at counting wicketKeeper role for teamId "; 
    public static final 
            String LABEL_ERROR_AT_COUNTING_TOTAL_PLAYERS_FOR_A_TEAM = 
            "Error at counting total player for teamId ";
    public static final 
            String LABEL_ERROR_IN_CLOSING_SESSION = "Error in closing session ";
    public static final String LABEL_OOPS = "Oops ";
    public static final String LABEL_PLAYER_NOT_CREATED = 
            "Player cannot be created ";        
    public static final String LABEL_UNABLE_TO_VIEW_PLAYER = 
            "Unable to view player ";        
    public static final String LABEL_UNABLE_TO_VIEW_ALL_PLAYER = 
            "Unable to view all player details ";                
    public static final String LABEL_UPDATION_FAILED_TO_PLAYER = 
            "Updating failed to player ";                 
    public static final String LABEL_FAILED_TO_DELETE_PLAYER = 
            "Failed to delete player ";  
    public static final String LABEL_NO_PLAYERS_TO_COUNT = 
            "No players available to count ";         
    public static final String LABEL_NO_PLAYERS_FOUND_FOR_THIS_COUNTRY = 
            "Failed to find players for this country ";         
    public static final String LABEL_NO_PLAYERS_AVAIL_TO_ADD_IN_TEAM = 
            "Failed to find players to add in team "; 
    public static final String LABEL_NO_PLAYERS_FOUND_FOR_THIS_TEAM = 
            "Failed to find players for team "; 
    public static final String LABEL_NO_BATSMAN_TO_COUNT_IN_THIS_TEAM = 
            "Failed to count batsman role ";                 
    public static final String LABEL_NO_BOWLER_TO_COUNT_IN_THIS_TEAM = 
            "Failed to count bowler role "; 
    public static final String LABEL_NO_WICKETKEEPER_TO_COUNT_IN_THIS_TEAM = 
            "Failed to count wicketKeeper role "; 
    public static final String LABEL_NO_PLAYERS_TO_COUNT_IN_THIS_TEAM = 
            "Failed to count total players ";         
                       
    //Label constants for Json object
    public static final String LABEL_PLAYER_ID = "Id";
    public static final String LABEL_PLAYER_NAME = "PlayerName";
    public static final String LABEL_PLAYER_ROLE = "Role";
    public static final String LABEL_PLAYER_STATUS = "Status";
    public static final String LABEL_PLAYER_COUNTRY = "Country";
    
    //Constanst for ball result
    public static final int DOTBALL = 0;
    public static final int ONES = 1;
    public static final int TWOS = 2;
    public static final int THREES = 3;
    public static final int FOURS = 4;
    public static final int SIXES = 6;
    public static final int BYES = -1;
    public static final int LEGBYES = -2;
    public static final int NOBALL = -3;
    public static final int WIDEBALL = -4;
    public static final int WICKET = -5;
}
