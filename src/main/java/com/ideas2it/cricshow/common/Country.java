package com.ideas2it.cricshow.common;

/**
 * List of countries available to a players to represent in a team. Available
 * countries to represent a team are india, australia, england, bangladesh,
 * newzealand, srilanka, pakistan, southafrica and westindies.
 */
public enum Country { INDIA, AUSTRALIA, ENGLAND, BANGLADESH,
            NEWZEALAND, SRILANKA, PAKISTAN, SOUTHAFRICA,
            WESTINDIES;
}


