package com.ideas2it.cricshow.config;

import java.io.IOException;
 
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * It provides the front end authentication of CricShow application. It stops 
 * from accessing the url without having correct authentication and authorisation
 * to it. Also blocks the request from anonymous user trying to access it.
 *
 * @author    Lakshmanprabhu Naidu
 *
 * @date    25/08/2019
 */
public class CricShowFilter implements Filter {
    
    /**
     * The servlet container calls the init method exactly once after 
     * instantiating the filter.
     *
     * @param fconfig - is a filter configuration object used by a servlet 
     * container to pass information to a filter during initialization.
     * @throws ServletException when failed in initialization.
     */ 
    public void init(FilterConfig fConfig) throws ServletException {}
    
    /**
     * It is called by container each time a request/response pair is passed 
     * through the chain due to a client request for a resource. It checks 
     * whether the user have access or not and based on the access it will 
     * forward the response to the user.
     * for e.g if a user have proper access to it, then this doFilter will 
     * forward to homepage of web application otherwise it will redirect to 
     * index page which tells the user a appropriate message.  
     *
     * @param request - is a ServletRequest request. 
     * @param response - is a ServletResponse response.
     * @param fconfig - is a invocation chain of filtered request for a resource.
     * @throws ServletException when failed to pass request through chain.
     */
    public void doFilter(ServletRequest request, ServletResponse response,
             FilterChain chain) throws IOException, ServletException {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpSession session = httpRequest.getSession(Boolean.FALSE);
            boolean isLoggedIn = (null != session 
                                       && null != session.getAttribute("username"));
            String loginURI = httpRequest.getContextPath() + "/signIn";
            String signUpURI = httpRequest.getContextPath() + "/signUp";
            String registerURI = httpRequest.getContextPath() + "/register";
            boolean isLoginRequest = httpRequest.getRequestURI().equals(loginURI);
            boolean isSignUpRequest = httpRequest.getRequestURI().equals(signUpURI);
            boolean isRegisterRequest = httpRequest.getRequestURI().equals(registerURI);
            if (isLoggedIn || isLoginRequest || isSignUpRequest || isRegisterRequest) {
                chain.doFilter(request, response);
            } else {
                RequestDispatcher dispatcher = request
                        .getRequestDispatcher("index.jsp");
                dispatcher.forward(request, response);
            }
    }
    
    /**
     * It is only called once if all threads within the filter's doFilter method 
     * have exited or after a timeout period has passed.
     */
    public void destroy() {}
}
