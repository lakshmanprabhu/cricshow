package com.ideas2it.cricshow.config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.PropertyConfigurator;
/**
 * It provides the ContextListener for logger which was included at dao and
 * controller level of the application in case if any errors or mishap occurs
 * during the execution of the project.
 *
 * @author    Lakshmanprabhu Naidu
 *
 * @date    25/08/2019
 */ 
public class ContextListener implements ServletContextListener {
 
    /**
     * Initialize log4j when the application is being started.
     *
     * @param event - is a event class for notifications about changes to the 
     * servlet context of a web application. It takes the configuration file
     * which was located at resources folder in which the level of log to be 
     * implemented and the location where the log to be saved for future use
     * is given in it. 
     */
    public void contextInitialized(ServletContextEvent event) {     
        PropertyConfigurator.configure("log4j.properties");  
    }
    
    /**
     * Destroys log4j running when the application is being stopped. Destroys
     * in the sense it stops recording the log when the application is exited.
     *
     * @param event - is a event class for notification in which the application
     * exit is notified to the servlet context to stop the recording of log.
     */ 
    public void contextDestroyed(ServletContextEvent event) {}  
}
