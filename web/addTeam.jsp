<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
  <head>
    <title> </title>
    <link rel="stylesheet" type="text/css" href="/styles/teamstyles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    		
</script>
  </head>
  <body>
    <div class="top-div">
      <img class="img-style" src="/images/crickethome.jpg"/>
    </div> 
    <h3>CricShow 
      <span>&nbspTournament</span>
    </h3>
    <form:form action="saveTeam" method="post" modelAttribute="teamInfo">
      <div class = "flex-container">
        <fieldset class="box"  >
          <legend> Team Information</legend>
            <label> Name  </label><br>
              <form:input type="text" path="name" maxlength="40" required="required"/><br>
            <label> Country </label> <br>
              <form:select path="country"> 
                  <option > Choose country</option>
                  <form:option value = "INDIA" >India </form:option>
                  <form:option value = "AUSTRALIA" >Australia </form:option>
                  <form:option value = "ENGLAND" >England </form:option>
                  <form:option value = "BANGLADESH" >Bangladesh </form:option>
                  <form:option value = "NEWZEALAND" >NewZealand </form:option>
                  <form:option value = "SRILANKA" >SriLanka </form:option>
                  <form:option value = "PAKISTAN" >Pakistan </form:option>
                  <form:option value = "SOUTHAFRICA" >SouthAfrica </form:option>
                  <form:option value = "WESTINDIES" >WestIndies </form:option>
              </form:select> <br>
        </fieldset>
</div> 
      <input class="save" type="submit" value="save"/>
    </form:form>
    <form:form action="viewTeams" method="get"> 
      <input type="submit" value="back"/>
    </form:form>
  </body>
</html>
