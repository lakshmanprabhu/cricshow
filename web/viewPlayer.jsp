<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
  <head>
    <title>Single View</title>
    <link rel="stylesheet" type="text/css" href="/styles/styles.css">
  </head>
  <body>
    <div align="center">
      <table cellpadding="8" cellspacing="5" style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9 ; border-radius:15px;">
        <caption> <h2> <span>View Player Detail</span> </h2> </caption>
        <tr> <td colspan=2 align="center"> <img src="${playerInfo.imagePath}"  style ="width:100px; height:100px;border-radius: 15px;"> </img></td>
        </tr>
        <tr>
            <th> ID : </th>
            <td> <c:out value="${playerInfo.id}" /> </td>
        </tr>
        <tr>
            <th> Name : </th>
            <td> <c:out value="${playerInfo.name}" /> </td>
        </tr>
        <tr>
           <th> D.O.B : </th>
           <td> <c:out value="${playerInfo.dob}" /> </td>
        </tr>
        <tr>
           <th> Role : </th>
           <td> <c:out value="${playerInfo.role}" /> </td>
        </tr>
        <tr>
           <th> Batting-Type : </th>
           <td> <c:out value="${playerInfo.battingType}" /> </td>
        </tr>
        <tr>
           <th> Bowling-Type : </th>
           <td> <c:out value="${playerInfo.bowlingType}" /> </td>
        </tr>
        <tr>
           <th> Country : </th>
           <td> <c:out value="${playerInfo.country}" /> </td>
        </tr>
        <tr>
           <th> Age : </th>
           <td> <c:out value="${playerInfo.age}" /> </td>
        </tr>
        <tr>   
           <th> Status : </th>
           <td><c:choose>  
                  <c:when test="${playerInfo.status==false}">  
                    <c:out value="Inactive"></c:out>  
                  </c:when>  
                  <c:otherwise>  
                    <c:out value="Active"></c:out>  
                  </c:otherwise>  
                </c:choose> </td>
        </tr>
        <tr>
           <th> PhoneNumber : </th>
           <td> <c:out value="${playerInfo.phoneNumber}" /> </td>
        </tr>
        <tr>
           <th> Address : </th>
           <td> <c:out value="${playerInfo.address}" /> </td>
        </tr>
        <tr>
           <th> Pincode : </th>
           <td> <c:out value="${playerInfo.pincode}" /> </td>
        </tr>   
      </table>
      <form:form action="viewPlayers" method="get"> 
      <input type="submit" value="back"/>
      </form:form> 
    </div>   
  </body>
</html>
