<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <title> All Team Details </title>
    <link rel="stylesheet" type="text/css" href="/styles/styles.css">
    <style>
    td {
     text-align:center;
    }

     </style>
  </head>
  <body>
    <div align="center">
      <table style =" background-color:#9ed2d9; border-radius:15px;
       box-shadow:15px 15px 3px #9dd2c9 ;" cellpadding="5">
        <caption> <h2> <span>View All Matches</span> </h2> </caption>
        <tr style="text-align:center;">
        <td colspan=3>
        <button value="back" onclick="location.href = 'home.jsp';">back</button>
         <td colspan=3>
        <button value="create" onclick="location.href ='createMatch';">create</button>
        </td></tr>
        <tr>
            <th> ID  </th>
            <th> Name  </th>
            <th> Venue  </th> 
            <th> Playing Format  </th> 
            <th> Playing Date  </th> 
            <th> Edit</th>
            <th> Delete</th>
            <th> Start Play</th>
        </tr>
        <c:forEach var="match" items="${viewmatch}">
        <tr>
              <td> <c:out value="${match.id}" /> </td>
              <td> <a title="More details" href="viewMatch?id=${match.id}">
                      <c:out value="${match.name}" /></a></td>
              <td> <c:out value="${match.venue}"/> </td>
              <td> <c:out value="${match.playingFormat}"/> </td>
              <td> <c:out value="${match.playingDate}"/> </td>
              <td> 
                  <button type="submit" onclick="location.href='updateMatch?id=${match.id}';">update</button>
              </td>
              <td>
                  <button type="submit" onclick="location.href='deleteMatch?id=${match.id}';">delete</button>             
              </td> 
              <td>
                  <button type="submit" onclick="location.href='startMatch?id=${match.id}';">play</button>             
              </td>             
        </tr>
        </c:forEach>
      </table>
    </div>   
  </body>
</html>
