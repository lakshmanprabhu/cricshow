<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
<html>
<head>
	<title>User Management Application</title>
	<link rel="stylesheet" type="text/css" href="/styles/styles.css">
	<script type="text/javascript" src="/scripts/checkSignUp.js">
    </script>
</head>
<body>
	<center>
		<h2>User Account</h2>
	</center>
	<center>
	<form:form id="signUpForm" action="register" method="post" modelAttribute="userInfo">
      <div class = "sign-up-container">
        <fieldset class="sign-up-box">
          <legend style="text-align:center;"> Sign Up</legend>
            <label style="margin-top:10px"> Name </label><br>
              <form:input type="text" path="name" maxlength="40" required="required" style="margin-top:10px"/> <br>
            <label style="margin-top:10px"> Email Address </label> <br>
              <form:input type="email" path="emailAddress" required= "required" id="emailAddress" style="margin-top:10px"/> <br>
            <label style="margin-top:10px;"> Password </label> <br>
              <form:input type="password" path="password" 
                     required="required" style="margin-top:10px;margin-bottom:10px;"/> <br>
            <label> Role </label><br><br>
              <form:select path="role">
                  <option value = "Admin">Admin </option>
                  <option value = "Manager">Manager </option>
              </form:select> <br>
            <label style="margin-top:10px"> Phone Number </label> <br>
              <form:input type="tel" path="phoneNumber" title="Only 10 digit number"
                     pattern="\d{10}" maxlength="10" style="margin-top:10px; margin-bottom:10px;"/> <br>
            <label> Gender </label><br><br>
            <form:radiobutton path="gender" value="male" style="text-align:left;"/>Male 
            <form:radiobutton path="gender" value="female" />Female
         </fieldset>
       </div>
       <input type="submit" value="register" id="okButton" style="margin-top:15px" disabled="disabled"/>
     </form:form>	
     </center>
     <script>
        isSignUp();
     </script>
</body>
</html>
