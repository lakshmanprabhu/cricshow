<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
  <head>
    <title>Update</title>
    <link rel="stylesheet" type="text/css" href="/styles/teamstyles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <style> 
   option[value=""][disabled] {
       display:none;
   }
   td {
     text-align:center;
    }    
    #hide {
        opacity:0.4;
    }
   </style>
   <script type="text/javascript" src="/scripts/batsmanSelection.js">
   </script>
  </head>
  <body>
  <form action="resumeOver" method="post">
  <div class = "flex-container"> 
      <table id = "addBatsman" class="box" cellpadding="5" 
        style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9;
         border-radius:15px;">
        <caption> <h3> <span>Batting Team Players List</span> </h3> </caption>
        <tr>
           <th>Id </th>
            <th> Name</th>
              <th>Country</th>
              <th>Role</th>
               <th>Select One Batsman </th>
         </tr>
        <c:forEach var="player" items="${playerSelectionInfo.battingPlayerInfos}">
        <c:choose> 
        <c:when test="${playerSelectionInfo.firstBatsmanId ne player.id && playerSelectionInfo.secondBatsmanId ne player.id}">  
        <tr>
              <td> <c:out value="${player.id}" /> </td>
              <td> <c:out value="${player.name}" /> </td>
              <td> <c:out value="${player.country}" /> </td> 
              <td> <c:out value="${player.role}" /> </td>    
              <td class="addBatsman"> <input type="checkbox" onclick = "selectOneBatsman()" name="addBatsman" 
                    value="${player.id}"/>  </td>     
        </tr>
        </c:when> 
        <c:otherwise> 
            <tr id = "hide">
              <td> <c:out value="${player.id}" /> </td>
              <td> <c:out value="${player.name}" /> </td>
              <td> <c:out value="${player.country}" /> </td> 
              <td> <c:out value="${player.role}" /> </td>      
            </tr> 
         </c:otherwise>
         </c:choose>
        </c:forEach>
      </table>
      </div>
      <input type = "hidden" name = "matchId" value =${playerSelectionInfo.matchId} />
      <input type = "hidden" name = "overId" value =${playerSelectionInfo.overId} />
      <input type = "hidden" name = "bowlerId" value =${playerSelectionInfo.bowlerId} />
      <input type = "hidden" name = "secondBatsmanId" value =${playerSelectionInfo.secondBatsmanId} />
      <input type = "submit" value ="Add Batsman"></input>
    </form>
    </body>
</html>
