<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
  <head>
    <title> </title>
    <link rel="stylesheet" type="text/css" href="/styles/matchstyles.css">
    <script type="text/javascript" src="/scripts/teamSelection.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <div class="top-div">
      <img class="img-style" src="/images/crickethome.jpg"/>
    </div> 
    <h3>CricShow 
      <span>&nbspTournament</span>
    </h3>
      <form:form action="addTeamsToMatch" method="post" modelAttribute="matchInfo">
      <div class = "flex-container"> 
          <table class="addteamshead" cellpadding="5";>
            <tr>
              <td class="addteams"> ID <form:input type="text"
                  path="id" readonly="readonly" value="${matchInfo.id}"/> </td>
              <td class="addteams"> Name <form:input type="text"
                  path="name" readonly="readonly" value="${matchInfo.name}"/> </td>
              <td class="addteams"> Venue<form:input type="text" 
                  path="venue" readonly="readonly" value="${matchInfo.venue}"/> </td>
              <td class="addteams"> Playing Date <input type="date" 
                  name="dateOfPlaying" readonly="readonly" value="${dateOfPlaying}"></input> 
              </td>
              <td class="addteams"> Playing Format<form:input type="text" 
                  path="playingFormat" readonly="readonly" value="${matchInfo.playingFormat}"/> 
              </td>
            </tr>
          </table>
          </div>
          <br>
          <div class = "flex-container"> 
            <table id="addTwoTeam" class="addteamsbody" cellpadding="5";>
              <tr>
                <th>Id </th>
                <th> Name</th>
                <th>Country</th>
                <th>Add Teams </th>
              </tr>
              <c:forEach var="team" items="${matchInfo.teams}">
              <tr>
                <td class="addteams"> <c:out value="${team.id}" /> </td>
                <td class="addteams"> <c:out value="${team.name}" /> </td>
                <td class="addteams"> <c:out value="${team.country}" /> </td>
                <td class="addteams"> <input type="checkbox" onclick="selectTwoTeam()" name="addteam" 
                    value="${team.id}"/> 
                </td>
              </tr>
              </c:forEach>
            </table>
         </div>
         <br>
         <input class="save" type="submit" value="addTeams">
      </form:form>
  </body>
</html>
