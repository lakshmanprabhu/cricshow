<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<title>User Management Application</title>
	<link rel="stylesheet" type="text/css" href="/styles/styles.css">
	<style>
	.emailaddress:valid {
  border-color: black;
}
.emailaddress:invalid {
  border-color: red;
}
	</style>
	<script type="text/javascript" src="/scripts/checkLogin.js">
    </script>
</head>
<body>
	<center>
		<h2>Login Page</h2>
    <h4> Not Registered yet?<a href="signUp"> Click here to register </a> </h4>
	<div><p> ${err} </p> </div></center>
	<center>
	<div class = "index-container">
	<form id="loginForm" action="signIn" method="post">
        <fieldset class="index-box">
          <legend style="text-align:center;"> Sign In</legend>
            <label style="margin-top:15px"> Email Address </label><br>
              <input style="margin-top:15px" type="email" id="emailaddress" name="emailaddress" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" required></input><br>
            <label style="margin-top:15px"> Password </label><br>
              <input style="margin-top:15px" type="password" name="password" 
                     required></input><br>
         </fieldset>
      
       <input id="okButton" type="submit" name="action" value = "signIn" disabled/>
     </form>	
      </div>
      </center>
      <script>
        isLogin();
     </script>
</body>
</html>
