<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
  <head>
    <title>Update</title>
    <link rel="stylesheet" type="text/css" href="/styles/teamstyles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <style> 
   option[value=""][disabled] {
       display:none;
   }
   td {
     text-align:center;
    }
   </style>
  </head>
  <body>
    <form:form action="editTeam" method="post" modelAttribute="teamInfo">
      <div class = "flex-container">
        <fieldset class="box">
        
          <legend> Team Information</legend>
            <label> ID </label>
              <form:input type="text" path="id" readonly="true" value="${teamInfo.id}" />
            <label> Name </label>
              <form:input type="text" path="name" value="${teamInfo.name}" />
            <label> Country </label>  
              <form:input type="text" path="country" readonly="true" value="${teamInfo.country}"/>      
        </fieldset>
        </div>
        <div class = "flex-container">
        <table class="box" cellpadding="5" 
        style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9 ;
         border-radius:15px;">
        <caption> <h2> <span>Players in team</span> </h2> </caption>
        <tr>
           <th>Id </th>
            <th> Name</th>
             <th>Role </th>
              <th>Country</th>
               <th>Remove player </th>
         </tr>        
        <c:forEach var="player" items="${teamInfo.playersInTeam}">
        <tr>
              <td> <c:out value="${player.id}" /> </td>
              <td> <c:out value="${player.name}" /> </td>
              <td> <c:out value="${player.role}" /> </td>
              <td> <c:out value="${player.country}" /> </td>    
              <td> <input type="checkbox" name="removePlayers" value="${player.id}"/> </td>   
        </tr>
        </c:forEach>       
      </table>
      <br> <br>
      <table class="box"cellpadding="5" style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9 ; border-radius:15px;">
        <caption> <h2> <span>Players not in team</span> </h2> </caption>
        <tr>
           <th>Id </th>
            <th> Name</th>
             <th>Role </th>
              <th>Country</th>
               <th>Add players </th>
         </tr>
        <c:forEach var="player" items="${teamInfo.playersNotInTeam}">
        <tr>
              <td> <c:out value="${player.id}" /> </td>
              <td> <c:out value="${player.name}" /> </td>
              <td> <c:out value="${player.role}" /> </td>
              <td> <c:out value="${player.country}" /> </td>    
              <th><td> <input type="checkbox" name="addPlayers" value="${player.id}"/> </td>   
        </tr>
        </c:forEach>        
      </table>
      </div>
       <button type="submit" value="edit">save</button>
    </form:form>
    <form:form action="viewTeams" method="get"> 
      <input type="submit" value="back"/>
    </form:form>
  </body>
</html>
