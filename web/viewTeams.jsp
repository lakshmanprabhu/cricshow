<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
  <head>
    <title> All Team Details </title>
    <link rel="stylesheet" type="text/css" href="/styles/styles.css">
    <style>
    td {
     text-align:center;
    }

     </style>
  </head>
  <body>
    <div align="center">
      <table style =" background-color:#9ed2d9; border-radius:15px;
       box-shadow:15px 15px 3px #9dd2c9 ;" cellpadding="5">
        <caption> <h2> <span>View All Teams</span> </h2> </caption>
        <tr style="text-align:center;">
        <td colspan=3>
        <button value="back" onclick="location.href = 'home.jsp';">back</button>
         <td colspan=3>
        <button value="create" onclick="location.href = 'createTeam';">create</button>
        </td></tr>
        <tr>
            <th> ID  </th>
            <th> Name  </th>
            <th> Country  </th>
            <th> Edit</th>
            <th> Delete</th> 
        </tr>
        <c:forEach var="team" items="${viewTeams}">
        <tr>
              <td> <c:out value="${team.id}" /> </td>
              <td> <a title="More details" href="viewTeam?id=${team.id}&country=${team.country}">
                      <c:out value="${team.name}" /></a></td>
              <td> <c:out value="${team.country}" /> </td>
              <td>
                  <button type="submit" onclick="location.href='updateTeam?id=${team.id}&country=${team.country}';">update
                  </button>
              </td>
              <td>
                  <button type="submit" onclick="location.href='deleteTeam?id=${team.id}';">delete</button>       
              </td>               
        </tr>
        </c:forEach>
      </table>
    </div>   
  </body>
</html>
