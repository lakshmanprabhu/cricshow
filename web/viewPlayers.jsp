<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <title> View All Players Details </title>
    <link rel="stylesheet" type="text/css" href="/styles/styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="/scripts/pagination.js">
    </script>
    <style>
    td {
     text-align:center;
    }

     </style>
  </head>
  <body>
    <div  align="center">
    <button value="back" onclick="location.href = 'home.jsp';">back</button>
    <table id="info" style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9 ; border-radius:15px;" cellpadding="5">
       <caption> <h2> <span>View All Players</span> </h2> </caption>
       <button value="create" onclick="location.href ='createPlayer';">create</button>
         <tr>
           <th> ID  </th>
           <th> Name  </th>
           <th> Role  </th>
           <th> Country  </th>
           <th> Status  </th> 
           <th> Edit</th>
           <th> Delete</th>
         </tr>
       <c:forEach var="player" items="${pagedPlayerInfo.playerInfos}">
       <tr>
           <td> <c:out value="${player.id}" /> </td>
           <td><a href="viewPlayer?id=${player.id}" title="More details">
               <c:out value="${player.name}" /> </a>
           </td>
           <td> <c:out value="${player.role}" /> </td>
           <td> <c:out value="${player.country}" /> </td>
           <td> 
              <c:choose>  
                <c:when test="${player.status==false}">  
                  <c:out value="Inactive"></c:out>  
                </c:when>  
                <c:otherwise>  
                  <c:out value="Active"></c:out>  
                </c:otherwise>  
              </c:choose>
           </td>  
           <td>
             <a id="edit" href="updatePlayer?id=${player.id}"style="color:green;" title="Update">&#x1F58B;
             </a>
           </td>
           <td>
           <a id="delete" href="deletePlayer?id=${player.id}" style="color:red;" title="Delete">&#128465;
           </a>
           </td>               
       </tr>
       </c:forEach>
     </table> <br>     
      <center>
       <table>
          <c:if test="${pagedPlayerInfo.currentPage gt 0}">
           <td align="left"> <button id = "prev" value="1" onclick ="callAjax(this.value, '-1', ${last});"> 
                        Previous
                    </button>
           </td>
          </c:if>
          <c:forEach begin="1" end="${pagedPlayerInfo.noOfPages}" var="i">
          <td>
                    <input type="hidden" name="currentPage" class="currentpage" value="${i}">
                    <button onclick="callAjax(${i}, 'content', ${last});">${i}</button> </td>
          </c:forEach>
          
           <c:if test="${pagedPlayerInfo.currentPage lt pagedPlayerInfo.noOfPages}">
                   <td> <button id="next" value="1" onclick="callAjax(this.value, '+1', ${last});">Next</button></td>
           </c:if> 
         </table>
      </center>
  </body>
</html>
