<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
  <head>
    <title>Update</title>
    <link rel="stylesheet" type="text/css" href="/styles/teamstyles.css">
    <script type="text/javascript" src="/scripts/teamSelection.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <style> 
   option[value=""][disabled] {
       display:none;
   }
   td {
     text-align:center;
    }
   </style>
  </head>
  <body>
    <form:form action="editMatch" method="post" modelAttribute="matchInfo">
      <div class = "flex-container">
        <fieldset class="box">
        
          <legend> Match Information</legend>
            <label> ID </label>
              <form:input type="text" path="id" readonly="true" value="${matchInfo.id}"/>
            <label> Name </label>
              <form:input type="text" path="name" value="${matchInfo.name}"/> 
            <label> Venue </label>
              <form:input type="text" path="venue" value="${matchInfo.venue}"/>
            <label> Playing Format </label>
              <form:select path="playingFormat"> 
              	  <option selected>${matchInfo.playingFormat}</option>
                  <option value = "ONEDAY" >One Day </option>
                  <option value = "T20" >T20 </option>
                  <option value = "TEST" >Test </option>
              </form:select> <br>
            <label> Playing Date </label>  
              <input type="text" name="dateOfPlaying" readonly="true" value="${matchInfo.playingDate}">
               </input> 
              <br>  
                   
        </fieldset>
        </div>
        <div class = "flex-container">
        <table class="box" cellpadding="5" 
        style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9 ;
         border-radius:15px;">
        <caption> <h2> <span>Teams in match</span> </h2> </caption>
        <tr>
           <th>Id </th>
            <th> Name</th>
              <th>Country</th>
         </tr>
        
        <c:forEach var="team" items="${matchInfo.teams}">
        <tr>
              <td> <c:out value="${team.id}" /> </td>
              <td> <c:out value="${team.name}" /> </td>
              <td> <c:out value="${team.country}" /> </td>    
        </tr>
        </c:forEach>
        
      </table>
      <br> <br>
      <table id="addTwoTeam" class="box"cellpadding="5" style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9 ; border-radius:15px;">
        <caption> <h2> <span>List of teams</span> </h2> </caption>
        <tr>
           <th>Id </th>
            <th> Name</th>
              <th>Country</th>
               <th>Add teams </th>
         </tr>
        <c:forEach var="team" items="${matchInfo.teamsNotInMatch}">
        <tr>
              <td> <c:out value="${team.id}" /> </td>
              <td> <c:out value="${team.name}" /> </td>
              <td> <c:out value="${team.country}" /> </td>    
              <th><td class="addteams"> <input type="checkbox" onclick="selectTwoTeam()" name="addteam" 
                    value="${team.id}"/>  </td>     
        </tr>
        </c:forEach>
        
      </table>

      </div>
       <button type="submit">save</button>
    </form:form>
    <form:form action="viewMatches" method="get"> 
      <input type="submit" value="back"/>
      </form:form>
  </body>
</html>
