<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
<html>
  <head>
    <title> </title>
    <link rel="stylesheet" type="text/css" href="/styles/matchstyles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    		
</script>
  </head>
  <body>
    <div class="top-div">
      <img class="img-style" src="/images/crickethome.jpg"/>
    </div> 
    <h3>CricShow 
      <span>&nbspTournament</span>
    </h3>
    <form:form action="saveMatch" method="post" modelAttribute="matchInfo">
      <div class = "flex-container">
        <fieldset class="box"  >
          <legend> Match Information</legend>
            <label> Name  </label><br>
              <form:input type="text" path="name" maxlength="40" required= "required"/> <br>
            <label> Venue </label> <br>
              <form:input type="text" path="venue" maxlength="40" required="required"/> <br>
            <label> Playing Date  </label> <br>
              <input type="date" name="dateOfPlaying" required></input> <br>
            <label> Playing Format </label> <br>
              <form:select path="playingFormat"> 
                  <option > Choose playing format</option>
                  <option value = "ONEDAY" >One Day </option>
                  <option value = "T20" >T20 </option>
                  <option value = "TEST" >Test </option>
              </form:select> <br>  
        </fieldset>
</div> 
      <input class="save" type="submit" value="save"/>
    </form:form>
    <form:form action="viewMatches" method="get"> 
      <input type="submit" value="back"/>
      </form:form>
  </body>
</html>
