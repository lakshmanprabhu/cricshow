<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
  <head>
    <title>Single View</title>
    <link rel="stylesheet" type="text/css" href="/styles/styles.css">
  </head>
  <body>
    <div align="center">
      <table cellpadding="5" style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9 ; border-radius:15px;">
        <caption> <h2> <span>View team</span> </h2> </caption>
        <tr>
            <th> Id  </th>
            <td> <c:out value="${teamInfo.id}"/> </td>
        </tr>
        <tr>
            <th> Name  </th>
            <td> <c:out value="${teamInfo.name}"/></td>
        </tr>
        <tr>
           <th> Country </th>
           <td> <c:out value="${teamInfo.country}"/> </td>
        </tr>
        </table>
        <br>
        <table cellpadding="5" style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9 ; border-radius:15px;">
        <caption> <h3> <span>Players list in team</span> </h3> </caption>
        <tr>
           <th>Id </th>
            <th> Name</th>
             <th>Role </th>
              <th>Country</th>
         </tr>
        <c:forEach var="player" items="${teamInfo.playersInTeam}">
        <tr>
              <td> <c:out value="${player.id}" /> </td>
              <td> <c:out value="${player.name}" /> </td>
              <td> <c:out value="${player.role}" /> </td>
              <td> <c:out value="${player.country}" /> </td>       
        </tr>
        </c:forEach>
        
      </table>
      <table>
      <tr>
      <th>Status</th>
      <th>Batsman</th>
      <th>Bowler</th>
      <th>WicketKeeper</th>
      <th>Total</th>
      </tr>
      <tr>
       <td> 
       <c:choose>  
                <c:when test="${teamInfo.rolesCount[4]==1}">  
                  <c:out value="true"></c:out>  
                </c:when>  
                <c:otherwise>  
                  <c:out value="false"></c:out>  
                </c:otherwise>  
              </c:choose>
       
        </td>
        <td> <c:out value="${teamInfo.rolesCount[0]}" /> </td>
              <td> <c:out value="${teamInfo.rolesCount[1]}" /> </td>
              <td> <c:out value="${teamInfo.rolesCount[2]}" /> </td>
              <td> <c:out value="${teamInfo.rolesCount[3]}" /> </td>       
      </tr>
      </table>
      <br>
      <form:form action="viewTeams" method="get"> 
      <input type="submit" value="back"/>
      </form:form> 
      
    </div>   
    
  </body>
</html>
