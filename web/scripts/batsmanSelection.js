function selectOneBatsman() {
	var checkboxgroup = document.getElementById('addBatsman').getElementsByTagName("input");
	var limit = 1;
	for (var i = 0; i < checkboxgroup.length; i++) {
		checkboxgroup[i].onclick = function() {
			var checkedcount = 0;
				for (var i = 0; i < checkboxgroup.length; i++) {
				checkedcount += (checkboxgroup[i].checked) ? 1 : 0;
			}
			if (checkedcount > limit) {
				alert("Kindly select " + limit + " batsman.");
				this.checked = false;
			}
		}
	}
}
function selectTwoBatsman() {
	var checkboxgroup = document.getElementById('addBatsmen').getElementsByTagName("input");
	var limit = 2;
	for (var i = 0; i < checkboxgroup.length; i++) {
		checkboxgroup[i].onclick = function() {
			var checkedcount = 0;
				for (var i = 0; i < checkboxgroup.length; i++) {
				checkedcount += (checkboxgroup[i].checked) ? 1 : 0;
			}
			if (checkedcount > limit) {
				alert("Kindly select " + limit + " batsman only.");
				this.checked = false;
			}
		}
	}
}
