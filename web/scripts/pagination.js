function callAjax(page, choice, last) {
    document.getElementById('prev').value = page;
    document.getElementById('next').value = page;   
    var checkPage = page; 
    if (choice === '-1' && checkPage > 1) {
        page = page - 1;
        document.getElementById('prev').value = page;
    }
    if (choice === '+1' && checkPage < last ) {
        page = page * 1 + 1;
        document.getElementById('next').value = page;
    }
    httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        console.log('Unable to create XMLHTTP instance');
        return false;
    }
    httpRequest.open('GET', 'viewRemainingPlayers?page='+page);
    httpRequest.responseType = 'json';
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                var array = httpRequest.response;
                var j = 0;
                for (var i = 1; i<= array.length; i++) {
                    var row = document.getElementById("info").rows;
                    var col = row[i].cells;
                    var name = array[j].PlayerName;
                    var viewPlayer = name.link("viewPlayer?id="+array[j].Id);
                    col[0].innerHTML = array[j].Id;
                    col[1].innerHTML = viewPlayer;
                    col[2].innerHTML = array[j].Role;
                    col[3].innerHTML = array[j].Status;
                    col[4].innerHTML = array[j].Country;
                    col[5].style.display ='';
                    var editIcon = "&#x1F58B;";
                    var editsize = editIcon.fontsize(4);
                    var editColor = editsize.fontcolor('green');
                    var edit = editColor.link("updatePlayer?id="+array[j].Id);
                    col[5].innerHTML = edit;
                    col[6].style.display ='';
                    var delIcon = "&#128465;";
                    var delsize = delIcon.fontsize(4);
                    var delColor = delsize.fontcolor('red');
                    var del = delColor.link("deletePlayer?id="+array[j].Id);
                    col[6].innerHTML = del;
                    j = j + 1;
                }
                for (var i=array.length+1 ; i<=5; i++) {
                    var row = document.getElementById('info').rows;
                    var col = row[i].cells;
                    col[0].innerHTML = "";
                    col[1].innerHTML = "";
                    col[2].innerHTML = "";
                    col[3].innerHTML = "";
                    col[4].innerHTML = "";
                    col[5].style.display ='none';
                    col[6].style.display ='none';
                }  
            } else {
                console.log('Something went wrong..!!');
            }
        }
    }
}
