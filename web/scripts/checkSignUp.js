function isSignUp() {
    const signUpForm = document.getElementById('signUpForm');
    const emailAddress = document.getElementById('emailAddress');
    const okButton = document.getElementById('okButton'); 
    emailAddress.addEventListener('keyup', function (event) {
      isValidEmail = emailAddress.checkValidity();
      
      if ( isValidEmail ) {
        okButton.disabled = false;
      } else {
        okButton.disabled = true;
      }
    });
      
    okButton.addEventListener('click', function (event) {
      loginForm.submit();
    });
}
