function selectTwoTeam() {
	var checkboxgroup = document.getElementById('addTwoTeam').getElementsByTagName("input");
	var limit = 2;
	for (var i = 0; i < checkboxgroup.length; i++) {
		checkboxgroup[i].onclick = function() {
			var checkedcount = 0;
				for (var i = 0; i < checkboxgroup.length; i++) {
				checkedcount += (checkboxgroup[i].checked) ? 1 : 0;
			}
			if (checkedcount > limit) {
				alert("Kindly select " + limit + " team only.");
				this.checked = false;
			}
		}
	}
}
