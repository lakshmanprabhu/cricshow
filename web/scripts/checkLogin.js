function isLogin() {    
    const loginForm = document.getElementById('loginForm');
    const emailAddress = document.getElementById('emailaddress');
    const okButton = document.getElementById('okButton'); 
    emailAddress.addEventListener('keyup', function (event) {
      isValidEmail = emailAddress.checkValidity();
      
      if ( isValidEmail ) {
        okButton.disabled = false;
      } else {
        okButton.disabled = true;
      }
    });
      
    okButton.addEventListener('click', function (event) {
      loginForm.submit();
    });
}
