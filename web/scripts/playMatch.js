function playMatch(bowlerId, matchId, overId, firstBatsmanId, secondBatsmanId, strikerID) {
    document.getElementById('displayInfo').style.display="block";
    var total = document.getElementById('total').value;
    var runsInOver = document.getElementById('runsInOver').value;
    var newTotal = runsInOver * 1;
    //var striker = '*';
    var strikeBatsman = firstBatsmanId;
    var nonStrikeBatsman = secondBatsmanId;
    var striker = strikerID;
    var run;
    httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        console.log('Unable to create XMLHTTP instance');
        return false;
    }
    httpRequest.open('GET', 'startOver?bowlerId='+bowlerId+'&matchId='+matchId+'&overId='+overId+'&batsmanId='+striker);
    httpRequest.responseType = 'json';
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                var array = httpRequest.response;
                var j = 0; 
                var i = 1;    
                var row = document.getElementById("playInfo").rows;
                var col = row[i].cells;
                col[0].innerHTML = array[j].BowlerName;
                col[1].innerHTML = array[j].BatsmanOnStrikeId;
                if (6 >= array[j].BallNumber) {
                    col[2].innerHTML = array[j].BallNumber;
                } else {
                  selectNewBowler(); 
                }
                col[3].innerHTML = array[j].Runs;
                col[4].innerHTML = array[j].BallResult;
                if ('WICKET' === array[j].BallResult) {
                    chooseNextBatsman();
                }
                newTotal = newTotal + (array[j].Runs * 1);
                col[5].innerHTML = newTotal;
                if ("ONES" === array[j].BallResult || "THREES" === array[j].BallResult || "LEGBYES" === array[j].BallResult
                || "BYES" === array[j].BallResult) {
                    var strikerName = array[j].BatsmanOnStrikeId;
                    striker = strikerName;
                    var checkbatsmanName = strikerName;
                    var firstBatsmanId = document.getElementById('strikerId').value;
                    var secondBatsmanId = document.getElementById('nonStrikerId').value;
                    if (checkbatsmanName == firstBatsmanId) { 
                        striker = secondBatsmanId;
                        document.getElementById('striker').innerHTML = secondBatsmanId;
                    } else {
                        striker = firstBatsmanId;
                        document.getElementById('striker').innerHTML = firstBatsmanId;
                    }  
                }
                runsInOver = newTotal * 1;
                total = (total * 1) + runsInOver;
            } else {
                console.log('Something went wrong..!!');
            }
        }
        document.getElementById('runsInOver').value = runsInOver;
        document.getElementById('total').value = total;
    }
}
function selectNewBowler() {
    document.getElementById('selectBowler').style.display="block";
    document.getElementById('newOver').style.display="none";
    document.getElementById('startPlay').style.display='none';
    document.getElementById('displayInfo').style.display="none";
}

function chooseNextBatsman() {
    document.getElementById('displayInfo').style.display="none";
    document.getElementById('selectBatsman').style.display="block";
    document.getElementById('newOver').style.display="none";
    document.getElementById('startPlay').style.display='none';
}
