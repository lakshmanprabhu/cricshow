<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
<html>
  <head>
    <title> </title>
    <link rel="stylesheet" type="text/css" href="/styles/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <div class="top-div">
      <img class="img-style" src="/images/crickethome.jpg"/>
    </div> 
    <h3>CricShow 
      <span>&nbspTournament</span>
    </h3>  
    <form:form action="savePlayer" method="post" modelAttribute="playerInfo"
     enctype="multipart/form-data">
      <div class = "flex-container">
        <fieldset class="box">
          <legend> Personal Information</legend>
            <label> Picture</label>
            <input type = "file" name = "file" /></br>
            <label> Name  </label><br>
              <form:input type="text" path="name" pattern="[a-zA-Z][a-zA-Z\s]*"
              maxlength="40" title="Only alphabet, spaces upto 40 characters"
              required="required" />
              <br>
            <label> Date of birth  </label> <br>
              <form:input type="date" path="dob" required="required" /> <br>
            <label> Country </label> <br>
              <form:select path="country"> 
                  <option > Choose country</option>
                  <form:option value = "INDIA" >India </form:option>
                  <form:option value = "AUSTRALIA" >Australia </form:option>
                  <form:option value = "ENGLAND" >England </form:option>
                  <form:option value = "BANGLADESH" >Bangladesh </form:option>
                  <form:option value = "NEWZEALAND" >NewZealand </form:option>
                  <form:option value = "SRILANKA" >SriLanka </form:option>
                  <form:option value = "PAKISTAN" >Pakistan </form:option>
                  <form:option value = "SOUTHAFRICA" >SouthAfrica </form:option>
                  <form:option value = "WESTINDIES" >WestIndies </form:option>
              </form:select> <br>
            <label> Role  </label> <br>
              <form:select path="role">
                  <form:option value = "Batsman" >Batsman </form:option>
                  <form:option value = "Bowler" >Bowler </form:option>
                  <form:option value = "WicketKeeper" >WicketKeeper </form:option>
                  <form:option value = "Allrounder" >Allrounder </form:option>
              </form:select><br>
            <label> Batting type  </label> <br>
              <form:select path="battingType">
                  <form:option value = "Right-handed">Right-handed </form:option>
                  <form:option value = "Left-handed">Left-handed </form:option>
              </form:select> <br>
            <label> Bowling type  </label> <br>
              <form:select path="bowlingType"> 
                  <form:option value = "Right-arm Fast" >Right-arm Fast </form:option>
                  <form:option value = "Left-arm Fast">Left-arm Fast </form:option>
                  <form:option value = "Off-spin" >Off-spin </form:option>
                  <form:option value = "Leg-spin" >Leg-spin </form:option>
              </form:select> <br>
        </fieldset>
        <fieldset class="box">
          <legend> Contact Information </legend>
            <label> Address  </label> <br>
              <form:textarea rows="4" cols="50" path="address" 
                   placeholder="Door no,Street,City,State" required="required"/> <br>
            <label> Phone Number  </label> <br>
              <form:input type="tel" path="phoneNumber" title="Only 10 digit number"
               pattern="\d{10}" maxlength="10" required="required"></form:input> <br>
            <label> Pincode  </label> <br>
              <form:input type="text" path="pincode" pattern="[1-9][0-9]{5}"
              title="Only 6 digit pincode" maxlength="6" required="required"></form:input> <br>
        </fieldset>
      </div>
      <input type="submit" value="save"/>
    </form:form>
    <form:form action="viewPlayers" method="get"> 
      <input type="submit" value="back"/>
    </form:form> 
  </body>
</html>
