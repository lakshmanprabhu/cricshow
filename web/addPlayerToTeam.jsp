<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
  <head>
    <title> </title>
    <link rel="stylesheet" type="text/css" href="/styles/teamstyles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <div class="top-div">
      <img class="img-style" src="/images/crickethome.jpg"/>
    </div> 
    <h3>CricShow 
      <span>&nbspTournament</span>
    </h3>
      <form:form action="addPlayerToTeam" method="post" modelAttribute="teamInfo">
      <div class="flex-container"> 
          <table class="addplayershead" cellpadding="5">
            <tr>
              <td class="addplayer"> Name <form:input type="text"
               path="name" readonly="readonly" value="${teamInfo.name}"/></td>
              <td class="addplayer"> Country<form:input type="text" 
              path="country" readonly="readonly" value="${teamInfo.country}"/></td>
            </tr>
          </table>
           <table class="teamrequirements" cellpadding="5" width="100%">
          <tr>
          <th colspan=3>Team Requirements</th>
          </tr><tr>
          <td class="addplayer"> 3 Batsman  </td>
          <td class="addplayer"> 3 Bowler </td>
          <td class="addplayer"> 1 WicketKeeper only </td>
          </tr>
          </table>
          </div>
          <div class = "flex-container"> 
          <table class="addplayersbody" cellpadding="5">
          <tr>
           <th>Id </th>
           <th> Name</th>
           <th>Role </th>
           <th>Country</th>
           <th>Add players </th>
          </tr>
          <c:forEach var="player" items="${playersByCountry}">
             <tr>
              <td class="addplayer"> <c:out value="${player.id}" /> </td>
              <td class="addplayer"> <c:out value="${player.name}" /> </td>
              <td class="addplayer"> <c:out value="${player.role}" /> </td>
              <td class="addplayer"> <c:out value="${player.country}" /> </td>
              <td class="addplayer"> <input type="checkbox" name="addPlayers" 
                  value="${player.id}"/> </td>
        </tr>
        </c:forEach>
        </table>
        </div>
         <br>
         <input class="save" type="submit" value="add"/>
      </form:form>
  </body>
</html>
