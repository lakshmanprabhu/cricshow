<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
  <head>
    <title>Update</title>
    <link rel="stylesheet" type="text/css" href="/styles/teamstyles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <style> 
   option[value=""][disabled] {
       display:none;
   }
   td {
     text-align:center;
    }
   </style>
      <script type="text/javascript" src="/scripts/batsmanSelection.js">
      </script>
      <script type="text/javascript" src="/scripts/bowlerSelection.js">
      </script>
   
  </head>
  <body>
    <form action="createOver" method="post">
    <div align="center">
      <table style =" background-color:#9ed2d9; border-radius:15px;
       box-shadow:15px 15px 3px #9dd2c9;" cellpadding="5">
        <caption> <h1> <span>Match Information</span> </h1> </caption>
        <tr>
            <th> ID  </th>
            <th> Name  </th>
            <th> Venue  </th> 
            <th> Playing Format  </th> 
            <th> Playing Date  </th> 
        </tr>
        <tr>
              <td> <c:out value="${playInfo.matchInfo.id}" /> </td>
              <td> <c:out value="${playInfo.matchInfo.name}" /></td>
              <td> <c:out value="${playInfo.matchInfo.venue}"/> </td>
              <td> <c:out value="${playInfo.matchInfo.playingFormat}"/> </td>
              <td> <c:out value="${playInfo.matchInfo.playingDate}"/> </td>            
        </tr>
      </table>
    </div> <br><br>
        <h2><center><span> Select the players to play match</span></center></h2>
        <br>
        <div class = "flex-container">
        <table class="box" cellpadding="5" 
        style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9;
         border-radius:15px;">
        <caption> <h3> <span>Batting Team </span> </h3> </caption>
        <tr>
          <th>Team ID</th>      
          <th>Team Name</th>
        </tr>
        <tr>
          <td> <c:out value="${playInfo.battingTeamInfo.id}" /></td>
          <td> <c:out value="${playInfo.battingTeamInfo.name}" /></td>
        </tr></table>
      <br> <br>
      <table class="box"cellpadding="5" style =" background-color:#9ed2d9;
             sbox-shadow:15px 15px 3px #9dd2c9; border-radius:15px;">
        <caption> <h3> <span>Fielding Team </span> </h3> </caption>
        <tr>
          <th>Team ID</th>      
          <th>Team Name</th>
        </tr>
        <tr>
          <td> <c:out value="${playInfo.fieldingTeamInfo.id}" /></td>
          <td> <c:out value="${playInfo.fieldingTeamInfo.name}" /></td>
        </tr></table>
      </div>
      <div class = "flex-container">  
      <table id="addBatsmen" class="box" cellpadding="5" 
        style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9;
         border-radius:15px;">
        <caption> <h4> <span>Batting Team Players List</span> </h4> </caption>
        <tr>
           <th>Id </th>
            <th> Name</th>
              <th>Country</th>
              <th>Role</th>
              <th>Select Two Batsman </th>
         </tr>
        
        <c:forEach var="player" items="${playInfo.battingTeamInfo.playersInTeam}">
        <tr>
              <td> <c:out value="${player.id}" /> </td>
              <td> <c:out value="${player.name}" /> </td>
              <td> <c:out value="${player.country}" /> </td> 
              <td> <c:out value="${player.role}" /> </td>    
              <td class="addBatsmen"> <input type="checkbox" onclick="selectTwoBatsman()" name="addBatsmen" 
                    value="${player.id}"/>  </td> 
        </tr>
        </c:forEach>
        
      </table>
      <table id="addBowler" class="box" cellpadding="5" 
        style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9;
         border-radius:15px;">
        <caption> <h3> <span>Fielding Team Players List</span> </h3> </caption>
        <tr>
           <th>Id </th>
            <th> Name</th>
              <th>Country</th>
              <th>Role</th>
               <th>Select One Bowler </th>
         </tr>
        <c:forEach var="player" items="${playInfo.fieldingTeamInfo.playersInTeam}">
        <tr>
              <td> <c:out value="${player.id}" /> </td>
              <td> <c:out value="${player.name}" /> </td>
              <td> <c:out value="${player.country}" /> </td> 
              <td> <c:out value="${player.role}" /> </td>    
              <td class="addBowler"> <input type="checkbox" onclick="selectOneBowler()" name="addBowler" 
                    value="${player.id}"/>  </td>     
        </tr>
        </c:forEach>
      </table>
      </div>
      <input type = "hidden" name = "matchId" value =${playInfo.matchInfo.id} />
       <input type = "submit" value ="Start Play"></input>
    </form>
    <form:form action="viewMatches" method="get"> 
      <input type="submit" value="back"/>
      </form:form>
    </div>
  </body>
</html>
