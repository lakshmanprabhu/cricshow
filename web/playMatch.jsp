<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
  <head>
    <title>Update</title>
    <link rel="stylesheet" type="text/css" href="/styles/styles.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="/scripts/playMatch.js">
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <style> 
   option[value=""][disabled] {
       display:none;
   }
   td {
     text-align:center;
    }
    #displayInfo {
        display:none;
    }
    #newOver {
        display:none;
    }
    #selectBowler {
        display:none;
    }
    #selectBatsman {
        display:none;
    }
   </style>
  </head>
  <body>
    <div align="center">
    <h1> <span>Play Information </span> </h1>
    <div style="color:green; font-size:20px;">${matchOverInfo.matchInfo.playingFormat},&nbsp;${matchOverInfo.matchInfo.name}&nbsp;in&nbsp;${matchOverInfo.matchInfo.venue},&nbsp;${matchOverInfo.matchInfo.playingDate}</div></div>
       <h1> <span>Play Information</span> </h1>
      <div class="flex-container">
      <table style =" background-color:#9ed2d9; border-radius:15px;
       box-shadow:15px 15px 3px #9dd2c9;" cellpadding="5" width="40%">
        <tr>
            <th>Bowler Name</th>
            <th>Over Number</th> 
        </tr>
        <td><c:out value="${matchOverInfo.bowlerName}" /></td>
        <td><c:out value="${matchOverInfo.overNumber}" /></td> 
        <tr>
        </tr>
        </table>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <table style =" background-color:#9ed2d9; border-radius:15px;
       box-shadow:15px 15px 3px #9dd2c9;" cellpadding="5" width = "40%">
            <input id="matchId" type = "hidden" value = ${matchOverInfo.matchId} />
            <input id="overId" type = "hidden" value = ${matchOverInfo.overId} />
            <input id="bowlerId" type = "hidden" value = ${matchOverInfo.bowlerId} />
            <input id="strikerId" type = "hidden" value = ${matchOverInfo.firstBatsmanId} />
            <input id="nonStrikerId" type = "hidden" value = ${matchOverInfo.secondBatsmanId} />
            <th>Striker Name</th> 
            <th>Non-Striker Name</th>
        </tr>
        <tr>
              <td id ="firstName"><c:out value="${matchOverInfo.firstBatsmanName}"/></td>
              <td id ="secondName"><c:out value="${matchOverInfo.secondBatsmanName}"/></td>            
        </tr>
      </table>
      </div>
      <div id ='displayInfo'>
      <table>
        <tr>
          <th> Total score</th>
          </tr>
        <tr>
          <td id ="score"></td>
        </tr>
       </table>
       <table id = 'playInfo'> 
       <caption> <h3> Over Information </h3> </caption>
       <tr>
           <th>Bowler Name</th>
           <th>Batsman Name</th>
           <th>Ball Number</th>
           <th>Runs</th>
           <th>BallResult</th>
           <th>Runs In Over</th> 
       </tr>
       <tr> 
       <td></td>
       <td id = "striker"></td>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
       </tr>
       </table>
      </div>
      <div id = "check"></div>
      
      <input type="hidden" name="total" id="total" value="0"/>
      <input type="hidden" name="runsInOver" id="runsInOver" value="0"/>
      <button id = "startPlay" onclick ="playMatch(${matchOverInfo.bowlerId}, ${matchOverInfo.matchId}, ${matchOverInfo.overId}, ${matchOverInfo.firstBatsmanId},${matchOverInfo.secondBatsmanId},${matchOverInfo.firstBatsmanId});">Start Play</button>
      
      <div id = "newOver">
      <form action="nextOver" method="post">
          <input type = "hidden" name = "matchId" value = ${matchOverInfo.matchId} />
          <input type = "hidden" name = "firstBatsmanId" value =${matchOverInfo.firstBatsmanId} />
          <input type = "hidden" name = "secondBatsmanId" value =${matchOverInfo.secondBatsmanId} />
          <input type = "hidden" name = "bowlerId" value =${matchOverInfo.bowlerId} />
          <input type = "submit" value ="Next Over"></input>
      </form>
      </div>
      <div id = "selectBatsman">
          <form action="selectBatsman" method="post">
          <input type = "hidden" name = "matchId" value = ${matchOverInfo.matchId} />
          <input id = "batsmanOut" type = "hidden" name = "outBatsmanId" value =${matchOverInfo.firstBatsmanId} />
          <input type = "hidden" name = "overId" value =${matchOverInfo.overId} />
          <input type = "hidden" name = "bowlerId" value =${matchOverInfo.bowlerId} />
          <input type = "hidden" name = "nonStrikerId" value =${matchOverInfo.secondBatsmanId} />
          <input type = "submit" value ="Select Batsman"></input>
          </form>
      </div>
      <div id = "selectBowler">
      <form action="selectBowler" method="post">
          <input type = "hidden" name = "matchId" value = ${matchOverInfo.matchId} />
          <input type = "hidden" name = "firstBatsmanId" value =${matchOverInfo.firstBatsmanId} />
          <input type = "hidden" name = "secondBatsmanId" value =${matchOverInfo.secondBatsmanId} />
          <input type = "hidden" name = "bowlerId" value =${matchOverInfo.bowlerId} />
          <input type = "hidden" name = "overId" value =${matchOverInfo.overId} />
          <input type = "submit" value ="Select Bowler"></input>
      </form>
      </div>
      
  </body>
</html>
