<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
  <head>
    <title>Single View</title>
    <link rel="stylesheet" type="text/css" href="/styles/styles.css">
  </head>
  <body>
    <div align="center">
      <table cellpadding="5" style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9; border-radius:15px;">
        <caption> <h2> <span>View match</span> </h2> </caption>
        <tr>
            <th> Id  </th>
            <td> <c:out value="${matchInfo.id}"/> </td>
        </tr>
        <tr>
            <th> Name  </th>
            <td> <c:out value="${matchInfo.name}"/></td>
        </tr>
        <tr>
           <th> Venue </th>
           <td> <c:out value="${matchInfo.venue}"/> </td>
        </tr>
        <tr>
           <th> Playing Format </th>
           <td> <c:out value="${matchInfo.playingFormat}"/> </td>
        </tr>
        <tr>
           <th> Playing Date </th>
           <td> <c:out value="${matchInfo.playingDate}"/> </td>
        </tr>
        </table>
        <br>
        <table cellpadding="5" style =" background-color:#9ed2d9;box-shadow:15px 15px 3px #9dd2c9; border-radius:15px;">
        <caption> <h3> <span>Teams list for match</span> </h3> </caption>
        <tr>
           <th>Id </th>
            <th> Name</th>
             <th>Role </th>
         </tr>
        <c:forEach var="team" items="${matchInfo.teams}">
        <tr>
              <td> <c:out value="${team.id}" /> </td>
              <td> <c:out value="${team.name}" /> </td>
              <td> <c:out value="${team.country}" /> </td>       
        </tr>
        </c:forEach>
        
      </table>
      <br>
      <form:form action="viewMatches" method="get"> 
      <input type="submit" value="back"/>
      </form:form>
      
    </div>   
    
  </body>
</html>
