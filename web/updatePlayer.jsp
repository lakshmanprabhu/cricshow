<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
  <head>
    <title>Update</title>
    <link rel="stylesheet" type="text/css" href="/styles/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <style> 
   option[value=""][disabled] {
       display:none;
   }
   </style>
  </head>
  <body>
    <form:form action="editPlayer" method="post" modelAttribute="playerInfo"
    enctype="multipart/form-data">
      <div class = "flex-container">
        <fieldset class="box">
          <legend> Personal Information</legend>
          <label> Picture</label>
          <input type = "file" name = "file" />
          <form:input type="hidden" path="imagePath" value= "${playerInfo.imagePath}"/></br>
            <label> ID : </label>
              <form:input type="text" path="id" readonly="true" 
              value="${playerInfo.id}"></form:input>
            <label> Name : </label>
              <form:input type="text" path="name" value="${playerInfo.name}"  
                          required="required" pattern="[a-zA-Z][a-zA-Z\s]*" 
                          maxlength="40"/><br>
            <label> Date of birth : </label> 
              <form:input type="date" path="dob" value="${playerInfo.dob}"
               required="required" ></form:input> <br>
            <label> Country : </label>
              <form:select path="country"> 
                  <option selected>${playerInfo.country}</option>
                  <form:option value = "INDIA">INDIA </form:option>
                  <form:option value = "AUSTRALIA">AUSTRALIA </form:option>
                  <form:option value = "ENGLAND">ENGLAND </form:option>
                  <form:option value = "BANGLADESH">BANGLADESH </form:option>
                  <form:option value = "NEWZEALAND">NEWZEALAND </form:option>
                  <form:option value = "SRILANKA">SRILANKA </form:option>
                  <form:option value = "PAKISTAN">PAKISTAN </form:option>
                  <form:option value = "SOUTHAFRICA">SOUTHAFRICA </form:option>
                  <form:option value = "WESTINDIES">WESTINDIES </form:option>
              </form:select> <br>
            <label> Role : </label>
              <form:select path="role"> 
                  <option selected>${playerInfo.role}</option>            
                  <form:option value = "Batsman">Batsman </form:option>
                  <form:option value = "Bowler">Bowler </form:option>
                  <form:option value = "WicketKeeper">WicketKeeper </form:option>
                  <form:option value = "Allrounder">Allrounder </form:option>
              </form:select><br>
            <label> Batting type : </label>
              <form:select path="battingType">
                  <option selected>${playerInfo.battingType}</option>
                  <form:option value = "Right-handed">Right-handed </form:option>
                  <form:option value = "Left-handed">Left-handed </form:option>
              </form:select> <br>
            <label> Bowling type : </label>
              <form:select path="bowlingType"> 
                  <option selected>${playerInfo.bowlingType}</option>
                  <form:option value = "Right-arm Fast">Right-arm Fast </form:option>
                  <form:option value = "Left-arm Fast">Left-arm Fast </form:option>
                  <form:option value = "Off-spin">Off-spin </form:option>
                  <form:option value = "Leg-spin">Leg-spin </form:option>
              </form:select> <br>
        </fieldset>
        <fieldset class="box">
          <legend> Contact Information </legend>
            <form:hidden path="contactId" value = "${playerInfo.contactId}"/>
            <label> Address : </label>
              <form:textarea rows="4" cols="50" path="address" 
                   placeholder="Door no,Street,City,State" required="required"
                   value="${playerInfo.address}"/> <br>
            <label> Phone Number : </label>
              <form:input type="tel" path="phoneNumber" maxlength="10"
               value="${playerInfo.phoneNumber}" required="required" ></form:input> <br>
            <label> Pincode : </label> 
              <form:input type="text" path="pincode" maxlength="6"
               value="${playerInfo.pincode}" required="required" ></form:input> <br>
        </fieldset>
      </div>
       <input type="submit" value="edit"/>
    </form:form>
    <form:form action="viewPlayers" method="get"> 
      <input type="submit" value="back"/>
    </form:form> 
  </body>
</html>
