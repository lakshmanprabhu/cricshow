# CricShow
It is a cricket management which provides user to perform operations on 
player, team, match, over and over detail. CricShow gives the ease of doing 
various operation available.
####Operations available such as 

   - View
   - Create
   - Delete
   - Update 

## Requirements
   
    Java Open JDK 1.8 
    Tomcat 8.5.3 
    Mysql 5.7.27
      
## Getting Started

   1. Install and configure JDK 1.8
   2. Install Mysql 5.7.27
   3. Install Tomcat 8.5.3
   4. Install and configure Apache Maven 3.6.0
   5. Add dependency of various jar files which is described in pom.xml file.

## Installing

Download the project. 
Change your working directory to the project directory.
Run the project using the command ``` mvn clean package ```

## Built With
   * Maven
   * Spring

      
